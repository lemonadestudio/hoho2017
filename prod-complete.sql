-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: lemonade.nazwa.pl:3306
-- Czas generowania: 30 Maj 2017, 13:55
-- Wersja serwera: 5.5.53-MariaDB
-- Wersja PHP: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `lemonade_8`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_classes`
--

CREATE TABLE `acl_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_entries`
--

CREATE TABLE `acl_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `security_identity_id` int(10) UNSIGNED NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) UNSIGNED NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_object_identities`
--

CREATE TABLE `acl_object_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_object_identity_ancestors`
--

CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) UNSIGNED NOT NULL,
  `ancestor_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_security_identities`
--

CREATE TABLE `acl_security_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `attribute`
--

CREATE TABLE `attribute` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `seoGenerate` tinyint(1) DEFAULT NULL,
  `seoTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoKeywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mapPositionFromTop` double DEFAULT NULL,
  `mapPositionFromLeft` double DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `city`
--

INSERT INTO `city` (`id`, `name`, `slug`, `createdAt`, `updatedAt`, `seoGenerate`, `seoTitle`, `seoKeywords`, `seoDescription`, `mapPositionFromTop`, `mapPositionFromLeft`, `postal_code`) VALUES
(1, 'Mińsk Mazowiecki', 'minsk-mazowiecki', '2013-12-17 08:51:37', NULL, 1, 'to change', 'to change', 'to change', 52.177984563503, 21.581783294678, '05-300'),
(2, 'Opole Lubelskie', 'opole-lubelskie', '2013-12-17 08:51:44', NULL, 1, 'to change', 'to change', 'to change', 51.147048104912, 21.970081329346, '24-300'),
(3, 'Łosice', 'losice', '2013-12-17 08:51:50', NULL, 1, 'to change', 'to change', 'to change', 52.212169248292, 22.717001438141, '08-200'),
(4, 'Lubartów', 'lubartow', '2013-12-17 08:51:56', NULL, 1, 'to change', 'to change', 'to change', 51.460852446455, 22.587890625, '21-100'),
(5, 'Lublin', 'lublin', '2013-12-17 08:52:03', NULL, 1, 'to change', 'to change', 'to change', 51.235374691297, 22.574672698975, ''),
(7, 'Krasnystaw', 'krasnystaw', '2013-12-17 08:52:23', NULL, 1, 'to change', 'to change', 'to change', 50.98869235779, 23.154716491699, '22-300'),
(8, 'Hrubieszów', 'hrubieszow', '2013-12-17 08:52:41', NULL, 1, 'to change', 'to change', 'to change', 50.806097448347, 23.886337280273, '22-500'),
(10, 'Kozienice', 'kozienice', '2013-12-17 08:52:56', NULL, 1, 'to change', 'to change', 'to change', 51.580483198305, 21.566162109375, '26-900'),
(11, 'Chełm', 'chelm', '2013-12-17 09:16:33', NULL, 1, 'to change', 'to change', 'to change', 51.135954983623, 23.477954864502, '22-100'),
(12, 'Biała Podlaska', 'biala-podlaska', '2013-12-17 09:31:02', NULL, 1, 'to change', 'to change', 'to change', 52.037392656245, 23.119826316833, '21-500'),
(13, 'Łuków', 'lukow', '2013-12-18 10:21:38', NULL, 1, 'to change', 'to change', 'to change', 51.929236371811, 22.381725311279, '21-400'),
(14, 'Lipsko', 'lipsko', '2013-12-18 10:22:15', NULL, 1, 'to change', 'to change', 'to change', 51.15178610143, 21.651306152344, '27-300'),
(15, 'Kraśnik', 'krasnik', '2013-12-18 10:23:03', NULL, 1, 'to change', 'to change', 'to change', 50.911692475709, 22.236328125, '23-210'),
(16, 'Ostrowiec Świętokrzyski', 'ostrowiec-swietokrzyski', '2013-12-18 10:23:54', NULL, 1, 'to change', 'to change', 'to change', 50.92035050944, 21.395874023438, '27-400'),
(17, 'Włodawa', 'wlodawa', '2013-12-18 10:24:33', NULL, 1, 'to change', 'to change', 'to change', 51.544673673399, 23.550449609756, '22-200'),
(18, 'Węgrów', 'wegrow', '2013-12-18 10:25:11', NULL, 1, 'to change', 'to change', 'to change', 52.392363397072, 22.022094726562, '07-100'),
(19, 'Tomaszów Lubelski', 'tomaszow-lubelski', '2013-12-18 10:26:00', NULL, 1, 'to change', 'to change', 'to change', 50.444168971502, 23.417358398438, '22-600'),
(21, 'Siemiatycze', 'siemiatycze', '2013-12-18 10:27:21', NULL, 1, 'to change', 'to change', 'to change', 52.40577019043, 22.857055664062, '17-300'),
(22, 'Siedlce', 'siedlce', '2013-12-18 10:28:00', NULL, 1, 'to change', 'to change', 'to change', 52.168088592395, 22.276239395142, '08-110'),
(23, 'Radzyń Podlaski', 'radzyn-podlaski', '2013-12-18 10:28:40', NULL, 1, 'to change', 'to change', 'to change', 51.779922221349, 22.615957260132, '21-300'),
(24, 'Radom', 'radom', '2013-12-18 10:29:17', NULL, 1, 'to change', 'to change', 'to change', 51.402632657661, 21.154174804688, '26-610'),
(25, 'Puławy', 'pulawy', '2013-12-18 10:29:52', NULL, 1, 'to change', 'to change', 'to change', 51.412912129355, 21.967163085938, '24-110'),
(26, 'Poniatowa', 'poniatowa', '2013-12-18 10:30:28', NULL, 1, 'to change', 'to change', 'to change', 51.191192216528, 22.062885761261, '24-320'),
(28, 'Biłgoraj', 'bilgoraj', '2013-12-18 10:31:35', NULL, 1, 'to change', 'to change', 'to change', 50.532034470703, 22.717752456665, '23-400'),
(31, 'Bełżyce', 'belzyce', '2013-12-20 09:41:54', NULL, 1, 'to change', 'to change', 'to change', 51.169011079452, 22.287139892578, '24-200'),
(33, 'Bielsk Podlaski', 'bielsk-podlaski-2', '2013-12-20 09:57:08', NULL, 1, 'to change', 'to change', 'to change', 52.782623487451, 23.20930480957, '17-100'),
(34, 'Hajnówka', 'hajnowka', '2013-12-20 10:02:19', NULL, 1, 'to change', 'to change', 'to change', 52.742943198857, 23.584899902344, '17-200'),
(35, 'Janów Lubelski', 'janow-lubelski', '2013-12-20 10:04:23', NULL, 1, 'to change', 'to change', 'to change', 50.707, 22.41, '23-300'),
(36, 'Ciechanowiec', 'ciechanowiec', '2013-12-20 10:13:30', NULL, 1, 'to change', 'to change', 'to change', 52.668054800688, 22.489013671875, '18-230'),
(37, 'Łapy', 'lapy', '2013-12-20 10:17:54', NULL, 1, 'to change', 'to change', 'to change', 52.988337253395, 22.8955078125, '18-100'),
(38, 'Międzyrzec Podlaski', 'miedzyrzec-podlaski', '2013-12-20 10:21:03', NULL, 1, 'to change', 'to change', 'to change', 51.98493300038, 22.784614562988, '21-560'),
(39, 'Ostrów Mazowiecka', 'ostrow-mazowiecka', '2013-12-20 10:24:35', NULL, 1, 'to change', 'to change', 'to change', 52.801931169682, 21.880645751953, '07-300'),
(40, 'Ryki', 'ryki', '2013-12-20 10:37:16', NULL, 1, 'to change', 'to change', 'to change', 51.624837461743, 21.95068359375, '08-500'),
(42, 'Sokołów Podlaski', 'sokolow-podlaski', '2013-12-20 10:53:16', NULL, 1, 'to change', 'to change', 'to change', 52.404801476675, 22.251777648926, '08-300'),
(43, 'Stoczek Łukowski', 'stoczek-lukowski', '2013-12-20 10:54:18', NULL, 1, 'to change', 'to change', 'to change', 51.956114798662, 21.980895996094, '21-450'),
(45, 'Wysokie Mazowieckie', 'wysokie-mazowieckie', '2013-12-20 11:05:59', NULL, 1, 'to change', 'to change', 'to change', 52.9174938735, 22.512702941895, '18-200'),
(47, 'Starachowice', 'starachowice', '2014-02-14 13:56:19', NULL, 1, 'to change', 'to change', 'to change', 51.042, 21.069, '27-200'),
(49, 'Kock', 'kock', '2014-03-07 16:05:25', NULL, 1, 'to change', 'to change', 'to change', 51.640487047911, 22.446484565735, '21-150'),
(50, 'Brańsk', 'bransk', '2014-07-15 08:11:27', NULL, 1, 'to change', 'to change', 'to change', 52.741514242043, 22.837872505188, '17-120'),
(52, 'Skarzysko Kamienna', 'skarzysko-kamienna', '2014-07-15 08:54:12', NULL, 1, 'to change', 'to change', 'to change', 51.103521942404, 20.857543945312, ''),
(53, 'Terespol', 'terespol', '2014-09-04 08:24:45', NULL, 1, 'to change', 'to change', 'to change', 52.074230817754, 23.614768981934, '21-550'),
(55, 'Kielce', 'kielce', '2015-02-24 14:16:34', NULL, 1, 'to change', 'to change', 'to change', 50.877, 20.643, ''),
(56, 'Garwolin', 'garwolin', '2015-07-22 11:27:46', NULL, 1, 'to change', 'to change', 'to change', 51.908325676217, 21.605215072632, '08-400'),
(57, 'Łochów', 'lochow', '2015-10-06 15:26:12', NULL, 1, 'to change', 'to change', 'to change', 52.530477649524, 21.681861877441, '07-130'),
(58, 'Parczew', 'parczew', '2015-10-30 11:40:59', NULL, 1, 'to change', 'to change', 'to change', 51.639, 22.902, '21-200'),
(59, 'Sandomierz', 'sandomierz', '2015-11-20 09:35:31', NULL, 1, 'to change', 'to change', 'to change', 50.679056768285, 21.745548248291, ''),
(60, 'Dęblin', 'deblin', '2015-12-04 11:56:22', NULL, 1, 'to change', 'to change', 'to change', 50.432798421168, 21.753959655762, ''),
(61, 'Radzyń Podlaski', 'radzyn-podlaski-1', '2017-04-03 09:38:55', NULL, 1, NULL, NULL, NULL, 51.782505064386, 22.615020498633, '21-300'),
(62, 'Gołdap ul.Królewiecka 11', 'goldap-ul-krolewiecka-11', '2017-04-05 15:10:38', NULL, 1, NULL, NULL, NULL, 54.306973675845, 22.303805798292, '19-500'),
(63, 'Ketrzyn ul.Kołobrzeska 1b/1', 'ketrzyn-ul-kolobrzeska-1b-1', '2017-04-05 15:14:19', NULL, 1, NULL, NULL, NULL, 54.080875632696, 21.36923097074, '11-400'),
(64, 'Białobrzegi ul.Królewiecka', 'bialobrzegi-ul-krolewiecka', '2017-04-05 15:18:28', NULL, 1, NULL, NULL, NULL, 51.648942236493, 20.971763134003, '26-800'),
(65, 'Suwałki', 'suwalki', '2017-04-05 15:20:52', NULL, 1, NULL, NULL, NULL, 54.109246333483, 22.930283993483, '16-400'),
(66, 'Olecko', 'olecko', '2017-04-05 15:23:10', NULL, 1, NULL, NULL, NULL, 54.037474758267, 22.506144270301, '19-400'),
(67, 'Augustów', 'augustow', '2017-04-05 15:25:24', NULL, 1, NULL, NULL, NULL, 53.841147864388, 22.984700649977, '16-300'),
(68, 'Zamość', 'zamosc', '2017-04-05 15:27:51', NULL, 1, NULL, NULL, NULL, 50.720047247713, 23.264493942261, '22-400'),
(69, 'Chełm', 'chelm-1', '2017-04-05 15:29:59', NULL, 1, NULL, NULL, NULL, 51.135000702598, 23.469129055738, '21-100'),
(70, 'Stalowa Wola', 'stalowa-wola', '2017-04-05 15:32:27', NULL, 1, NULL, NULL, NULL, 50.578884176811, 22.057807669044, '37-450'),
(71, 'Ełk', 'elk', '2017-04-05 15:33:48', NULL, 1, NULL, NULL, NULL, 53.828445789784, 22.369773387909, '19-300'),
(72, 'Mrągowo', 'mragowo', '2017-04-05 15:36:41', NULL, 1, NULL, NULL, NULL, 53.852982255126, 21.308455467224, '11-700'),
(73, 'Kock', 'kock-1', '2017-05-15 15:24:31', NULL, 1, NULL, NULL, NULL, 51.640694285456, 22.446234449744, '21-150'),
(74, 'Kock', 'kock-2', '2017-05-15 15:26:59', NULL, 1, NULL, NULL, NULL, 51.64403076278, 22.445941418409, '21-150'),
(75, 'Kock ul.Piłsudskiego', 'kock-ul-pilsudskiego', '2017-05-18 10:14:07', NULL, 1, NULL, NULL, NULL, 51.644084024582, 22.445769757032, '21-150'),
(76, 'Mrągowo', 'mragowo-1', '2017-05-18 10:18:07', NULL, 1, NULL, NULL, NULL, 53.866107093431, 21.305852383375, '11-700'),
(77, 'Białobrzegi', 'bialobrzegi', '2017-05-18 10:33:35', NULL, 1, NULL, NULL, NULL, 51.646579769211, 20.956599935889, '26-800'),
(78, 'Kętrzyn', 'ketrzyn', '2017-05-18 10:36:22', NULL, 1, NULL, NULL, NULL, 54.08052316757, 21.369574293494, '11-400'),
(79, 'Gołdap', 'goldap', '2017-05-18 10:39:08', NULL, 1, NULL, NULL, NULL, 54.308676267467, 22.30337664485, '19-500'),
(80, 'Dęblin', 'deblin-1', '2017-05-18 10:42:12', NULL, 1, NULL, NULL, NULL, 51.562573623701, 21.865210980177, '08-530'),
(81, 'Łuków', 'lukow-1', '2017-05-18 10:46:14', NULL, 1, NULL, NULL, NULL, 51.934490637713, 22.390580624342, '21-400'),
(82, 'Lublin', 'lublin-1', '2017-05-18 10:50:13', NULL, 1, NULL, NULL, NULL, NULL, NULL, '20-105'),
(83, 'Łuków', 'lukow-2', '2017-05-18 11:02:56', NULL, 1, NULL, NULL, NULL, 51.929323203678, 22.381732687354, '21-400'),
(84, 'Brańsk', 'bransk-1', '2017-05-18 11:06:55', NULL, 1, NULL, NULL, NULL, 52.741183789109, 22.83805154264, '17-120'),
(85, 'Hrubieszów', 'hrubieszow-1', '2017-05-18 11:10:47', NULL, 1, NULL, NULL, NULL, 50.807000034307, 23.893683180213, '22-500'),
(86, 'Sokołów Podlaski', 'sokolow-podlaski-1', '2017-05-18 11:13:50', NULL, 1, NULL, NULL, NULL, 52.405463378831, 22.25320123136, '08-300'),
(87, 'Siedlce', 'siedlce-1', '2017-05-18 11:17:02', NULL, 1, NULL, NULL, NULL, 52.166839925024, 22.276769131422, '08-110'),
(88, 'Włodawa', 'wlodawa-1', '2017-05-18 11:20:39', NULL, 1, NULL, NULL, NULL, 51.55130629882, 23.552248701453, '22-200'),
(89, 'Radzyń Podlaski', 'radzyn-podlaski-2', '2017-05-18 11:28:42', NULL, 1, NULL, NULL, NULL, 51.782213018877, 22.611329779029, '21-300'),
(90, 'Radzyń Podlaski', 'radzyn-podlaski-3', '2017-05-18 11:29:41', NULL, 1, NULL, NULL, NULL, 51.783443828549, 22.619737498462, '21-300'),
(92, 'Janów Lubelski', 'janow-lubelski-2', '2017-05-18 11:50:03', NULL, 1, NULL, NULL, NULL, 50.704809966105, 22.409112676978, '23-300'),
(93, 'Bielsk Podlaski', 'bielsk-podlaski-1', '2017-05-30 11:19:20', NULL, 1, NULL, NULL, NULL, 52.771541705867, 23.193198144436, '17-100'),
(94, 'Janów Lubelski', 'janow-lubelski-1', '2017-05-30 11:23:12', NULL, 1, NULL, NULL, NULL, 50.705380721004, 22.410013899207, '23-300'),
(95, 'Tomaszów Lubelski ul.Lwowska 14', 'tomaszow-lubelski-ul-lwowska-14', '2017-05-30 11:28:11', NULL, 1, NULL, NULL, NULL, 50.456371848631, 23.420806378126, '22-600'),
(96, 'Tomaszów Lubelski, ul.Lwowska 56', 'tomaszow-lubelski-ul-lwowska-56', '2017-05-30 11:29:33', NULL, 1, NULL, NULL, NULL, 50.439717425738, 23.419276177883, '22-600'),
(97, 'Tomaszów Lubelski, ul.Lwowska 19', 'tomaszow-lubelski-ul-lwowska-19', '2017-05-30 11:30:59', NULL, 1, NULL, NULL, NULL, 50.452944430395, 23.419619500637, '22-600'),
(98, 'Lublin ul.Zamojska', 'lublin-ul-zamojska', '2017-05-30 11:35:24', NULL, 1, NULL, NULL, NULL, 51.242375541672, 22.569022625685, '22-102');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `contact_form_message`
--

CREATE TABLE `contact_form_message` (
  `id` int(11) NOT NULL,
  `senderName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `senderEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `senderMessage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `senderIp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sentAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `contact_form_message`
--

INSERT INTO `contact_form_message` (`id`, `senderName`, `senderEmail`, `senderMessage`, `senderIp`, `sentAt`) VALUES
(17, 'Jacek Wasilewksi', 'fhurajw@interia.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam.Przesłałem Panstwu wiele ofert swojego importu.Prosze o zapoznanie się i kontakt.Pozdarwiam serdecznie. <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Jacek Wasilewksi<br />Nazwa firmy: Szczepan Gdansk<br', '89.65.5.44', '2014-11-27 17:39:42'),
(18, 'Alicja Walczak', 'stylizacjapaznokcianna@wp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam,a jak ja bym chciala zakupowac towar taniej od was na swoj sklep to jak moglabym zamawiac chcialam  sie troche podchrapac bo wynajmuje nie wielki lokalik i chcialabym zrobic ceny od 2 do 5 i nie tylko zl ', '178.181.71.210', '2014-12-09 10:35:48'),
(19, 'ogbifs@jstadp.com', 'ogbifs@jstadp.com', '<h3>Wiadomość z formularza kontaktowego:</h3>hnRm7x  <a href=\"http://urciwxkqyyug.com/\">urciwxkqyyug</a>, [url=http://xagwspihbxnz.com/]xagwspihbxnz[/url], [link=http://ignqiyruylib.com/]ignqiyruylib[/link], http://zvkwmfhaicwf.com/ <hr /><h3>Pozostałe da', '195.154.181.152', '2015-01-14 11:03:33'),
(20, 'Monika Przewoźniczuk', 'mika3088@vp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam, jestem zainteresowana   współpraca z Państwa Firmą na terenie Województwa Małopolskiego. Chciała bym się dowiedzieć na jakich zasadach przebiegała by wspólna współpraca. <hr /><h3>Pozostałe dane:</h3>Imi', '83.10.99.156', '2015-01-17 16:38:07'),
(21, 'andrzej krasa', 'krasaandrzej@wp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Bardzo proszę o przesłanie oferty towarowo-cenowej w hurcie. Proszę równeż o informację czy wysyłacie Państwo towar paczką. <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: andrzej krasa<br />Nazwa firmy: Magan<b', '95.40.115.72', '2015-01-21 21:55:51'),
(22, 'Lidiya Bielecka', 'lbielecka7@gmail.com', '<h3>Wiadomość z formularza kontaktowego:</h3>Pilnie poszukuje pracy. W ciągu ostatnich lat pracowałam w sklepach spożywczych. Przedtem przez rok pracowałam w butiku odzieżowym. Wykorzystałam ten czas na zdobycie nowych umiejętności. Jestem osobą ambitną i', '77.112.78.103', '2015-01-22 11:10:52'),
(23, 'Tomasz Charkot', 'thomas@tippy.dk', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam<br />\r\nW nawiązaniu do rozmowy telefoniczne, zwracam się z zapytaniem odnośnie nawiązania współpracy handlowej. Razem z moim wspólnikiem jesteśmy właścicielami 5 sklepów o łącznej powierzchni ok 7000 tys.', '5.103.205.180', '2015-02-04 12:29:17'),
(24, 'Monika Przewoźniczuk', 'mika3088@vp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam, jestem zainteresowana współpracą z Państwa firma. wielokrotnie byłam klientka Sklepu Ho-Ho w Sokołowie Podlaskim jak i w Siedlcach w województwie mazowieckim, byłam i jestem zadowolona z zakupionego tam ', '83.10.41.199', '2015-02-08 14:07:20'),
(25, 'ROT MARIUSZ', 'rocikk@interia.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Jestem zainteresowany współpracom z państwa firmą.Działalność prowadzę od 1998r sukcesywnie się rozwijając, posiadam nowo wybudowany lokal w miejscowości Bliżyn 26-120 o powierzchni 500 m2 w którym obecnie prow', '77.255.60.174', '2015-02-10 09:25:15'),
(26, 'marek', NULL, '<h3>Wiadomość z formularza kontaktowego:</h3>witam interesuje mnie współpraca prosiłbym o jakieś szczegółowe informacje co do w współpracy <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: marek<br />Nazwa firmy: <br />Telefon: 502894710<br />E-mail: <br />', '77.254.230.94', '2015-02-24 11:36:08'),
(27, 'MAGDALENA SAJDA-ZIEMKOWSKA', 'magdalena-sajda@go2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam, prowadzę sklepy z tej samej branży, jestem zainteresowana współpracą, proszę o kontakt Magdalena Sajda-Ziemkowska <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: MAGDALENA SAJDA-ZIEMKOWSKA<br />Nazwa firm', '95.40.203.4', '2015-03-13 12:47:02'),
(28, 'Izabela ryczek', 'Izabela44441@vp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Posiadam do wynajecia lokal w Swiebodzinie woj lubuskie .Lokal znajduje sie w Centrum miasta w bardzo atrakcyjnym punkcie,lokal świeżo po remoncie .110m. <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Izabela r', '78.10.57.3', '2015-03-19 17:35:53'),
(29, 'Izabela ryczek', 'Izabela44441@vp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Posiadam do wynajecia lokal w Swiebodzinie woj lubuskie .Lokal znajduje sie w Centrum miasta w bardzo atrakcyjnym punkcie,lokal świeżo po remoncie .110m. <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Izabela r', '78.10.57.3', '2015-03-19 17:36:01'),
(30, 'ANITA SZPONAR-ŁUBIARZ', 'anitka-1978@o2,pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam. Chciałabym otrzymać nr telefonu do sklepu w Lublinie jeżeli jest taka możliwość. Potrzebuję informacji czy jest na sklepie dywan, który widziałam w sklepie w Biłgoraju, a niestety nie zdążyłam go kupić. ', '178.42.180.148', '2015-03-20 23:29:29'),
(31, 'Krzyszstof Borkowski', 'agronet@vp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam <br />\r\nPosiadam lokal ok 30 m kw.<br />\r\nPoszukuję pomysłu na działalność wynajem lub współpraca. <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Krzyszstof Borkowski<br />Nazwa firmy: Agronet<br />Telefo', '83.2.152.220', '2015-04-13 07:59:28'),
(32, 'Żaneta', 'zaneta.zenka@onet.com', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam<br />\r\nMozna dokonac u Państwa zakupu przez internet? <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Żaneta<br />Nazwa firmy: Alrauna beauty<br />Telefon: 605915808<br />E-mail: zaneta.zenka@onet.com<br /', '31.0.0.203', '2015-04-29 18:29:00'),
(33, 'Żaneta Zenka', 'zaneta.zenka@onet.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam.Mozna u Panstwa dokonac zakupu przez internet?Mieszkam 30 km od końskich jednak nie mam jak dojechac.Istnieje mozliwosc zakupu z dowozem kurierskim? <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Żaneta Z', '31.0.0.203', '2015-04-29 18:30:14'),
(34, 'bogusia772', 'bogusia750@op.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>witam  chcialabym  zakupic ten lampion czy mozna zakupic przez internet , <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: bogusia772<br />Nazwa firmy: <br />Telefon: <br />E-mail: bogusia750@op.pl<br />', '46.45.107.33', '2015-05-04 20:05:50'),
(35, 'Lukasz Grzesiak', NULL, '<h3>Wiadomość z formularza kontaktowego:</h3>Zgodnie z dzisiejszą rozmowa telefoniczna proszę o kontakt, gdyż chcieliśmy wraz ze wspólnikiem porozmawiać o warunkach współpracy, związanych z otwarciem sklepu HO HO  w woj.lubelskim. Mamy już kilka potencjal', '83.24.246.116', '2015-06-10 11:16:37'),
(36, 'Anna Ciucka', 'ani216@wp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam proszę o kontakt w sprawie oferty pracy w sklepie ho ho w Hrubieszowie Czy oferta jest nadal aktualna Jestem zainteresowana Dziekuje <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Anna Ciucka<br />Nazwa f', '83.28.161.93', '2015-07-04 10:54:58'),
(37, 'Marcin Wiak', 'wiagal@gmail.com', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam,<br />\r\nChciałem zapytać czy jest możliwość współpracy z Państwa firma. Jestem zainteresowany prowadzeniem punktu handlowego opartego na państwa asortymencie lub podobnym. Czy mogliby Państwo przesłac mi ', '178.235.75.112', '2015-07-04 21:23:23'),
(38, 'Kamil Szumski', 'kamyl1985@o2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam<br />\r\n<br />\r\nNazywa się Kamil Szumski posiadam nowy lokal w miejscowości Łuków województwo Lubelskie(miasto powyżej 30 tys mieszkańców) chcę nawiązać współprace w celu wynajęcia lokalu.Lokal był budowan', '31.41.80.209', '2015-07-06 10:29:46'),
(39, 'Anna Ciucka', 'ani216@wp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Zwracam się z prośbą do pani Ani Przysadki o kontakt w sprawie oferty pracy Dzwoniła pani wczoraj do mnie i prosiła o wysłanie CV na pocztę emalia Oczywiście wysłałam choć nie wiem czy doszło bo miałam problem ', '83.28.168.83', '2015-07-08 10:29:09'),
(40, 'daniel misiurski', 'h2dpiekarnia@gmail.com', '<h3>Wiadomość z formularza kontaktowego:</h3>Proszę o ofertę współpracy.  Prowadzę firmę w miejscowości Wola uhruska . Posiadam lokal w centrum miejscowości. Pozdrawiam. <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: daniel misiurski<br />Nazwa firmy: h2d', '89.77.96.112', '2015-09-14 21:35:35'),
(41, 'daniel misiurski', 'h2dpiekarnia@gmail.com', '<h3>Wiadomość z formularza kontaktowego:</h3>proszę o propozycję współpracy. Posiadam własny lokal o powierzchnia  ok.70m2 w centrum miejscowości wola uhruska pow. włodawski <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: daniel misiurski<br />Nazwa firmy:', '5.226.84.252', '2015-09-22 22:56:30'),
(42, 'Urszula Kuźmicz', 'ula 1501@wp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Dzień dobry mam pytanie czy prowadzicie sprzedaż wysyłkową jeżeli tak to od jakiej sumy ma być zamówienie pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Urszula Kuźmicz<br />Nazwa firmy: PUH KRON<br ', '217.99.63.177', '2015-10-04 15:07:19'),
(43, 'Monika Franczak', 'monikanajs@wp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Poszukuje pracy w Lublinie jako sprzedawca w sklepie z artykulami po 5,99. <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Monika Franczak<br />Nazwa firmy: <br />Telefon: 883904916<br />E-mail: monikanajs@wp.pl', '46.113.239.87', '2015-10-08 12:47:42'),
(44, 'Pająk-Pawlik Natalia', 'natalia@mix.box.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam, od kilku tygodni zauważyłam, że dostarczają Państwo do swoich sklepów filmy dvd oraz bajki ale od jakiegoś czasu w nowym towarze ich nie ma. Czy jest możliwość by Państwo na zamówienie do danego sklepu d', '5.226.117.2', '2015-10-19 22:22:50'),
(45, 'kinga osial', 'kingaosial@o2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>czy u panstwa mozna nabyc art po cenach hurt do malych sklepikow prosze o kontakt <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: kinga osial<br />Nazwa firmy: kingaosial<br />Telefon: 505068616<br />E-mail: kin', '31.41.82.176', '2015-11-22 19:14:54'),
(46, 'Ewelina Piliszczuk', 'Elevelka@autograf.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Chciałabym nawiązać z państwem współpracę gdyż otwieram sklep o nazwie \"Upominek dla każdego\" i interesują mnie państwa produkty. Sklep będzie mieścił się w powiecie zamojskim. Czy mogłabym dostać jakąś ofertę?', '178.42.134.102', '2015-11-26 20:27:10'),
(47, 'Anna Łepik', 'gala29@o2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam serdecznie.<br />\r\nW dn.22.12.15 robilam zakupy w waszym punkcie prz ulicy 25 czerwca  rog slowacliego w Radomiu ,z przykrościa musze państwa poinformować iz obsługa ktora tam pracuje zachowuje sie skanda', '94.254.198.45', '2015-12-23 13:27:06'),
(48, 'Ewelina Jodełka', 'ewelinka397@tlen.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam chciałabym zapytac czy nie poszukuja Panstwo sprzedawcy do sklepu w Gawolinie..Poszukuje pracy od zaraz na okres mozliwie najdłuzszy.Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Ewelina Jodeł', '37.248.109.217', '2016-01-08 13:40:08'),
(49, 'Leszek', 'leszekolczyk@tlen.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam czy są u was jeszcze te długopisy metalowe czarne przekręcane za 2.99 sztuka ? <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Leszek<br />Nazwa firmy: <br />Telefon: <br />E-mail: leszekolczyk@tlen.pl<br ', '217.8.185.1', '2016-01-20 12:14:11'),
(50, 'Anna Łepik', 'gala29@o2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam serdecznie pisze do panstwa poraz drugi ,raz to byl chyba w Grudniu.w sprawie kobiety ktora ubliza klientom,podaje rysopis blondynka ok 170 cm szczupla  pracowala w piątek 22.01 na drugiej zmianie .prosze', '94.254.193.25', '2016-01-23 12:26:42'),
(51, 'sgsdbxg25gsd@agsdsd.com', 'sgsdbxg25gsd@agsdsd.com', '<h3>Wiadomość z formularza kontaktowego:</h3>o3kH9Q http://brothosonkonlonwon.ru <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: sgsdbxg25gsd@agsdsd.com<br />Nazwa firmy: sgsdbxg25gsd@agsdsd.com<br />Telefon: sgsdbxg25gsd@agsdsd.com<br />E-mail: sgsdbxg25g', '46.148.31.28', '2016-01-24 11:04:36'),
(52, 'Albert Durda', 'betomix@o2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam, piszę do państwa z zapytaniem czy macie stałego dostawce towaru czy jesteście otwarci na inne możliwości pozyskania towaru do waszej sieci sklepów? Mam wiele artykułów, które mogłyby was zainteresować. M', '79.185.91.84', '2016-02-24 19:38:26'),
(53, 'Paluch Violetta', 'vjm@interia.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>witam interesuje mnie zakup 9 sztuk artykułu cv114608/rs czy jest taka możliwość i jaka cena byłaby z przesyłką proszę o szybki kontakt sprawa jest pilna Pozdrawiam.proszę w razie w o numer konta i dane do prze', '79.190.84.10', '2016-02-29 23:22:18'),
(54, 'BURDACH IWONA', 'burdaszka 70 @o2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>proszę mi powiedzieć jakie są warunki zakupu abym mogła kupować wasz towar np. w woli osowińskiej <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: BURDACH IWONA<br />Nazwa firmy: sklep IWONA<br />Telefon: 6090417', '188.146.6.8', '2016-03-03 16:23:54'),
(55, 'BURDACH IWONA', 'burdaszka 70 @o2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>proszę mi powiedzieć jakie są warunki zakupu abym mogła kupować wasz towar np. w woli osowińskiej <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: BURDACH IWONA<br />Nazwa firmy: sklep IWONA<br />Telefon: 6090417', '188.146.6.8', '2016-03-03 16:23:59'),
(56, 'BURDACH IWONA', 'burdaszka 70 @o2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>proszę mi powiedzieć jakie są warunki zakupu abym mogła kupować wasz towar np. w woli osowińskiej <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: BURDACH IWONA<br />Nazwa firmy: sklep IWONA<br />Telefon: 6090417', '188.146.6.8', '2016-03-03 16:24:00'),
(57, 'aneta Siebor', 'aneta11.89@o2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam czy w niedziele handlowa jest czynny państwa sklep w Kozienicach ?<br />\r\nPozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: aneta Siebor<br />Nazwa firmy: <br />Telefon: <br />E-mail: aneta11.89@o', '213.216.100.157', '2016-03-16 19:22:18'),
(58, 'Renata Gorączka', 'rgoraczka@wp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam,<br />\r\nChciałam się dowiedzieć do której godziny czynny jest sklep z chińską odzieżą Ho Ho w Belżycach?<br />\r\nZ góry dziękuję za odpowiedź<br />\r\nPozdrawiam-Renata Gorączka <hr /><h3>Pozostałe dane:</h3', '185.6.53.2', '2016-06-29 13:24:32'),
(59, 'Dorota Hulak', 'dorotahulak@wp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3> <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Dorota Hulak<br />Nazwa firmy: <br />Telefon: 51158<br />E-mail: dorotahulak@wp.pl<br />', '94.254.225.112', '2016-06-29 14:25:46'),
(60, 'Dorota Hulak', 'dorotahulak@wp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam, jestem zaintesowana Panstwa ofertą odnośnie otwarcia Panstwa sklepu w postacji Franczyzy w miejscowości Zamość woj. Lubelskie. Jezeli byliby Państwo zainteresowani współpracą, prośze o kontakt, oraz ofer', '94.254.225.112', '2016-06-29 14:28:19'),
(61, 'SANJEEV DUDEJA', 'sanjeev@impactartexports.comts.com', '<h3>Wiadomość z formularza kontaktowego:</h3>We are one of the leading manufacturers & exporters of Home Furnishings, Textiles, Made-Ups & All Kinds of Floor Covering Products. We deals in Rugs, Carpets, Bath Mats, Throws, Towels, Bath Robes, Bed Covers, ', '49.200.118.93', '2016-08-11 13:59:13'),
(62, 'Eliza Zbyrasz', 'ezbyrasz@gmail.com', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam. Widziałam w waszym sklepie w Stalowej Woli dywanik łazienkowy z mikrofibry wyglądający jak dywanik z Ikei o nazwie TOFTBO - nie moge dodac tu zdjęcia. Byl on w cenie ok 23 zl - mam pytanie. Czy jest możl', '83.24.170.199', '2016-08-25 22:58:28'),
(63, 'katarzyna', 'tomos94@op. pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam mam nietypowe pytanie czy będą dostępne w sprzedaży gwiazdki brokatowe i takie renifery ażurowe brokatowe jak w tamtym roku?  Także doniczki kolorowe z motywem świątecznym takie fajne były bo musze zacząć', '37.47.30.227', '2016-09-05 19:14:57'),
(64, 'Małgorzata Oszczyk', 'm-oszczyk@o2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam, jestem zainteresowana talerzem do ciasta Tulipan w cenie 3,99 za sztukę. Tylko mam pytanie: czy mają Państwo do kompletu małe talerzyki i salaterki? Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię i nazwis', '95.160.154.98', '2016-10-12 09:19:58'),
(65, 'Guadalupe Abler', '28692Polovoy@hotmail.com', '<h3>Wiadomość z formularza kontaktowego:</h3> <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Guadalupe Abler<br />Nazwa firmy: Karissa Sapp<br />Telefon: <br />E-mail: 28692Polovoy@hotmail.com<br />', '62.210.111.96', '2016-11-05 19:14:11'),
(66, 'ANETA PISKAŁA', 'studio-kreator@wp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Posiadam sklep z upominkami, chciałabym wzbogacić ofertę o niektóre artykuły z Państwa asortymentu. Pozdrawiam Aneta Piskała <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: ANETA PISKAŁA<br />Nazwa firmy: KREATO', '91.235.125.51', '2016-11-09 16:29:55'),
(67, 'Damian Resiak', 'damres1@op.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam<br />\r\nPiszę do państwa w sprawię pozyskania informacji. Czy potrzebują państwo pracownika na stanowisku kierowcy bądź przedstawiciela handlowego? Mam 23 lata. Posiadam prawo jazdy od 4 lat. Dużo podróżuj', '5.172.255.112', '2016-12-16 14:47:01'),
(68, 'Mieczysław Wiszniewski', 'martawiszniewska-1979@wp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam,czy jest możliwość w hurtowni składania zamówień internetowych,jeśli tak to na jaką kwotę? Dziękuję za odpowiedz. <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Mieczysław Wiszniewski<br />Nazwa firmy: F.', '91.146.194.218', '2016-12-20 09:34:39'),
(69, 'Zyza Justyna', NULL, '<h3>Wiadomość z formularza kontaktowego:</h3>byłam klientkoa sklepu przy ul. Lwowskiej 19. w tomaszowie lub. przez swoja nie uwage podałam sprzedawczyni dwa banknoty 200zł. reszta została wydana tylko z 200 zł. drugi banknot nie zostałam mi zwrócony . uwa', '159.255.185.18', '2016-12-28 12:17:22'),
(70, 'Zyza Justyna', NULL, '<h3>Wiadomość z formularza kontaktowego:</h3>pisałam do państwa firmy w dniu 28.12. myslałam ze w związku z zaistniałą sytuacja jak mnie spotka w jednym z tomaszowskich sklepów sprawa zostanie przez państwa wyjasniona a przedewszystkim  zostana wprowadzon', '159.255.185.18', '2016-12-30 10:55:20'),
(71, 'Zyza Justyna', NULL, '<h3>Wiadomość z formularza kontaktowego:</h3>pisałam do państwa firmy w dniu 28.12. myslałam ze w związku z zaistniałą sytuacja jak mnie spotka w jednym z tomaszowskich sklepów sprawa zostanie przez państwa wyjasniona a przedewszystkim  zostana wprowadzon', '159.255.185.18', '2016-12-30 10:55:40'),
(72, 'justyna zyza', NULL, '<h3>Wiadomość z formularza kontaktowego:</h3>Witam serdecznie chciałabym uzyskac informacje odnosnie wyjasniania sytuacji jaka mnie spotkała w jednym z Państwa sklepów.? oczywiescie jeden z kierowników z kontakotówał sie ze mna ale niestety tylko w celu b', '159.255.185.18', '2017-01-09 11:00:21'),
(73, 'justyna zyza', NULL, '<h3>Wiadomość z formularza kontaktowego:</h3>Witam serdecznie chciałabym uzyskac informacje odnosnie wyjasniania sytuacji jaka mnie spotkała w jednym z Państwa sklepów.? oczywiescie jeden z kierowników z kontakotówał sie ze mna ale niestety tylko w celu b', '159.255.185.18', '2017-01-09 11:00:23'),
(74, 'Justyna Zyza', 'justynazyza@wp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Proszę o informacje, czy sprawa incydentu jaki miał miejsce w jednym z Tomszowskich sklepów prz ul. lwowskiej 19 w dn. 27.12.2016r. opisywanym prze ze mnie jest w jakis sposób wyjaśniana przez państwa firmę. Cz', '159.255.185.18', '2017-01-11 14:54:59'),
(75, 'Agnieszka Borysiak', 'gagapa@poczta.onet.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam<br />\r\nW sklepie w Sokołowie podlaskim kupiłam piękne osłonki na małe doniczki białe robione na szydełku i krochmalone. Potrzebuję jeszcze ok 8 - 10. Czy jest szansa kupić je u Państwa w jednym z sklepów?', '94.254.163.89', '2017-01-19 21:51:00'),
(76, 'Jolanta Kowalik', 'jolka25061987@interia.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Dobry wieczór mam pytanie odnośnie pracy w sklepie w Radzyniu Podlaskim . Można jeszcze składać cv? <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Jolanta Kowalik<br />Nazwa firmy: <br />Telefon: 513447325<br /', '178.36.8.145', '2017-01-28 20:53:17'),
(77, 'Marcin Wójtowicz', NULL, '<h3>Wiadomość z formularza kontaktowego:</h3> <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Marcin Wójtowicz<br />Nazwa firmy: <br />Telefon: <br />E-mail: <br />', '94.254.146.120', '2017-03-22 16:58:07'),
(78, 'Marcin Wójtowicz', NULL, '<h3>Wiadomość z formularza kontaktowego:</h3> <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Marcin Wójtowicz<br />Nazwa firmy: <br />Telefon: <br />E-mail: <br />', '94.254.146.120', '2017-03-22 16:58:11'),
(79, 'Marcin Wójtowicz', 'marcin@lemonadestudio.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Zapytanie testowe <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Marcin Wójtowicz<br />Nazwa firmy: <br />Telefon: 609212060<br />E-mail: marcin@lemonadestudio.pl<br />', '91.196.10.11', '2017-03-27 21:41:22'),
(80, 'Philipdex', 'jackyapril@gmail.com', '<h3>Wiadomość z formularza kontaktowego:</h3>http://stemmeries.xyz <a href=\"http://stemmeries.xyz\">norsk kasino</a> http://stemmeries.xyz - norsk kasino <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Philipdex<br />Nazwa firmy: Philipdex<br />Telefon: 829', '46.161.9.40', '2017-04-10 07:08:36'),
(81, 'EWA JABŁONOWSKA', 'ewasklep@interia.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam <br />\r\nChciałabym się zapytać w jaki sposób jest możliwość zamawiania u was produktów na fakturę  VAT czy muszę się gdzieś zarejestrować i czy są podawane gdzieś ceny hurtowe . <hr /><h3>Pozostałe dane:<', '217.99.251.230', '2017-04-18 20:03:36'),
(82, 'Michał skraburski', 'Chomik31@o2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Dzień dobry czy można u państwa zakupić długopisy żelowe GEL INK PEN? <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Michał skraburski<br />Nazwa firmy: Michał skraburski<br />Telefon: 603219341<br />E-mail: Ch', '94.254.242.111', '2017-04-20 11:44:30'),
(83, 'Michał skraburski', 'Chomik31@o2.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Dzień dobry czy można u państwa zakupić długopisy żelowe GEL INK PEN? <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Michał skraburski<br />Nazwa firmy: Michał skraburski<br />Telefon: 603219341<br />E-mail: Ch', '94.254.242.111', '2017-04-20 11:44:34'),
(84, 'Sailipdex', 'r8974jjil@gmail.com', '<h3>Wiadomość z formularza kontaktowego:</h3>http://undeclaiming.xyz <a href=\"http://undeclaiming.xyz\">norsk kasino</a> http://undeclaiming.xyz - norsk kasino <hr /><h3>Pozostałe dane:</h3>Imię i nazwisko: Sailipdex<br />Nazwa firmy: Sailipdex<br />Telefo', '46.161.9.35', '2017-05-14 00:08:13'),
(85, 'Zdzisław Ługowski', 'ladydii@vp.pl', '<h3>Wiadomość z formularza kontaktowego:</h3>Witam , posiadam lokal do wynajecia o pow.ok 60m2 plus zaplecze  w Starachowicach w województwie świętokrzyskim w bardzo dobrym miejscu w centrum w rynku ,wejście bezposrednio z ulicy , obecnie jest tam placowk', '94.254.139.180', '2017-05-24 15:05:46');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `doctor`
--

CREATE TABLE `doctor` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `fullName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `namePrefix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `doctor`
--

INSERT INTO `doctor` (`id`, `gallery_id`, `fullName`, `namePrefix`, `slug`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`, `content_short_generate`) VALUES
(2, NULL, 'Irena Karaś', 'Lek. stom.', 'irena-karas', 'to change', '<p>\r\n	lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;</p>', 'doctor-image-52a9e70255388.jpeg', 0, 'Irena Karaś', 'Irena, Karaś, lorem, ipsum lorem, ipsum', 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum', '2013-12-12 17:39:33', NULL, '2013-12-12 00:00:00', 0),
(3, NULL, 'Joanna Karaś', 'Lek. dent.', 'joanna-karas', 'to change', '<p>\r\n	llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;llorem ipsum&nbsp;</p>', 'doctor-image-52a9ea344e070.jpeg', 0, 'Joanna Karaś', 'Joanna, Karaś, llorem, ipsum llorem, ipsum', 'llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum llorem ipsum', '2013-12-12 17:54:05', NULL, '2013-12-12 00:00:00', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_group`
--

CREATE TABLE `fos_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`) VALUES
(1, 'admin', 'admin', 'marcin@lemonadestudio.pl', 'marcin@lemonadestudio.pl', 1, '8tzqynr944wsgkgosc4gsgogwccgwwc', 'gYW4zflhlCI5ojBvMUhTSMdG9e3MmlU4Ft6tVUVZMQQj06UUw4EqqFCVHmFnkvtwZkHO4w4YEIOLObpi8Nj7gA==', '2017-05-30 13:37:16', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 0, NULL, '2013-11-13 15:40:50', '2017-05-30 13:37:16', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(3, 'piotrek', 'piotrek', 'piotrek@lemonadestudio.pl', 'piotrek@lemonadestudio.pl', 1, 's6ru8tk0tlwww4kocskokgcwgoogg8s', 'BuNILmILotn5V25iTb7pv6ZmTpOwUj8fipxvT69PE8TpMRGOYZEwmuDIlOHqqQEeoVrC3NKITDa4GlJjU7cPpg==', '2013-12-31 14:59:14', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 0, NULL, '2013-12-09 18:21:30', '2013-12-31 14:59:14', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(5, 'hoho', 'hoho', 'sellmax@op.pl', 'sellmax@op.pl', 1, 'k7xyzonm1yosggc8g0osg84owwswckc', 'FXCmukUm8Cw9vaJFMA3VtUB6UoKTiuRfvkSPXHAowdr6ZM+bFyXFPKrvppkdsDd6UoBspSkyBZWiWgcZz+qE6w==', '2016-06-24 08:24:54', 0, 0, NULL, NULL, NULL, 'a:2:{i:0;s:17:\"ROLE_SONATA_ADMIN\";i:1;s:16:\"ROLE_SUPER_ADMIN\";}', 0, NULL, '2013-12-19 16:58:07', '2016-06-24 08:24:54', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(6, 'michal', 'michal', 'kontakt@luszczakmichal.com', 'kontakt@luszczakmichal.com', 1, '3r7aiflkpx2c8s88gkgoggkogokk0cg', '3r8dprpLVMubP3UpU92KsTcri0ADXCA7MOopFqyrApuMxvKcqLpLoZSh8DvzePvzeHyHCb2RbGex1b9Z9CWqXQ==', '2017-04-03 19:40:48', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', 0, NULL, '2017-03-16 15:10:10', '2017-04-03 19:40:48', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(7, 'agata', 'agata', 'biuroastek@op.pl', 'biuroastek@op.pl', 1, '3xzn7id6i16osc00w0oswk48008k88g', 'NIyb7quc9Fe0lEZ7d8wVtc8/uSkTwzrtbCFUG6xQxyxPSTUq/qbkS3MDzWCYFtQWzb5Mt0sTz/K3Qg+GeIQt9w==', '2017-05-30 12:54:07', 0, 0, NULL, NULL, NULL, 'a:2:{i:0;s:10:\"ROLE_ADMIN\";i:1;s:16:\"ROLE_SUPER_ADMIN\";}', 0, NULL, '2017-03-27 12:55:39', '2017-05-30 12:54:07', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user_user_group`
--

CREATE TABLE `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `on_list` tinyint(1) NOT NULL DEFAULT '0',
  `attachable` tinyint(1) NOT NULL DEFAULT '0',
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `gallery`
--

INSERT INTO `gallery` (`id`, `title`, `slug`, `on_list`, `attachable`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`) VALUES
(4, 'Radzyń Podlaski ul.Chomiczewskiego 15', 'radzyn-podlaski-ul-chomiczewskiego-15', 1, 1, 1, 'Radzyń Podlaski ul.Chomiczewskiego 15', NULL, NULL, '2013-12-20 20:50:56', NULL),
(5, 'Siedlce ul.Wojskowa 18', 'siedlce-ul-wojskowa-18', 1, 1, 1, 'Siedlce ul.Wojskowa 18', NULL, NULL, '2013-12-20 21:00:15', NULL),
(7, 'Mińsk Mazowiecki ul.Warszawska 111', 'minsk-mazowiecki-ul-warszawska-111', 1, 1, 1, 'Mińsk Mazowiecki ul.Warszawska 111', NULL, NULL, '2013-12-20 21:14:26', NULL),
(8, 'Bełżyce ul.Spółdzielcza2', 'belzyce-ul-spoldzielcza2', 1, 1, 1, 'Bełżyce ul.Spółdzielcza2', NULL, NULL, '2013-12-23 08:56:02', NULL),
(9, 'Ryki ul.Rynek Stary 1/5', 'ryki-ul-rynek-stary-1-5', 1, 1, 1, 'Ryki ul.Rynek Stary 1/5', NULL, NULL, '2013-12-23 09:09:07', NULL),
(11, 'Lublin ul.Wolska 111', 'lublin-ul-wolska-111', 1, 1, 1, 'Lublin ul.Wolska 111', NULL, NULL, '2013-12-23 09:18:14', NULL),
(12, 'Biłgoraj ul.Widok 4', 'bilgoraj-ul-widok-4', 1, 1, 1, 'Biłgoraj ul.Widok 4', NULL, NULL, '2013-12-23 09:23:25', NULL),
(13, 'Opole Lubelskie ul.Piekarska 4', 'opole-lubelskie-ul-piekarska-4', 1, 1, 1, 'Opole Lubelskie ul.Piekarska 4', NULL, NULL, '2013-12-23 09:26:24', NULL),
(14, 'Stoczek Łukowski Plac Kościuszki 2', 'stoczek-lukowski-plac-kosciuszki-2', 1, 1, 1, 'Stoczek Łukowski Plac Kościuszki 2', NULL, NULL, '2013-12-23 09:28:28', NULL),
(15, 'Bielsk Podlaski ul.Mickiewicza 38', 'bielsk-podlaski-ul-mickiewicza-38', 1, 1, 1, 'Bielsk Podlaski ul.Mickiewicza 38', NULL, NULL, '2013-12-23 09:50:04', NULL),
(16, 'Bielsk Podlaski ul.Szkolna 2A', 'bielsk-podlaski-ul-szkolna-2a', 1, 1, 1, 'Bielsk Podlaski ul.Szkolna 2A', NULL, NULL, '2013-12-23 09:53:22', NULL),
(17, 'Radzyń Podl.ul.Warszawska 63/67', 'radzyn-podl-ul-warszawska-63-67', 1, 1, 0, NULL, NULL, NULL, '2013-12-23 09:57:19', NULL),
(18, 'Siemiatycze ul.Jana Pawła II 14', 'siemiatycze-ul-jana-pawla-ii-14', 1, 1, 1, 'Siemiatycze ul.Jana Pawła II 14', NULL, NULL, '2013-12-23 09:59:11', NULL),
(19, 'Kozienice ul.Warszawska 34', 'kozienice-ul-warszawska-34', 1, 1, 1, 'Kozienice ul.Warszawska 34', NULL, NULL, '2013-12-23 10:01:40', NULL),
(20, 'Biłgoraj ul.Kościuszki 24', 'bilgoraj-ul-kosciuszki-24', 1, 1, 1, 'Biłgoraj ul.Kościuszki 24', NULL, NULL, '2013-12-23 10:05:44', NULL),
(21, 'Janów Lubelski ul.Wesoła 5', 'janow-lubelski-ul-wesola-5', 1, 1, 1, 'Janów Lubelski ul.Wesoła 5', NULL, NULL, '2013-12-23 10:08:29', NULL),
(22, 'Poniatowa ul.Przechodnia 39', 'poniatowa-ul-przechodnia-39', 1, 1, 1, 'Poniatowa ul.Przechodnia 39', NULL, NULL, '2013-12-23 10:12:47', NULL),
(24, 'Ryki ul.Warszawska 15', 'ryki-ul-warszawska-15', 1, 1, 1, 'Ryki ul.Warszawska 15', NULL, NULL, '2013-12-23 10:20:40', NULL),
(25, 'Tomaszów Lubelski ul.Lwowska 14', 'tomaszow-lubelski-ul-lwowska-14', 1, 1, 1, 'Tomaszów Lubelski ul.Lwowska 14', NULL, NULL, '2013-12-23 10:23:33', NULL),
(26, 'Włodawa ul.Czerwonego Krzyża 14a', 'wlodawa-ul-czerwonego-krzyza-14a', 1, 1, 1, 'Włodawa ul.Czerwonego Krzyża 14a', NULL, NULL, '2013-12-23 10:27:23', NULL),
(27, 'Włodawa ul.Żeromskiego 23', 'wlodawa-ul-zeromskiego-23', 1, 1, 1, 'Włodawa ul.Żeromskiego 23', NULL, NULL, '2013-12-27 13:14:02', NULL),
(28, 'Biała Podlaska ul.Szkolny Dwór 10', 'biala-podlaska-ul-szkolny-dwor-10', 1, 1, 1, 'Biała Podlaska ul.Szkolny Dwór 10', NULL, NULL, '2013-12-27 13:37:22', NULL),
(29, 'Biała Podlaska ul.Prosta 4', 'biala-podlaska-ul-prosta-4', 1, 1, 1, 'Biała Podlaska ul.Prosta 4', NULL, NULL, '2013-12-27 13:37:45', NULL),
(30, 'Chełm ul.Lubelska 17', 'chelm-ul-lubelska-17', 1, 1, 1, 'Chełm ul.Lubelska 17', NULL, NULL, '2013-12-27 13:38:10', NULL),
(31, 'Ciechanowiec ul.Plac Ks.Kluka 3a', 'ciechanowiec-ul-plac-ks-kluka-3a', 1, 1, 1, 'Ciechanowiec ul.Plac Ks.Kluka 3a', NULL, NULL, '2013-12-27 13:38:36', NULL),
(32, 'Hajnówka ul.Wierobieja 2', 'hajnowka-ul-wierobieja-2', 1, 1, 1, 'Hajnówka ul.Wierobieja 2', NULL, NULL, '2013-12-27 13:39:08', NULL),
(33, 'Hrubieszów ul.Narutowicza 54', 'hrubieszow-ul-narutowicza-54', 1, 1, 1, 'Hrubieszów ul.Narutowicza 54', NULL, NULL, '2013-12-27 13:40:08', NULL),
(34, 'Krasnystaw ul.Okszei 8', 'krasnystaw-ul-okszei-8', 1, 1, 1, 'Krasnystaw ul.Okszei 8', NULL, NULL, '2013-12-27 13:40:29', NULL),
(35, 'Kraśnik ul.Urzędowska 36', 'krasnik-ul-urzedowska-36', 1, 1, 1, 'Kraśnik ul.Urzędowska 36', NULL, NULL, '2013-12-27 13:40:53', NULL),
(36, 'Kraśnik ul.Lubelska 46', 'krasnik-ul-lubelska-46', 1, 1, 1, 'Kraśnik ul.Lubelska 46', NULL, NULL, '2013-12-27 13:41:15', NULL),
(37, 'Lipsko ul.Zwoleńska 8 A', 'lipsko-ul-zwolenska-8-a', 1, 1, 0, NULL, NULL, NULL, '2013-12-27 13:41:41', NULL),
(38, 'Lubartów ul.Legionów 37', 'lubartow-ul-legionow-37', 1, 1, 1, 'Lubartów ul.Legionów 37', NULL, NULL, '2013-12-27 13:42:09', NULL),
(39, 'Łapy ul.Główna 1', 'lapy-ul-glowna-1', 1, 1, 1, 'Łapy ul.Główna 1', NULL, NULL, '2013-12-27 13:42:33', NULL),
(40, 'Łapy ul.Sikorskiego 38', 'lapy-ul-sikorskiego-38', 1, 1, 1, 'Łapy ul.Sikorskiego 38', NULL, NULL, '2013-12-27 13:42:52', NULL),
(41, 'Łosice ul.Rynek 3', 'losice-ul-rynek-3', 1, 1, 1, 'Łosice ul.Rynek 3', NULL, NULL, '2013-12-27 13:43:13', NULL),
(42, 'Łuków ul.Wyszyńskiego 10/17', 'lukow-ul-wyszynskiego-10-17', 1, 1, 1, 'Łuków ul.Wyszyńskiego 10/17', NULL, NULL, '2013-12-27 13:43:34', NULL),
(43, 'Międzyrzec Podlaski ul.Brzeska 23', 'miedzyrzec-podlaski-ul-brzeska-23', 1, 1, 1, 'Międzyrzec Podlaski ul.Brzeska 23', NULL, NULL, '2013-12-27 13:43:58', NULL),
(44, 'Międzyrzec Podlaski ul.Warszawska 29', 'miedzyrzec-podlaski-ul-warszawska-29', 1, 1, 1, 'Międzyrzec Podlaski ul.Warszawska 29', NULL, NULL, '2013-12-27 13:44:23', NULL),
(45, 'Ostrowiec Świętokrzyski ul.Rynek 50b', 'ostrowiec-swietokrzyski-ul-rynek-50b', 1, 1, 1, 'Ostrowiec Świętokrzyski ul.Rynek 50b', NULL, NULL, '2013-12-27 13:44:53', NULL),
(46, 'Ostrów Mazowiecka ul.3 maja 9', 'ostrow-mazowiecka-ul-3-maja-9', 1, 1, 1, 'Ostrów Mazowiecka ul.3 maja 9', NULL, NULL, '2013-12-27 13:45:33', NULL),
(47, 'Radom ul.Krauza 17a', 'radom-ul-krauza-17a', 1, 1, 1, 'Radom ul.Krauza 17a', NULL, NULL, '2013-12-27 13:46:00', NULL),
(48, 'Radzyń PodL. UL.Pocztowa 4', 'radzyn-podl-ul-pocztowa-4', 1, 1, 1, 'Radzyń PodL. UL.Pocztowa 4', NULL, NULL, '2013-12-27 13:46:38', NULL),
(50, 'Sokołów Podlaski ul.Magistracka 1', 'sokolow-podlaski-ul-magistracka-1', 1, 1, 1, 'Sokołów Podlaski ul.Magistracka 1', NULL, NULL, '2013-12-27 13:47:29', NULL),
(51, 'Szydłowiec ul.Staszica 19a', 'szydlowiec-ul-staszica-19a', 1, 1, 1, 'Szydłowiec ul.Staszica 19a', NULL, NULL, '2013-12-27 13:47:53', NULL),
(52, 'Tomaszów Lubelski ul.Lwowska 19', 'tomaszow-lubelski-ul-lwowska-19', 1, 1, 1, 'Tomaszów Lubelski ul.Lwowska 19', NULL, NULL, '2013-12-27 13:48:50', NULL),
(53, 'Tomaszów Lubelski ul.Lwowska 56', 'tomaszow-lubelski-ul-lwowska-56', 1, 1, 1, 'Tomaszów Lubelski ul.Lwowska 56', NULL, NULL, '2013-12-27 13:49:15', NULL),
(54, 'Węgrów ul.Rynkowa 3', 'wegrow-ul-rynkowa-3', 1, 1, 1, 'Węgrów ul.Rynkowa 3', NULL, NULL, '2013-12-27 13:49:49', NULL),
(55, 'Wysokie Mazowieckie ul.Jagielońska 35', 'wysokie-mazowieckie-ul-jagielonska-35', 1, 1, 1, 'Wysokie Mazowieckie ul.Jagielońska 35', NULL, NULL, '2013-12-27 13:50:19', NULL),
(56, 'HOHO', 'hoho', 1, 1, 1, 'HOHO', NULL, NULL, '2014-01-03 09:20:20', NULL),
(57, 'Siedlce ul.Pułaskiego 10', 'siedlce-ul-pulaskiego-10', 1, 1, 0, NULL, NULL, NULL, '2014-01-28 13:42:47', NULL),
(58, 'Kock ul.1 maja 11', 'kock-ul-1-maja-11', 1, 1, 0, NULL, NULL, NULL, '2014-03-07 16:06:35', NULL),
(59, 'Starachowice ul.Niska 6', 'starachowice-ul-niska-6', 1, 1, 0, NULL, NULL, NULL, '2014-03-13 09:10:54', NULL),
(60, 'Skarżysko Kamienna ul.Krasińskiego 20', 'skarzysko-kamienna-ul-krasinskiego-20', 1, 1, 0, NULL, NULL, NULL, '2014-06-02 11:32:14', NULL),
(61, 'Gazetka', 'gazetka', 0, 1, 0, NULL, NULL, NULL, '2014-06-13 18:20:33', NULL),
(62, 'Hrubieszów ul.Ludna 9', 'hrubieszow-ul-ludna-9', 1, 1, 0, NULL, NULL, NULL, '2014-06-18 08:20:34', NULL),
(63, 'Brańsk ul.Rynek 43', 'bransk-ul-rynek-43', 1, 1, 0, NULL, NULL, NULL, '2014-07-15 08:12:00', NULL),
(65, 'Terespol ul.Wojska Polskiego 134', 'terespol-ul-wojska-polskiego-134', 1, 1, 0, NULL, NULL, NULL, '2014-09-04 08:27:22', NULL),
(66, 'Łuków ul.Staropijarska 20 piętro 1', 'lukow-ul-staropijarska-20-pietro-1', 1, 1, 0, NULL, NULL, NULL, '2014-09-23 08:47:16', NULL),
(67, 'Ostrowiec Świętokrzyski ul.Górzysta 6', 'ostrowiec-swietokrzyski-ul-gorzysta-6', 1, 1, 0, NULL, NULL, NULL, '2015-02-03 12:05:25', NULL),
(69, 'Kielce ul.Sienkiewicza 68', 'kielce-ul-sienkiewicza-68', 1, 1, 0, NULL, NULL, NULL, '2015-02-24 14:21:23', NULL),
(70, 'Puławy UL.Piłsudskiego 24', 'pulawy-ul-pilsudskiego-24', 1, 1, 0, NULL, NULL, NULL, '2015-07-28 14:41:51', NULL),
(71, 'Garwolin ul.Kościuszki 1', 'garwolin-ul-kosciuszki-1', 1, 1, 0, NULL, NULL, NULL, '2015-08-07 14:01:37', NULL),
(72, 'Łochów ul.1 maja 2 piętro 1', 'lochow-ul-1-maja-2-pietro-1', 1, 1, 0, NULL, NULL, NULL, '2015-10-06 15:27:36', NULL),
(73, 'Lublin ul.Zamojska 12', 'lublin-ul-zamojska-12', 1, 1, 0, NULL, NULL, NULL, '2015-10-06 15:56:58', NULL),
(74, 'Radom ul.Słowackiego 19', 'radom-ul-slowackiego-19', 1, 1, 0, NULL, NULL, NULL, '2015-10-09 08:42:22', NULL),
(75, 'Łuków ul.Międzyrzecka 6 a', 'lukow-ul-miedzyrzecka-6-a', 1, 1, 0, NULL, NULL, NULL, '2015-10-09 08:45:19', NULL),
(76, 'Parczew ul.Kościelna 54 A', 'parczew-ul-koscielna-54-a', 1, 1, 0, NULL, NULL, NULL, '2015-10-30 11:41:39', NULL),
(77, 'Kraśnik Fabryczny', 'krasnik-fabryczny', 1, 1, 0, NULL, NULL, NULL, '2015-11-20 09:33:28', NULL),
(78, 'Sandomierz ul.Mickiewicza 46 b', 'sandomierz-ul-mickiewicza-46-b', 1, 1, 0, NULL, NULL, NULL, '2015-11-20 09:39:06', NULL),
(79, 'Dęblin ul.Warszawska 34', 'deblin-ul-warszawska-34', 1, 1, 0, NULL, NULL, NULL, '2015-12-04 11:56:59', NULL),
(80, 'Międzyrzec Podlaski ul.Lubelska 6', 'miedzyrzec-podlaski-ul-lubelska-6', 1, 1, 0, NULL, NULL, NULL, '2016-04-28 12:22:03', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_photo`
--

CREATE TABLE `gallery_photo` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `gallery_photo`
--

INSERT INTO `gallery_photo` (`id`, `gallery_id`, `photo`, `arrangement`) VALUES
(67, 4, 'gallery-image-52b7ea8be9b08.jpeg', 1),
(68, 4, 'gallery-image-52b7ea9b9290b.jpeg', 2),
(71, 4, 'gallery-image-52b7eacd87488.jpeg', 3),
(72, 4, 'gallery-image-52b7eb0fde4b9.jpeg', 4),
(73, 4, 'gallery-image-52b7eb2124226.jpeg', 5),
(74, 4, 'gallery-image-52b7eb5219437.jpeg', 6),
(77, 5, 'gallery-image-52b7ebfcdb5fa.jpeg', 1),
(78, 5, 'gallery-image-52b7ec0b7d1d0.jpeg', 2),
(79, 5, 'gallery-image-52b7ec1b14313.jpeg', 3),
(81, 8, 'gallery-image-52b7edaf5cc74.jpeg', 1),
(82, 8, 'gallery-image-52b7edc0a9759.jpeg', 2),
(83, 9, 'gallery-image-52b7efc1ca90e.jpeg', 1),
(84, 9, 'gallery-image-52b7efdb5a414.jpeg', 2),
(91, 7, 'gallery-image-52b7f167bbda3.jpeg', 1),
(92, 11, 'gallery-image-52b7f232d8eee.jpeg', 2),
(94, 11, 'gallery-image-52b7f256be0e6.jpeg', 3),
(96, 11, 'gallery-image-52b7f27e7f743.jpeg', 4),
(97, 12, 'gallery-image-52b7f31b97925.jpeg', 5),
(98, 12, 'gallery-image-52b7f3332c31e.jpeg', 4),
(99, 12, 'gallery-image-52b7f345664f7.jpeg', 1),
(100, 13, 'gallery-image-52b7f3cb1aa13.jpeg', 1),
(101, 13, 'gallery-image-52b7f3dd730d8.jpeg', 2),
(102, 14, 'gallery-image-52b7f4433f337.jpeg', 1),
(105, 15, 'gallery-image-52b7f99808add.jpeg', 1),
(106, 15, 'gallery-image-52b7f9a52515e.jpeg', 2),
(107, 16, 'gallery-image-52b7fa1bbb902.jpeg', 1),
(111, 18, 'gallery-image-52b7fb7188550.jpeg', 1),
(112, 18, 'gallery-image-52b7fb85a8be0.jpeg', 2),
(113, 18, 'gallery-image-52b7fb91cf8b7.jpeg', 3),
(117, 14, 'gallery-image-52b7fc82736f0.jpeg', 2),
(118, 14, 'gallery-image-52b7fc8f644ec.jpeg', 3),
(119, 20, 'gallery-image-52b7fd0a1546c.jpeg', 1),
(120, 20, 'gallery-image-52b7fd1ba45c3.jpeg', 2),
(121, 20, 'gallery-image-52b7fd29ce29a.jpeg', 3),
(122, 21, 'gallery-image-52b7fdadeab4b.jpeg', 1),
(123, 21, 'gallery-image-52b7fdbea2848.jpeg', 2),
(124, 21, 'gallery-image-52b7fdd6a4ef1.jpeg', 3),
(125, 22, 'gallery-image-52b7feaa31026.jpeg', 1),
(126, 22, 'gallery-image-52b7feb86500a.jpeg', 2),
(127, 22, 'gallery-image-52b7fec7dac33.jpeg', 3),
(128, 22, 'gallery-image-52b7fed8afe66.jpeg', 4),
(129, 22, 'gallery-image-52b7fee96b255.jpeg', 5),
(135, 24, 'gallery-image-52b80085ebb80.jpeg', 1),
(136, 24, 'gallery-image-52b8009601b04.jpeg', 2),
(137, 24, 'gallery-image-52b800a89dcaf.jpeg', 3),
(139, 25, 'gallery-image-52b80146d4dd6.jpeg', 1),
(140, 25, 'gallery-image-52b80158de236.jpeg', 2),
(141, 25, 'gallery-image-52b8016cce237.jpeg', 3),
(142, 25, 'gallery-image-52b8017e7265a.jpeg', 4),
(143, 25, 'gallery-image-52b8018e984cc.jpeg', 5),
(144, 26, 'gallery-image-52b8021109ed4.jpeg', 1),
(145, 26, 'gallery-image-52b8021f298e6.jpeg', 2),
(148, 27, 'gallery-image-52bd6f1e8502e.jpeg', 1),
(155, 56, 'gallery-image-52c672fc59983.jpeg', 1),
(156, 51, 'gallery-image-52d401a6479a1.jpeg', 1),
(157, 51, 'gallery-image-52d4022e96f5d.jpeg', 2),
(158, 51, 'gallery-image-52d4026354271.jpeg', 3),
(160, 45, 'gallery-image-52d68487a44e3.jpeg', 1),
(161, 45, 'gallery-image-52d684963a34f.jpeg', 2),
(162, 45, 'gallery-image-52d684abc9467.jpeg', 3),
(166, 33, 'gallery-image-52d68534ca8fc.jpeg', 1),
(167, 33, 'gallery-image-52d68545dc714.jpeg', 2),
(168, 33, 'gallery-image-52d685540d136.jpeg', 3),
(169, 33, 'gallery-image-52d6856986dd0.jpeg', 4),
(170, 52, 'gallery-image-52d6859649bb3.jpeg', 1),
(171, 52, 'gallery-image-52d685a272fe2.jpeg', 2),
(172, 52, 'gallery-image-52d685af78d03.jpeg', 3),
(173, 53, 'gallery-image-52d685cc72535.jpeg', 1),
(174, 53, 'gallery-image-52d685db2328c.jpeg', 2),
(175, 53, 'gallery-image-52d685e7ebfbd.jpeg', 3),
(176, 43, 'gallery-image-52e0d30220fb4.jpeg', 1),
(177, 43, 'gallery-image-52e0d3198be3f.jpeg', 2),
(178, 43, 'gallery-image-52e0d38dea4d0.jpeg', 3),
(179, 43, 'gallery-image-52e0d39ad92dc.jpeg', 4),
(180, 43, 'gallery-image-52e0d3aa4045b.jpeg', 5),
(181, 43, 'gallery-image-52e0d3b6aa48d.jpeg', 6),
(182, 43, 'gallery-image-52e0d3c45a2b1.jpeg', 7),
(183, 43, 'gallery-image-52e0d3d2e9bef.jpeg', 8),
(184, 32, 'gallery-image-52e0d6b35eab4.jpeg', 2),
(185, 32, 'gallery-image-52e0d6c1e63b2.jpeg', 3),
(186, 32, 'gallery-image-52e0d6d1c97a0.jpeg', 4),
(187, 44, 'gallery-image-52e0d8834dcbb.jpeg', 1),
(188, 44, 'gallery-image-52e0d8905525a.jpeg', 2),
(189, 44, 'gallery-image-52e0d89f9417d.jpeg', 3),
(190, 28, 'gallery-image-52e0da15b8015.jpeg', 1),
(191, 28, 'gallery-image-52e0da22cb05d.jpeg', 2),
(192, 28, 'gallery-image-52e0da2fc1ca1.jpeg', 3),
(193, 28, 'gallery-image-52e0da3c285ad.jpeg', 4),
(194, 57, 'gallery-image-52e7a5e65cbfe.jpeg', 1),
(195, 57, 'gallery-image-52e7a5efdf1a7.jpeg', 2),
(196, 57, 'gallery-image-52e7a5f8f106a.jpeg', 3),
(197, 57, 'gallery-image-52e7a60c6d71f.jpeg', 4),
(198, 57, 'gallery-image-52e7a6159dfb9.jpeg', 5),
(200, 57, 'gallery-image-52e7a6284f01b.jpeg', 6),
(203, 51, 'gallery-image-52f885ea7d4f6.jpeg', 4),
(207, 59, 'gallery-image-5321684ac7d25.jpeg', 1),
(208, 59, 'gallery-image-5321685aa3275.jpeg', 2),
(209, 59, 'gallery-image-5321686b02d51.jpeg', 3),
(210, 59, 'gallery-image-53216885a5ffa.jpeg', 4),
(211, 24, 'gallery-image-53565fa6c54d0.jpeg', 4),
(212, 24, 'gallery-image-5356602e20e5e.jpeg', 5),
(213, 24, 'gallery-image-5356611507907.jpeg', 6),
(214, 32, 'gallery-image-53578f9c8b894.jpeg', 1),
(216, 32, 'gallery-image-53578fd21e0ea.jpeg', 5),
(217, 32, 'gallery-image-53578fe6d356e.jpeg', 6),
(218, 32, 'gallery-image-535790014aa3b.jpeg', 7),
(220, 32, 'gallery-image-5357903b0adad.jpeg', 8),
(221, 15, 'gallery-image-535793f77c4a5.jpeg', 3),
(222, 15, 'gallery-image-5357940985505.jpeg', 4),
(223, 15, 'gallery-image-5357941c94d19.jpeg', 5),
(224, 15, 'gallery-image-53579452e3b9b.jpeg', 6),
(226, 15, 'gallery-image-535794a0dea73.jpeg', 7),
(227, 15, 'gallery-image-535794cc8ba8f.jpeg', 8),
(229, 16, 'gallery-image-535795045443f.jpeg', 2),
(230, 16, 'gallery-image-5357951736b06.jpeg', 3),
(231, 16, 'gallery-image-53579529a4b19.jpeg', 4),
(233, 16, 'gallery-image-5357955db78f2.jpeg', 5),
(234, 16, 'gallery-image-5357957a507b4.jpeg', 6),
(235, 58, 'gallery-image-53760d0969fc3.jpeg', 1),
(236, 58, 'gallery-image-53760d16d9e23.jpeg', 2),
(237, 58, 'gallery-image-53760d25238da.jpeg', 3),
(238, 58, 'gallery-image-53760d376b7cb.jpeg', 4),
(239, 58, 'gallery-image-53760d4c2d503.jpeg', 5),
(240, 58, 'gallery-image-53760d596db50.jpeg', 6),
(242, 26, 'gallery-image-5376109e3d2ce.jpeg', 3),
(243, 26, 'gallery-image-537610ae31c9b.jpeg', 4),
(244, 26, 'gallery-image-537610bb72445.jpeg', 5),
(245, 26, 'gallery-image-537610c938eeb.jpeg', 6),
(246, 26, 'gallery-image-537610d6bc0d6.jpeg', 7),
(247, 26, 'gallery-image-537610f471e9f.jpeg', 8),
(248, 26, 'gallery-image-537611065a868.jpeg', 9),
(249, 26, 'gallery-image-5376111274b54.jpeg', 11),
(250, 26, 'gallery-image-537611555c05a.jpeg', 12),
(253, 55, 'gallery-image-537614d24ba88.jpeg', 1),
(254, 55, 'gallery-image-537614df78b67.jpeg', 2),
(255, 55, 'gallery-image-537614ed1694e.jpeg', 3),
(256, 55, 'gallery-image-537614fef0a8b.jpeg', 4),
(257, 55, 'gallery-image-5376150f6aad8.jpeg', 5),
(258, 55, 'gallery-image-5376151c72195.jpeg', 6),
(259, 55, 'gallery-image-5376152f20349.jpeg', 7),
(260, 55, 'gallery-image-5376153ea0e22.jpeg', 8),
(261, 55, 'gallery-image-53761559bcea3.jpeg', 9),
(262, 27, 'gallery-image-537616ea1f0e4.jpeg', 2),
(263, 27, 'gallery-image-537616f6ee416.jpeg', 3),
(264, 27, 'gallery-image-537617036458b.jpeg', 4),
(265, 27, 'gallery-image-5379a84e36cba.jpeg', 5),
(273, 13, 'gallery-image-5379b4a3cca43.jpeg', 3),
(274, 13, 'gallery-image-5379b4b1e475e.jpeg', 4),
(275, 13, 'gallery-image-5379b4c1b5736.jpeg', 5),
(276, 13, 'gallery-image-5379b4d80493e.jpeg', 6),
(277, 13, 'gallery-image-5379b4e7866eb.jpeg', 7),
(278, 13, 'gallery-image-5379b4f7b5e58.jpeg', 8),
(279, 13, 'gallery-image-5379b506ae37a.jpeg', 9),
(280, 22, 'gallery-image-5379b53639673.jpeg', 6),
(281, 22, 'gallery-image-5379b547015cb.jpeg', 7),
(282, 22, 'gallery-image-5379b5553bd5f.jpeg', 8),
(283, 22, 'gallery-image-5379b56be558c.jpeg', 9),
(293, 9, 'gallery-image-5379b87e89d29.jpeg', 3),
(295, 9, 'gallery-image-5379b8a69d8d4.jpeg', 4),
(296, 9, 'gallery-image-5379b8babb725.jpeg', 5),
(297, 9, 'gallery-image-5379b8d06f282.jpeg', 6),
(298, 9, 'gallery-image-5379b8e1e2771.jpeg', 7),
(299, 9, 'gallery-image-5379b8f42604c.jpeg', 8),
(300, 29, 'gallery-image-5379be1329812.jpeg', 1),
(301, 29, 'gallery-image-5379be2053749.jpeg', 2),
(302, 29, 'gallery-image-5379be2e4efcb.jpeg', 3),
(303, 29, 'gallery-image-5379be3c7416f.jpeg', 4),
(304, 29, 'gallery-image-5379be490ca65.jpeg', 5),
(305, 29, 'gallery-image-5379be59535e1.jpeg', 6),
(306, 28, 'gallery-image-5379bece6c672.jpeg', 5),
(307, 28, 'gallery-image-5379bedb877f3.jpeg', 6),
(308, 12, 'gallery-image-5383085bed908.jpeg', 2),
(309, 12, 'gallery-image-5383086c2a797.jpeg', 3),
(310, 12, 'gallery-image-538308867f795.jpeg', 6),
(311, 12, 'gallery-image-5383089d299a0.jpeg', 7),
(312, 12, 'gallery-image-538308b51186a.jpeg', 8),
(313, 12, 'gallery-image-538308ed6510d.jpeg', 9),
(314, 12, 'gallery-image-53830954bf371.jpeg', 10),
(315, 12, 'gallery-image-5383096cc152e.jpeg', 11),
(316, 12, 'gallery-image-5383097d90597.jpeg', 12),
(317, 11, 'gallery-image-538309f1d612c.jpeg', 1),
(318, 11, 'gallery-image-53830a2ba95b8.jpeg', 5),
(319, 11, 'gallery-image-53830a3ccaf94.jpeg', 6),
(320, 11, 'gallery-image-53830a4b1e34f.jpeg', 7),
(321, 11, 'gallery-image-53830a6758fb3.jpeg', 8),
(322, 11, 'gallery-image-53830a7a8c816.jpeg', 9),
(323, 11, 'gallery-image-53830a93931a0.jpeg', 10),
(324, 11, 'gallery-image-53830aa3b5935.jpeg', 11),
(325, 11, 'gallery-image-53830ab4a505d.jpeg', 12),
(326, 11, 'gallery-image-53830ace56356.jpeg', 13),
(327, 11, 'gallery-image-53830adfd7ebd.jpeg', 14),
(329, 11, 'gallery-image-53830b06dd941.jpeg', 15),
(330, 11, 'gallery-image-53830b17f33e2.jpeg', 16),
(331, 14, 'gallery-image-53830b96dc5bd.jpeg', 4),
(332, 14, 'gallery-image-53830baa0fed8.jpeg', 5),
(333, 14, 'gallery-image-53830bba4a91a.jpeg', 6),
(334, 14, 'gallery-image-53830bcb3f03e.jpeg', 7),
(335, 14, 'gallery-image-53830be27396b.jpeg', 8),
(336, 14, 'gallery-image-53830bf9486e2.jpeg', 9),
(337, 7, 'gallery-image-53830c499dab1.jpeg', 2),
(338, 7, 'gallery-image-53830c5a3b8d2.jpeg', 3),
(339, 7, 'gallery-image-53830c6761903.jpeg', 4),
(340, 7, 'gallery-image-53830c78b0dfb.jpeg', 5),
(341, 7, 'gallery-image-53830e933a3e5.jpeg', 6),
(342, 42, 'gallery-image-53830ebbd5195.jpeg', 1),
(343, 42, 'gallery-image-53830eca0fea0.jpeg', 2),
(344, 42, 'gallery-image-53830ed904d69.jpeg', 3),
(345, 29, 'gallery-image-538c69020c59c.jpeg', 7),
(346, 29, 'gallery-image-538c69341de7c.jpeg', 8),
(347, 29, 'gallery-image-538c6953ab8a6.jpeg', 9),
(348, 29, 'gallery-image-538c696b013f1.jpeg', 10),
(349, 29, 'gallery-image-538c697ab5821.jpeg', 11),
(351, 28, 'gallery-image-538c69ae061da.jpeg', 7),
(352, 28, 'gallery-image-538c69bc8a279.jpeg', 8),
(353, 28, 'gallery-image-538c69ce4e77f.jpeg', 9),
(354, 28, 'gallery-image-538c69dce2973.jpeg', 10),
(355, 28, 'gallery-image-538c69eba3737.jpeg', 11),
(356, 28, 'gallery-image-538c69fb11492.jpeg', 12),
(357, 28, 'gallery-image-538c6a1998861.jpeg', 13),
(358, 28, 'gallery-image-538c6a619579f.jpeg', 14),
(359, 17, 'gallery-image-538c6e12aaac1.jpeg', 1),
(360, 17, 'gallery-image-538c6e2532bb7.jpeg', 2),
(361, 17, 'gallery-image-538c6e335f2b9.jpeg', 3),
(362, 17, 'gallery-image-538c6e40f3fa7.jpeg', 4),
(363, 17, 'gallery-image-538c6e4c95670.jpeg', 5),
(364, 17, 'gallery-image-538c6e5ae1a87.jpeg', 6),
(365, 17, 'gallery-image-538c72829df9f.jpeg', 7),
(366, 17, 'gallery-image-538c7295c15ac.jpeg', 8),
(368, 17, 'gallery-image-538c72b812ac8.jpeg', 9),
(369, 17, 'gallery-image-538c72c56d0c9.jpeg', 10),
(370, 17, 'gallery-image-538c72d749567.jpeg', 11),
(371, 17, 'gallery-image-538c72e8528bf.jpeg', 12),
(372, 41, 'gallery-image-538d6b1423fe0.jpeg', 1),
(373, 41, 'gallery-image-538d6b226926f.jpeg', 2),
(375, 41, 'gallery-image-538d6b3bab654.jpeg', 3),
(376, 41, 'gallery-image-538d6b4ae749e.jpeg', 4),
(377, 41, 'gallery-image-538d6b6016587.jpeg', 5),
(378, 41, 'gallery-image-538d6b6ebfc75.jpeg', 6),
(379, 18, 'gallery-image-538d6de4ec098.jpeg', 4),
(380, 18, 'gallery-image-538d6df14a49a.jpeg', 5),
(381, 18, 'gallery-image-538d6dfdba77d.jpeg', 6),
(382, 18, 'gallery-image-538d6e0c3bca2.jpeg', 7),
(383, 18, 'gallery-image-538d6e1ec1952.jpeg', 8),
(385, 18, 'gallery-image-538d6e4605c74.jpeg', 9),
(388, 18, 'gallery-image-538d6e852bc0e.jpeg', 10),
(389, 18, 'gallery-image-538d6e9305da3.jpeg', 11),
(390, 26, 'gallery-image-538d706747585.jpeg', 10),
(391, 26, 'gallery-image-538d70892885f.jpeg', 13),
(392, 26, 'gallery-image-538d7097c1ede.jpeg', 14),
(393, 26, 'gallery-image-538d70a552610.jpeg', 15),
(394, 55, 'gallery-image-538d73b190ed3.jpeg', 10),
(395, 55, 'gallery-image-538d73be0cfc2.jpeg', 11),
(396, 55, 'gallery-image-538d73c9b9a73.jpeg', 12),
(397, 55, 'gallery-image-538d73d65ca84.jpeg', 13),
(398, 55, 'gallery-image-538d73e64f9b8.jpeg', 14),
(399, 55, 'gallery-image-538d73f4eb786.jpeg', 15),
(400, 50, 'gallery-image-538d96d411092.jpeg', 1),
(401, 50, 'gallery-image-538d96dfb911f.jpeg', 2),
(402, 50, 'gallery-image-538d96ebe7171.jpeg', 3),
(403, 50, 'gallery-image-538d96f9b3954.jpeg', 4),
(404, 50, 'gallery-image-538d97072e8a3.jpeg', 5),
(405, 50, 'gallery-image-538d9719bffe2.jpeg', 6),
(406, 50, 'gallery-image-538d9726e2d6b.jpeg', 7),
(407, 50, 'gallery-image-538d973568cac.jpeg', 8),
(408, 50, 'gallery-image-538d975645a53.jpeg', 9),
(409, 5, 'gallery-image-538d989530650.jpeg', 4),
(410, 5, 'gallery-image-538d98a63367f.jpeg', 5),
(411, 5, 'gallery-image-538d98b33c14c.jpeg', 6),
(413, 5, 'gallery-image-538d98d17ea48.jpeg', 7),
(414, 5, 'gallery-image-538d99008acd6.jpeg', 8),
(415, 5, 'gallery-image-538d9918b0b5d.jpeg', 9),
(419, 57, 'gallery-image-538d9a84704e3.jpeg', 7),
(422, 8, 'gallery-image-539e99c95192f.jpeg', 3),
(423, 8, 'gallery-image-539e99e0cde42.jpeg', 4),
(425, 8, 'gallery-image-539e9a096d768.jpeg', 5),
(426, 8, 'gallery-image-539e9a2289460.jpeg', 6),
(427, 8, 'gallery-image-539e9a3fcfbf0.jpeg', 7),
(430, 62, 'gallery-image-53a12fd66a353.jpeg', 1),
(431, 62, 'gallery-image-53a12fe3684c2.jpeg', 2),
(432, 62, 'gallery-image-53a12ff0f26ba.jpeg', 3),
(433, 61, 'gallery-image-53c382a25af89.jpeg', 1),
(435, 59, 'gallery-image-53c4ccc785d50.jpeg', 5),
(436, 59, 'gallery-image-53c4ccdae203d.jpeg', 6),
(437, 59, 'gallery-image-53c4cce9bbce8.jpeg', 7),
(438, 59, 'gallery-image-53c4cd3955747.jpeg', 8),
(439, 60, 'gallery-image-53c4ceba750ed.jpeg', 1),
(440, 60, 'gallery-image-53c4cec73b8c7.jpeg', 2),
(441, 60, 'gallery-image-53c4ced3be04a.jpeg', 3),
(442, 60, 'gallery-image-53c4cee061e13.jpeg', 4),
(443, 63, 'gallery-image-53ccb5839bf56.jpeg', 2),
(444, 63, 'gallery-image-53ccb59182a60.jpeg', 3),
(446, 63, 'gallery-image-53ccb6a248dd9.jpeg', 4),
(448, 61, 'gallery-image-53db41ee54094.jpeg', 2),
(449, 65, 'gallery-image-54080aa65b950.jpeg', 1),
(451, 65, 'gallery-image-54080b3419e37.jpeg', 2),
(452, 65, 'gallery-image-54080b797d140.jpeg', 3),
(453, 65, 'gallery-image-54080b842d04f.jpeg', 4),
(454, 65, 'gallery-image-54080b8cc1a3e.jpeg', 5),
(455, 65, 'gallery-image-54080b97389c6.jpeg', 6),
(456, 65, 'gallery-image-54080b9f47684.jpeg', 7),
(457, 48, 'gallery-image-540d675d64130.jpeg', 1),
(458, 48, 'gallery-image-540d67717ba6b.jpeg', 2),
(459, 48, 'gallery-image-540d6788a7b02.jpeg', 3),
(460, 48, 'gallery-image-540d679a3675f.jpeg', 4),
(461, 48, 'gallery-image-540d67a6a45da.jpeg', 5),
(462, 48, 'gallery-image-540d67b4db2f0.jpeg', 6),
(463, 48, 'gallery-image-540d67c263b94.jpeg', 7),
(464, 19, 'gallery-image-540ea0f26433c.jpeg', 4),
(465, 19, 'gallery-image-540ea1025c88e.jpeg', 1),
(466, 19, 'gallery-image-540ea11128d41.jpeg', 2),
(467, 19, 'gallery-image-540ea11f17970.jpeg', 3),
(468, 19, 'gallery-image-540ea12c99e97.jpeg', 5),
(469, 19, 'gallery-image-540ea13abacc1.jpeg', 7),
(470, 19, 'gallery-image-540ea147dc3b1.jpeg', 6),
(471, 19, 'gallery-image-540ea156936ec.jpeg', 8),
(472, 4, 'gallery-image-541c2591dd731.jpeg', 7),
(473, 4, 'gallery-image-541c25ac47b29.jpeg', 8),
(474, 4, 'gallery-image-541c25b79e350.jpeg', 9),
(475, 4, 'gallery-image-541c25da1450a.jpeg', 10),
(476, 4, 'gallery-image-541c2678029cd.jpeg', 11),
(478, 4, 'gallery-image-541c26e0bdc40.jpeg', 12),
(479, 66, 'gallery-image-542e48440e029.jpeg', 1),
(480, 66, 'gallery-image-542e484ec61bf.jpeg', 2),
(481, 66, 'gallery-image-542e485b3c076.jpeg', 3),
(482, 66, 'gallery-image-542e486b726e1.jpeg', 4),
(483, 66, 'gallery-image-542e487741d66.jpeg', 5),
(484, 66, 'gallery-image-542e48836e6c5.jpeg', 6),
(485, 66, 'gallery-image-542e48939db7b.jpeg', 7),
(486, 66, 'gallery-image-542e48bedd6a0.jpeg', 8),
(487, 66, 'gallery-image-542e48cef0b84.jpeg', 9),
(488, 66, 'gallery-image-542e48e56f0c6.jpeg', 10),
(489, 11, 'gallery-image-543e49f86fad2.jpeg', 17),
(490, 11, 'gallery-image-543e4a0168a83.jpeg', 18),
(491, 11, 'gallery-image-543e4a169182f.jpeg', 19),
(492, 11, 'gallery-image-543e4a2387b23.jpeg', 20),
(493, 11, 'gallery-image-543e4a3577610.jpeg', 21),
(494, 11, 'gallery-image-543e4a40edbf8.jpeg', 22),
(495, 16, 'gallery-image-543f6597f008b.jpeg', 7),
(496, 16, 'gallery-image-543f65a489d03.jpeg', 8),
(497, 16, 'gallery-image-543f65b61d282.jpeg', 9),
(498, 16, 'gallery-image-543f65d92879a.jpeg', 10),
(499, 16, 'gallery-image-543f65e390e31.jpeg', 11),
(500, 16, 'gallery-image-543f65ed5cf35.jpeg', 12),
(501, 15, 'gallery-image-543f66f345b4e.jpeg', 9),
(503, 15, 'gallery-image-543f6705410f2.jpeg', 10),
(505, 63, 'gallery-image-543f67dd3f565.jpeg', 5),
(506, 63, 'gallery-image-543f67e6c0fc9.jpeg', 1),
(507, 63, 'gallery-image-543f67ee49c3d.jpeg', 6),
(509, 43, 'gallery-image-543f691c5110f.jpeg', 9),
(510, 43, 'gallery-image-543f6925b524b.jpeg', 10),
(511, 43, 'gallery-image-543f6931553ec.jpeg', 11),
(512, 43, 'gallery-image-543f69394ed42.jpeg', 12),
(513, 58, 'gallery-image-543f6a48523e2.jpeg', 7),
(514, 58, 'gallery-image-543f6a50a60c3.jpeg', 8),
(515, 58, 'gallery-image-543f6a59a1572.jpeg', 9),
(516, 28, 'gallery-image-543f8a3bd0fe2.jpeg', 15),
(517, 28, 'gallery-image-543f8a44c041a.jpeg', 16),
(518, 28, 'gallery-image-543f8a4eb31ee.jpeg', 17),
(519, 28, 'gallery-image-543f8dc68c213.jpeg', 18),
(520, 28, 'gallery-image-543f8dcee7107.jpeg', 19),
(521, 28, 'gallery-image-543f8dd6b656f.jpeg', 20),
(522, 28, 'gallery-image-543f8de1225ad.jpeg', 21),
(523, 28, 'gallery-image-543f8df731997.jpeg', 22),
(524, 18, 'gallery-image-543f8e815c973.jpeg', 12),
(525, 32, 'gallery-image-543f902240bba.jpeg', 9),
(526, 32, 'gallery-image-543f904c5f8a3.jpeg', 10),
(527, 32, 'gallery-image-543f90547cb8d.jpeg', 11),
(528, 32, 'gallery-image-543f905d6b9ed.jpeg', 12),
(529, 32, 'gallery-image-543f90675f6fb.jpeg', 13),
(530, 67, 'gallery-image-54d0ab9ca45f8.jpeg', 1),
(531, 67, 'gallery-image-54d0aba80a458.jpeg', 2),
(532, 67, 'gallery-image-54d0abb303b5a.jpeg', 3),
(533, 67, 'gallery-image-54d0abbd633fa.jpeg', 4),
(534, 67, 'gallery-image-54d0abc8881ff.jpeg', 5),
(535, 67, 'gallery-image-54d0abdb59674.jpeg', 6),
(536, 67, 'gallery-image-54d0abe837f5b.jpeg', 7),
(537, 67, 'gallery-image-54d0abf55d418.jpeg', 8),
(539, 67, 'gallery-image-54d0ac13568c4.jpeg', 9),
(540, 67, 'gallery-image-54d0ac21eda6d.jpeg', 10),
(541, 67, 'gallery-image-54d0ac407abf8.jpeg', 11),
(551, 69, 'gallery-image-54f0337ddd0cb.jpeg', 1),
(552, 69, 'gallery-image-54f033a9a9b9e.jpg', 2),
(553, 69, 'gallery-image-54f033aa00344.jpg', 3),
(554, 69, 'gallery-image-54f033aa58b96.jpg', 4),
(555, 69, 'gallery-image-54f033aaa3366.jpg', 5),
(556, 69, 'gallery-image-54f033aaed3c2.jpg', 6),
(557, 69, 'gallery-image-54f033ab48023.jpg', 7),
(559, 69, 'gallery-image-54f033eee8813.jpeg', 8),
(560, 69, 'gallery-image-54f03403813e1.jpeg', 9),
(561, 37, 'gallery-image-54f6bfb9496bd.jpeg', 1),
(562, 37, 'gallery-image-54f6bfc6d175b.jpeg', 2),
(563, 37, 'gallery-image-54f6bfd41c961.jpeg', 3),
(564, 37, 'gallery-image-54f6bfdfb2d57.jpeg', 4),
(565, 37, 'gallery-image-54f6bfed05921.jpeg', 5),
(566, 37, 'gallery-image-54f6bffcaa498.jpeg', 6),
(568, 15, 'gallery-image-5515633939485.jpeg', 11),
(569, 15, 'gallery-image-55156383cb282.jpeg', 12),
(570, 15, 'gallery-image-551563f2aea0f.jpeg', 13),
(571, 63, 'gallery-image-551564473a532.jpeg', 7),
(572, 63, 'gallery-image-5515645fcf5f0.jpeg', 8),
(573, 63, 'gallery-image-551564bfa35b4.jpeg', 9),
(574, 63, 'gallery-image-551564e1badcc.jpeg', 10),
(575, 63, 'gallery-image-551564ed11a4b.jpeg', 11),
(581, 44, 'gallery-image-551565b929fac.jpeg', 4),
(582, 65, 'gallery-image-551565fb18cf1.jpeg', 8),
(583, 65, 'gallery-image-5515660699c87.jpeg', 9),
(584, 65, 'gallery-image-5515661301616.jpeg', 10),
(585, 65, 'gallery-image-55156625c75ad.jpeg', 11),
(586, 65, 'gallery-image-55156631e5220.jpeg', 12),
(587, 65, 'gallery-image-5515663c69f3b.jpeg', 13),
(588, 65, 'gallery-image-551566459da9a.jpeg', 14),
(589, 65, 'gallery-image-5515664e297ff.jpeg', 15),
(590, 55, 'gallery-image-5515667bbc11a.jpeg', 16),
(591, 55, 'gallery-image-55156686161b2.jpeg', 17),
(592, 55, 'gallery-image-5515668edcabf.jpeg', 18),
(593, 55, 'gallery-image-551566a390da2.jpeg', 19),
(594, 55, 'gallery-image-551566ac607c1.jpeg', 20),
(595, 55, 'gallery-image-551566b7ae1b4.jpeg', 21),
(596, 69, 'gallery-image-5535e882e0434.jpeg', 10),
(597, 70, 'gallery-image-55b778f37fef3.jpeg', 1),
(598, 70, 'gallery-image-55b7796c36bd0.jpeg', 2),
(599, 70, 'gallery-image-55b7797896c49.jpeg', 3),
(600, 70, 'gallery-image-55b779828eca6.jpeg', 4),
(601, 70, 'gallery-image-55b7798a6f707.jpeg', 5),
(602, 70, 'gallery-image-55b779924aa7e.jpeg', 6),
(603, 70, 'gallery-image-55b7799ae3d60.jpeg', 7),
(604, 70, 'gallery-image-55b779b1d858e.jpeg', 8),
(605, 71, 'gallery-image-55c49e5a66170.jpeg', 1),
(606, 71, 'gallery-image-55c49e6ec92b3.jpeg', 2),
(607, 71, 'gallery-image-55c49e81c9d6a.jpeg', 3),
(608, 71, 'gallery-image-55c49e9f9da7a.jpeg', 4),
(609, 71, 'gallery-image-55c49eade4375.jpeg', 5),
(610, 71, 'gallery-image-55c49ec05df1f.jpeg', 6),
(611, 71, 'gallery-image-55c49eccdb92b.jpeg', 7),
(612, 71, 'gallery-image-55c49ed96511f.jpeg', 8),
(613, 37, 'gallery-image-55c49f4ca98e1.jpeg', 7),
(614, 37, 'gallery-image-55c49f596ee1d.jpeg', 8),
(615, 37, 'gallery-image-55c49f6663e35.jpeg', 9),
(616, 37, 'gallery-image-55c49f85b5aac.jpeg', 10),
(617, 47, 'gallery-image-56175ff9e5f56.jpeg', 1),
(618, 47, 'gallery-image-56176007949c4.jpeg', 2),
(619, 47, 'gallery-image-5617601728089.jpeg', 3),
(620, 47, 'gallery-image-56176025d8466.jpeg', 4),
(621, 47, 'gallery-image-561760342638b.jpeg', 5),
(622, 47, 'gallery-image-56176043294da.jpeg', 6),
(623, 47, 'gallery-image-561760527f383.jpeg', 7),
(624, 47, 'gallery-image-561760607c71d.jpeg', 8),
(625, 47, 'gallery-image-561760e20fe75.jpeg', 9),
(640, 74, 'gallery-image-5630a5500bd7c.jpeg', 1),
(641, 76, 'gallery-image-563b03b6abe5a.jpeg', 1),
(642, 76, 'gallery-image-563b03c4c32c0.jpeg', 2),
(643, 79, 'gallery-image-5661734f9a1a0.jpeg', 1),
(644, 79, 'gallery-image-5661735d20786.jpeg', 2),
(645, 79, 'gallery-image-5661747c502b0.jpeg', 3),
(646, 79, 'gallery-image-5661749114fa0.jpeg', 4),
(647, 79, 'gallery-image-566174a5e71de.jpeg', 5),
(648, 78, 'gallery-image-5661750659b79.jpeg', 1),
(649, 78, 'gallery-image-5661751281d23.jpeg', 2),
(650, 78, 'gallery-image-5661751f1b813.jpeg', 3),
(651, 78, 'gallery-image-5661752b6d89e.jpeg', 4),
(652, 78, 'gallery-image-5661753d87137.jpeg', 5),
(653, 78, 'gallery-image-5661754be2d2f.jpeg', 6),
(654, 73, 'gallery-image-56695acc8acdb.jpeg', 1),
(655, 73, 'gallery-image-56695ae4d2fc9.jpeg', 2),
(656, 73, 'gallery-image-56695af4a5097.jpeg', 3),
(657, 73, 'gallery-image-56695b03c610d.jpeg', 4),
(658, 73, 'gallery-image-56695b131f180.jpeg', 5),
(659, 73, 'gallery-image-56695b214477e.jpeg', 6),
(660, 73, 'gallery-image-56695b35cba9c.jpeg', 7),
(661, 72, 'gallery-image-5669620bdb93b.jpeg', 1),
(662, 72, 'gallery-image-56696240d95e7.jpeg', 2),
(663, 72, 'gallery-image-5669625216262.jpeg', 3),
(664, 72, 'gallery-image-56696261e41e1.jpeg', 4),
(665, 72, 'gallery-image-5669626fc48d8.jpeg', 5),
(666, 72, 'gallery-image-566962e73e708.jpeg', 6),
(667, 72, 'gallery-image-566962f528b4c.jpeg', 7),
(668, 72, 'gallery-image-5669630351119.jpeg', 8),
(669, 72, 'gallery-image-5669630d18ac6.jpeg', 9),
(671, 72, 'gallery-image-5669632198eb9.jpeg', 10),
(672, 72, 'gallery-image-5669632c28df1.jpeg', 11),
(673, 72, 'gallery-image-56696339c2fd4.jpeg', 12),
(674, 72, 'gallery-image-5669634299824.jpeg', 13),
(675, 72, 'gallery-image-5669634d9c9c1.jpeg', 14),
(676, 74, 'gallery-image-569c941829d10.jpeg', 2),
(677, 74, 'gallery-image-569c942851606.jpeg', 3),
(678, 74, 'gallery-image-569c9436e5d66.jpeg', 4),
(679, 74, 'gallery-image-569c944c5238b.jpeg', 5),
(680, 74, 'gallery-image-569c945dda11b.jpeg', 6),
(682, 74, 'gallery-image-569c9489c35c3.jpeg', 7),
(683, 74, 'gallery-image-569c9497b1515.jpeg', 8),
(684, 74, 'gallery-image-569c94aa365cf.jpeg', 9),
(685, 74, 'gallery-image-569c94b77e5d0.jpeg', 10),
(686, 74, 'gallery-image-569c94c4eea6f.jpeg', 11),
(687, 74, 'gallery-image-569c94d398894.jpeg', 12),
(688, 74, 'gallery-image-569c94e4eb949.jpeg', 13),
(689, 80, 'gallery-image-5721e46ca21f0.jpeg', 1),
(690, 80, 'gallery-image-5721e47f6aec1.jpeg', 2),
(691, 80, 'gallery-image-5721e49097615.jpeg', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `routeParameters` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `parent_id`, `location`, `type`, `title`, `route`, `routeParameters`, `url`, `onclick`, `arrangement`) VALUES
(2, NULL, 'menu_top', 'route', 'Nowości', 'lscms_product_new', '{\"slug\":\"o-nas\"}', NULL, NULL, 1),
(3, NULL, 'menu_top', 'route', 'Promocje', 'lscms_product_promotion', '{\"slug\":\"o-nas\"}', NULL, NULL, 2),
(4, NULL, 'menu_top', 'route', 'Współpraca', 'lscms_pages_show', '{\"slug\":\"wspolpraca\"}', NULL, NULL, 3),
(6, NULL, 'menu_top', 'route', 'Kontakt', 'lscms_contact', NULL, NULL, NULL, 5),
(8, NULL, 'menu_bottom', 'route', 'Nowości', 'lscms_product_new', '{\"slug\":\"o-nas\"}', NULL, NULL, 2),
(9, NULL, 'menu_bottom', 'route', 'Promocje', 'lscms_product_promotion', '{\"slug\":\"oferta\"}', NULL, NULL, 3),
(10, NULL, 'menu_bottom', 'route', 'Współpraca', 'lscms_pages_show', '{\"slug\":\"wspolpraca\"}', NULL, NULL, 4),
(11, NULL, 'menu_bottom', 'route', 'Kontakt', 'lscms_contact', '{\"slug\":\"pokoje\"}', NULL, NULL, 5),
(12, NULL, 'menu_bottom', 'route', 'Sklepy', 'lscms_shop', NULL, NULL, NULL, 6),
(13, NULL, 'menu_bottom', 'route', 'Strona główna', 'lscms_homepage', NULL, NULL, NULL, 1),
(14, NULL, 'menu_top', 'route', 'Sklepy', 'lscms_shop', NULL, NULL, NULL, 4),
(15, NULL, 'menu_top_middle', 'route', 'Szkoła', 'lscms_productCategory_show', '{\"slug\":\"artykuly-szkolne\"}', NULL, NULL, 2),
(16, NULL, 'menu_top_middle', 'route', 'Zabawki', 'lscms_productCategory_show', '{\"slug\":\"zabawki\"}', NULL, NULL, 3),
(19, NULL, 'menu_top_middle', 'route', 'Kosmetyki', 'lscms_productCategory_show', '{\"slug\":\"kosmetyki\"}', NULL, NULL, 4),
(25, NULL, 'menu_top_middle', 'route', 'Dom', 'lscms_productCategory_show', '{\"slug\":\"czysty-dom\"}', NULL, NULL, 5),
(26, NULL, 'menu_top_middle', 'route', 'Kwiaty', 'lscms_productCategory_show', '{\"slug\":\"kwiaty\"}', NULL, NULL, 1),
(27, NULL, 'menu_top_middle', 'route', 'Galanteria', 'lscms_productCategory_show', '{\"slug\":\"galanteria\"}', NULL, NULL, 6),
(28, NULL, 'menu_top_middle', 'route', 'Dekoracje', 'lscms_productCategory_show', '{\"slug\":\"artykuly-dekoracyjne\"}', NULL, NULL, 7),
(29, NULL, 'menu_top_middle', 'route', 'Kuchnia', 'lscms_productCategory_show', '{\"slug\":\"kuchnia\"}', NULL, NULL, 8),
(30, NULL, 'menu_top_middle', 'route', 'Szkło', 'lscms_productCategory_show', '{\"slug\":\"szklo\"}', NULL, NULL, 9),
(32, NULL, 'menu_top_middle', 'route', 'Narzędzia', 'lscms_productCategory_show', '{\"slug\":\"narzedzia\"}', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `content_under` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `news`
--

INSERT INTO `news` (`id`, `gallery_id`, `title`, `slug`, `content_short_generate`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`, `content_under`) VALUES
(1, NULL, 'asdasd', 'asdasd', 0, 'asda', '<p>\r\n	asda</p>', NULL, 0, 'asdasd', 'asdasd, asda', 'asda', '2013-12-11 18:05:10', NULL, '2013-12-11 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `content_under` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `pages`
--

INSERT INTO `pages` (`id`, `gallery_id`, `title`, `slug`, `content_short_generate`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `content_under`) VALUES
(1, NULL, 'O firmie', 'o-firmie', 0, 'Można nasze produkty (drobne agd, upominki, zabawki, szkło użytkowe i dekoracyjne, narzedzia, tekstylia, biżuterię, art.szkolne, artykuły wyposażenia wnętrza, artykuły wyposażenia kuchni, i wiele innych) znaleźć w 60 sklepach zlokalizowanych w województwa', '<p class=\"wstep\">\r\n	Przez ponad 10 lat doświadczenia poznaliśmy Państwa potrzeby i oczekiwania.</p>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Nasze produkty (drobne agd, upominki, zabawki, szkło użytkowe i dekoracyjne, narzedzia, tekstylia, biżuterię, art.szkolne, artykuły wyposażenia wnętrza, artykuły wyposażenia kuchni, i wiele innych) można znaleźć w 60 sklepach zlokalizowanych w wojew&oacute;dztwach: lubelskim, mazowieckim, podlaskim i świętokrzyskim.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Firma cały czas się rozwija i w najbliższym czasie planujemy otwarcie nowych punkt&oacute;w handlowych na terenia Polski wschodniej. Jeżeli prowadzisz firmę handlową z produktami&nbsp; z naszej branży zapraszamy do wsp&oacute;łpracy.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<p class=\"warning\">\r\n	Celem naszej Firmy jest spełnianie oczekiwań i potrzeb naszych klient&oacute;w dzięki zdobywanemu doświadczeniu.</p>', 'pages-image-528b901baded1.jpeg', 0, 'O firmie', 'firmie, Firma, Handlowa, \"ASTEK\", 2003r, specjalizuje, się, handlu, detalicznym, hurtowym', 'Firma Handlowa \"ASTEK\" od 2003r specjalizuje się w handlu detalicznym i hurtowym artykułami przemysłowymi w branży sklep', '2013-11-13 16:56:16', NULL, NULL),
(4, NULL, 'Współpraca', 'wspolpraca', 0, 'Firma poszukuję na terenie polski wschodniej lokali o metrażu od 90 do 500 m2 w rozsądnej cenie.\r\nPosiadasz taki lokal? Zadzwoń: 81 857 43 08  lub +48 502 438 740', '<p class=\"wstep\" style=\"text-align: center;\">\r\n	Firma poszukuję na terenie polski wschodniej lokali o metrażu od 60 do 200 m2 w rozsądnej cenie.</p>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: center;\">\r\n	Proponujemy naszym klientom otwarcie sklep&oacute;w partnerskich na atrakcyjnych warunkach.</p>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<p class=\"wstep\" style=\"text-align: center;\">\r\n	Szczeg&oacute;łowe informacje pod nr tel. +48 500 088 828</p>', NULL, 0, 'Współpraca', 'Współpraca, Firma, poszukuję, terenie, polski, wschodniej, lokali, metrażu, rozs', 'Firma poszukuję na terenie polski wschodniej lokali o metrażu od 60 do 200 m2 w rozs', '2013-12-18 10:39:08', NULL, NULL),
(5, 61, 'Gazetka', 'gazetka', 0, 'Zapraszamy do obejrzenia gazetki promocyjnej i skorzystania z naszej atrakcyjnej oferty:', '<p class=\"wstep\">\r\n	Zapraszamy do obejrzenia gazetki promocyjnej i skorzystania z naszej atrakcyjnej oferty:</p>', NULL, 0, 'Gazetka', 'Gazetka, Zapraszamy, obejrzenia, gazetki, promocyjnej, skorzystania, naszej, atrakcyjnej, oferty:', 'Zapraszamy do obejrzenia gazetki promocyjnej i skorzystania z naszej atrakcyjnej oferty:', '2014-06-13 18:25:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `promotionPrice` decimal(10,2) DEFAULT NULL,
  `isNew` tinyint(1) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `seoGenerate` tinyint(1) DEFAULT NULL,
  `seoTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoKeywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `contentShortGenerate` tinyint(1) DEFAULT NULL,
  `contentShort` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `newUntilDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `product`
--

INSERT INTO `product` (`id`, `category_id`, `name`, `slug`, `price`, `promotionPrice`, `isNew`, `photo`, `createdAt`, `updatedAt`, `quantity`, `seoGenerate`, `seoTitle`, `seoKeywords`, `seoDescription`, `content`, `contentShortGenerate`, `contentShort`, `newUntilDate`) VALUES
(1684, 31, 'Pamiętnik AS00533', 'pamietnik-as00533', 5.99, NULL, 0, 'product-image-58d1874f62524.jpeg', '2017-03-21 20:19:26', NULL, NULL, 0, 'Produkt1', 'Produkt1', NULL, NULL, 0, NULL, NULL),
(1685, 31, 'Teczka-organizer AS00628', 'teczka-organizer-as00628', 9.95, NULL, 0, 'product-image-58d1875fbc97e.jpeg', '2017-03-21 20:20:12', NULL, NULL, 0, 'Produkt2', 'Produkt2', NULL, NULL, 0, NULL, NULL),
(1686, 31, 'Teczka AS00630', 'teczka-as00630', 5.99, NULL, 0, 'product-image-58d1877185eda.jpeg', '2017-03-21 20:20:39', NULL, NULL, 0, 'Produkt3', 'Produkt3', NULL, NULL, 0, NULL, NULL),
(1687, 31, 'Teczka na dokumenty AS00626', 'teczka-na-dokumenty-as00626', 5.99, NULL, 0, 'product-image-58d18780d3e9c.jpeg', '2017-03-21 20:20:56', NULL, NULL, 0, 'Produkt4', 'Produkt4', NULL, NULL, 0, NULL, '2020-01-18 00:00:00'),
(1688, 31, 'Kalendarz książkowy', 'kalendarz-ksiazkowy', 1.99, 2.99, 0, 'product-image-58d1878d9a0b0.jpeg', '2017-03-21 20:21:12', NULL, NULL, 0, 'Produkt5', 'Produkt5', NULL, NULL, 0, NULL, NULL),
(1690, 31, 'Długopisy 8szt. AS00549', 'dlugopisy-8szt-as00549', 2.99, NULL, 0, 'product-image-58d19054e5623.jpeg', '2017-03-21 21:42:59', NULL, NULL, 0, 'Produkt6', 'Produkt6', NULL, NULL, 0, '', NULL),
(1691, 31, 'Taśma KP0595', 'tasma-kp0595', 2.99, NULL, 0, 'product-image-58d19071dd1ef.jpeg', '2017-03-21 21:43:29', NULL, NULL, 0, 'Produkt7', 'Produkt7', NULL, NULL, 0, '', NULL),
(1692, 31, 'Kredki do ciała AS00582', 'kredki-do-ciala-as00582', 3.99, NULL, 0, 'product-image-58d19083511b6.jpeg', '2017-03-21 21:43:46', NULL, NULL, 0, 'Produkt8', 'Produkt8', NULL, NULL, 0, '', NULL),
(1693, 31, 'Długopis AS00517', 'dlugopis-as00517', 2.99, NULL, 0, 'product-image-58d1909061900.jpeg', '2017-03-21 21:44:00', NULL, NULL, 0, 'Produkt9', 'Produkt9', NULL, NULL, 0, '', NULL),
(1694, 31, 'Kredki świecowe 12szt. AS00558', 'kredki-swiecowe-12szt-as00558', 5.99, NULL, 0, 'product-image-58d1909d568d1.jpeg', '2017-03-21 21:44:12', NULL, NULL, 0, 'Produkt10', 'Produkt10', NULL, NULL, 0, '', NULL),
(1695, 31, 'Kredki  świecowe kpl.12szt. AS00557', 'kredki-swiecowe-kpl-12szt-as00557', 2.99, NULL, 0, 'product-image-58d190a96b32b.jpeg', '2017-03-21 21:44:25', NULL, NULL, 0, 'Produkt11', 'Produkt11', NULL, NULL, 0, '', NULL),
(1696, 31, 'Długopisy 6szt. AS00542', 'dlugopisy-6szt-as00542', 2.99, NULL, 0, 'product-image-58d28a2337eed.jpeg', '2017-03-22 15:28:50', NULL, NULL, 0, 'Produkt12', 'Produkt12', NULL, NULL, 0, '', NULL),
(1697, 31, 'Kpl. cienkopisów 12szt. AS00560', 'kpl-cienkopisow-12szt-as00560', 3.99, NULL, 0, 'product-image-58d28a34f0752.jpeg', '2017-03-22 15:29:08', NULL, NULL, 0, 'Produkt13', 'Produkt13', NULL, NULL, 0, '', NULL),
(1698, 31, 'Farby do malowania AS00565', 'farby-do-malowania-as00565', 8.97, NULL, 0, 'product-image-58d28a40dc688.jpeg', '2017-03-22 15:29:20', NULL, NULL, 0, 'Produkt14', 'Produkt14', NULL, NULL, 0, '', NULL),
(1699, 31, 'Plastelina AS00564', 'plastelina-as00564', 4.99, NULL, 0, 'product-image-58d28a562f671.jpeg', '2017-03-22 15:29:41', NULL, NULL, 0, 'Produkt15', 'Produkt15', NULL, NULL, 0, '', NULL),
(1700, 31, 'Plastelina AS00559', 'plastelina-as00559', 2.99, NULL, 0, 'product-image-58d28a67b6e3e.jpeg', '2017-03-22 15:29:59', NULL, NULL, 0, 'Produkt16', 'Produkt16', NULL, NULL, 0, '', NULL),
(1701, 31, 'Flamastry 24szt. AS00561', 'flamastry-24szt-as00561', 3.99, NULL, 0, 'product-image-58d28a755bb87.jpeg', '2017-03-22 15:30:13', NULL, NULL, 0, 'Produkt17', 'Produkt17', NULL, NULL, 0, '', NULL),
(1702, 31, 'Taśma klejąca 6szt. 17645M', 'tasma-klejaca-6szt-17645m', 2.99, NULL, 0, 'product-image-58d28a83459da.jpeg', '2017-03-22 15:30:27', NULL, NULL, 0, 'Produkt18', 'Produkt18', NULL, NULL, 0, '', NULL),
(1703, 31, 'Kpl. gumek do ścierania+temperówki AS00547', 'kpl-gumek-do-scierania-temperowki-as00547', 2.99, NULL, 0, 'product-image-58d28a8e99512.jpeg', '2017-03-22 15:30:38', NULL, NULL, 0, 'Produkt19', 'Produkt19', NULL, NULL, 0, '', NULL),
(1704, 31, 'Nożyczki KP0546', 'nozyczki-kp0546', 2.99, NULL, 0, 'product-image-58d28a9a1f546.jpeg', '2017-03-22 15:30:49', NULL, NULL, 0, 'Produkt20', 'Produkt20', NULL, NULL, 0, '', NULL),
(1705, 31, 'Gumki 3szt. AS00553', 'gumki-3szt-as00553', 1.99, NULL, 0, 'product-image-58d28aa7ba240.jpeg', '2017-03-22 15:31:03', NULL, NULL, 0, 'Produkt21', 'Produkt21', NULL, NULL, 0, '', NULL),
(1706, 31, 'Klej z brokatem 12szt. AS00562', 'klej-z-brokatem-12szt-as00562', 3.99, NULL, 0, 'product-image-58d28ab3ceb3e.jpeg', '2017-03-22 15:31:15', NULL, NULL, 0, 'Produkt22', 'Produkt22', NULL, NULL, 0, '', NULL),
(1716, 32, 'Słonik nakręcany AS00685', 'slonik-nakrecany-as00685', 3.99, NULL, 0, 'product-image-58d4303e70ad6.jpeg', '2017-03-23 21:29:50', NULL, NULL, 0, 'Produkt1', 'Produkt1', NULL, NULL, 0, '', NULL),
(1717, 32, 'Wielbłąd nakecany AS00687', 'wielblad-nakecany-as00687', 3.99, NULL, 0, 'product-image-58d43053d463f.jpeg', '2017-03-23 21:30:11', NULL, NULL, 0, 'Produkt2', 'Produkt2', NULL, NULL, 0, '', NULL),
(1718, 32, 'Maskotka piesek AS00338', 'maskotka-piesek-as00338', 9.95, NULL, 0, 'product-image-58d4305f38dbd.jpeg', '2017-03-23 21:30:22', NULL, NULL, 0, 'Produkt3', 'Produkt3', NULL, NULL, 0, '', NULL),
(1719, 32, 'Maskotka słoń AS00341', 'maskotka-slon-as00341', 9.95, NULL, 0, 'product-image-58d4306a9899d.jpeg', '2017-03-23 21:30:33', NULL, NULL, 0, 'Produkt4', 'Produkt4', NULL, NULL, 0, '', NULL),
(1720, 32, 'Makotka Miś As00337', 'makotka-mis-as00337', 9.95, NULL, 0, 'product-image-58d4307585250.jpeg', '2017-03-23 21:30:45', NULL, NULL, 0, 'Produkt5', 'Produkt5', NULL, NULL, 0, '', NULL),
(1721, 32, 'Maskotka ośmiornica AS00340', 'maskotka-osmiornica-as00340', 9.95, NULL, 0, 'product-image-58d43080a8c76.jpeg', '2017-03-23 21:30:56', NULL, NULL, 0, 'Produkt6', 'Produkt6', NULL, NULL, 0, '', NULL),
(1722, 32, 'Klocki Misja Mars 245El. KB0304', 'klocki-misja-mars-245el-kb0304', 49.90, NULL, 0, 'product-image-58d43088df250.jpeg', '2017-03-23 21:31:04', NULL, NULL, 0, 'Produkt7', 'Produkt7', NULL, NULL, 0, '', NULL),
(1723, 32, 'Gra piłkarze A515404B', 'gra-pilkarze-a515404b', 69.96, NULL, 0, 'product-image-58d4309317d6d.jpeg', '2017-03-23 21:31:14', NULL, NULL, 0, 'Produkt8', 'Produkt8', NULL, NULL, 0, '', NULL),
(1724, 32, 'Klocki wojsko 670EL. KB84020', 'klocki-wojsko-670el-kb84020', 119.00, NULL, 0, 'product-image-58d4309b82fd6.jpeg', '2017-03-23 21:31:23', NULL, NULL, 0, 'Produkt9', 'Produkt9', NULL, NULL, 0, '', NULL),
(1725, 32, 'Miecz laserowy rozkładany 115cm z dźwiękiem i  światłem FC010456', 'miecz-laserowy-rozkladany-115cm-z-dzwiekiem-i-swiatlem-fc010456', 23.96, NULL, 0, 'product-image-58d431e810958.jpeg', '2017-03-23 21:31:35', NULL, NULL, 0, 'Produkt10', 'Produkt10', NULL, NULL, 0, '', NULL),
(1726, 32, 'Bańki mydlane 50ml - JNP-03', 'banki-mydlane-50ml-jnp-03', 1.99, NULL, 0, 'product-image-58d517e8af5de.jpeg', '2017-03-24 13:58:16', NULL, NULL, 0, 'Produkt11', 'Produkt11', NULL, NULL, 0, '', NULL),
(1727, 32, 'Wiatrak ASZ00061', 'wiatrak-asz00061', 5.99, NULL, 0, 'product-image-58d51e54e0632.jpeg', '2017-03-24 13:58:27', NULL, NULL, 0, 'Produkt12', 'Produkt12', NULL, NULL, 0, '', NULL),
(1728, 32, 'Wiatrak ASZ00063', 'wiatrak-asz00063', 2.99, NULL, 0, 'product-image-58d51e64f29e6.jpeg', '2017-03-24 13:58:39', NULL, NULL, 0, 'Produkt13', 'Produkt13', NULL, NULL, 0, '', NULL),
(1729, 32, 'Wiatrak ZL10E305E', 'wiatrak-zl10e305e', 5.99, NULL, 0, 'product-image-58d51e70eeda8.jpeg', '2017-03-24 13:58:52', NULL, NULL, 0, 'Produkt14', 'Produkt14', NULL, NULL, 0, '', NULL),
(1730, 32, 'Bączek piłka LED ASV0398', 'baczek-pilka-led-asv0398', 5.99, NULL, 0, 'product-image-58d51817178a9.jpeg', '2017-03-24 13:59:02', NULL, NULL, 0, 'Produkt15', 'Produkt15', NULL, NULL, 0, '', NULL),
(1731, 32, 'Samochód AS00683', 'samochod-as00683', 5.99, NULL, 0, 'product-image-58d518219834d.jpeg', '2017-03-24 13:59:13', NULL, NULL, 0, 'Produkt16', 'Produkt16', NULL, NULL, 0, '', NULL),
(1732, 32, 'Piłka LED ASV0372', 'pilka-led-asv0372', 2.99, NULL, 0, 'product-image-58d5182c0d8ac.jpeg', '2017-03-24 13:59:23', NULL, NULL, 0, 'Produkt17', 'Produkt17', NULL, NULL, 0, '', NULL),
(1733, 32, 'Piłka piankowa ASV0389', 'pilka-piankowa-asv0389-1', 1.99, NULL, 0, 'product-image-58d518369b332.jpeg', '2017-03-24 13:59:34', NULL, NULL, 0, 'Produkt18', 'Produkt18', NULL, NULL, 0, '', NULL),
(1734, 32, 'Piłka piankowa ASV0389', 'pilka-piankowa-asv0389', 1.99, NULL, 0, 'product-image-58d51840cd445.jpeg', '2017-03-24 13:59:44', NULL, NULL, 0, 'Produkt19', 'Produkt19', NULL, NULL, 0, '', NULL),
(1735, 32, 'Piłka piankowa ASV0391', 'pilka-piankowa-asv0391', 1.99, NULL, 0, 'product-image-58d5184d9366b.jpeg', '2017-03-24 13:59:56', NULL, NULL, 0, 'Produkt20', 'Produkt20', NULL, NULL, 0, '', NULL),
(1736, 34, 'Szczotka żelazko -duża', 'szczotka-zelazko-duza', 2.99, NULL, 0, 'product-image-58d5418922a17.jpeg', '2017-03-24 16:55:52', NULL, NULL, 0, 'Produkt1', 'Produkt1', NULL, NULL, 0, '', NULL),
(1737, 34, 'Szczotka do WC Romano', 'szczotka-do-wc-romano', 3.99, NULL, 0, 'product-image-58d541949c4d3.jpeg', '2017-03-24 16:56:04', NULL, NULL, 0, 'Produkt2', 'Produkt2', NULL, NULL, 0, '', NULL),
(1738, 34, 'Szczotka do WC', 'szczotka-do-wc', 3.99, NULL, 0, 'product-image-58d5419e0d072.jpeg', '2017-03-24 16:56:13', NULL, NULL, 0, 'Produkt3', 'Produkt3', NULL, NULL, 0, '', NULL),
(1739, 34, 'Zmiotka|+szufelka DUO', 'zmiotka-szufelka-duo', 2.99, NULL, 0, 'product-image-58d541ab679a0.jpeg', '2017-03-24 16:56:27', NULL, NULL, 0, 'Produkt4', 'Produkt4', NULL, NULL, 0, '', NULL),
(1740, 34, 'Zmiotka+szufelka z gumką Yaga SZ-1106', 'zmiotka-szufelka-z-gumka-yaga-sz-1106', 3.99, NULL, 0, 'product-image-58d541bdb5931.jpeg', '2017-03-24 16:56:45', NULL, NULL, 0, 'Produkt5', 'Produkt5', NULL, NULL, 0, '', NULL),
(1741, 34, 'Szczotka Perfekta SZ-020', 'szczotka-perfekta-sz-020', 1.99, NULL, 0, 'product-image-58d541c88dbe8.jpeg', '2017-03-24 16:56:56', NULL, NULL, 0, 'Produkt6', 'Produkt6', NULL, NULL, 0, '', NULL),
(1742, 34, 'Kij do szczotki plastikowy', 'kij-do-szczotki-plastikowy', 1.99, NULL, 0, 'product-image-58d541d4ee087.jpeg', '2017-03-24 16:57:08', NULL, NULL, 0, 'Produkt7', 'Produkt7', NULL, NULL, 0, '', NULL),
(1743, 34, 'Mop z kijem AS00171', 'mop-z-kijem-as00171', 9.95, NULL, 0, 'product-image-58d541df73b30.jpeg', '2017-03-24 16:57:19', NULL, NULL, 0, 'Produkt8', 'Produkt8', NULL, NULL, 0, '', NULL),
(1744, 34, 'Mop sznurkowy', 'mop-sznurkowy', 2.99, NULL, 0, 'product-image-58d541e9a13a9.jpeg', '2017-03-24 16:57:29', NULL, NULL, 0, 'Produkt9', 'Produkt9', NULL, NULL, 0, '', NULL),
(1745, 34, 'Mop dred mega', 'mop-dred-mega', 2.00, NULL, 0, 'product-image-58d541f8582f5.jpeg', '2017-03-24 16:57:43', NULL, NULL, 0, 'Produkt10', 'Produkt10', NULL, NULL, 0, '', NULL),
(1746, 34, 'Mop paskowy SUPER', 'mop-paskowy-super', 2.99, NULL, 0, 'product-image-58d54213eeef1.jpeg', '2017-03-24 16:58:11', NULL, NULL, 0, 'Produkt11', 'Produkt11', NULL, NULL, 0, '', NULL),
(1747, 34, 'Zestaw nr 1', 'zestaw-nr-1', 2.99, NULL, 0, 'product-image-58d5421e06983.jpeg', '2017-03-24 16:58:21', NULL, NULL, 0, 'Produkt12', 'Produkt12', NULL, NULL, 0, '', NULL),
(1748, 34, 'Zestaw nr 2', 'zestaw-nr-2', 2.99, NULL, 0, 'product-image-58d542289cafc.jpeg', '2017-03-24 16:58:32', NULL, NULL, 0, 'Produkt13', 'Produkt13', NULL, NULL, 0, '', NULL),
(1749, 34, 'Wkład do mopa z mikrofibry MMH-46JL', 'wklad-do-mopa-z-mikrofibry-mmh-46jl', 5.99, NULL, 0, 'product-image-58d54233ee894.jpeg', '2017-03-24 16:58:43', NULL, NULL, 0, 'Produkt14', 'Produkt14', NULL, NULL, 0, '', NULL),
(1750, 34, 'Zmywak profilowany 5szt.', 'zmywak-profilowany-5szt', 1.99, NULL, 0, 'product-image-58d5424097cfd.jpeg', '2017-03-24 16:58:56', NULL, NULL, 0, 'Produkt15', 'Produkt15', NULL, NULL, 0, '', NULL),
(1751, 34, 'Zmywak kuchenny duży- 5szt.', 'zmywak-kuchenny-duzy-5szt', 0.99, NULL, 0, 'product-image-58d5424d13f87.jpeg', '2017-03-24 16:59:08', NULL, NULL, 0, 'Produkt16', 'Produkt16', NULL, NULL, 0, '', NULL),
(1752, 34, 'Zmywak kuchenny mały -10szt.', 'zmywak-kuchenny-maly-10szt', 1.49, NULL, 0, 'product-image-58d5425a6523c.jpeg', '2017-03-24 16:59:22', NULL, NULL, 0, 'Produkt17', 'Produkt17', NULL, NULL, 0, '', NULL),
(1753, 34, 'Zmywak kuchenny teflonowy', 'zmywak-kuchenny-teflonowy', 1.00, NULL, 0, 'product-image-58d54268022d7.jpeg', '2017-03-24 16:59:35', NULL, NULL, 0, 'Produkt18', 'Produkt18', NULL, NULL, 0, '', NULL),
(1754, 34, 'Czyściki kuchenne kpl.4szt.', 'czysciki-kuchenne-kpl-4szt', 1.99, NULL, 0, 'product-image-58d542766c660.jpeg', '2017-03-24 16:59:50', NULL, NULL, 0, 'Produkt19', 'Produkt19', NULL, NULL, 0, '', NULL),
(1755, 34, 'Czyściki kuchenne kpl.3szt. AS00200', 'czysciki-kuchenne-kpl-3szt-as00200', 1.99, NULL, 0, 'product-image-58d542817dc87.jpeg', '2017-03-24 17:00:01', NULL, NULL, 0, 'Produkt20', 'Produkt20', NULL, NULL, 0, '', NULL),
(1756, 34, 'Szczoteczki 3szt AS00097', 'szczoteczki-3szt-as00097', 2.99, NULL, 0, 'product-image-58d5432bb7c11.jpeg', '2017-03-24 17:02:51', NULL, NULL, 0, 'Produkt21', 'Produkt21', NULL, NULL, 0, '', NULL),
(1757, 34, 'Kpl. 2 ściereczek z mikrofibry 20*30 DMF23-2PH30', 'kpl-2-sciereczek-z-mikrofibry-20-30-dmf23-2ph30', 2.99, NULL, 0, 'product-image-58d54334938d6.jpeg', '2017-03-24 17:03:00', NULL, NULL, 0, 'Produkt22', 'Produkt22', NULL, NULL, 0, '', NULL),
(1758, 34, 'Ściereczki z mikrofibry DMF23-4PH20', 'sciereczki-z-mikrofibry-dmf23-4ph20', 2.99, NULL, 0, 'product-image-58d54341693ca.jpeg', '2017-03-24 17:03:13', NULL, NULL, 0, 'Produkt23', 'Produkt23', NULL, NULL, 0, '', NULL),
(1759, 34, 'Ścierka z mikrofibry DMF46WL-1PH22T', 'scierka-z-mikrofibry-dmf46wl-1ph22t', 1.99, NULL, 0, 'product-image-58d5434dc27cc.jpeg', '2017-03-24 17:03:25', NULL, NULL, 0, 'Produkt24', 'Produkt24', NULL, NULL, 0, '', NULL),
(1760, 34, 'Ściereczki 2szt. AST62174', 'sciereczki-2szt-ast62174', 5.99, NULL, 0, 'product-image-58d54357251b6.jpeg', '2017-03-24 17:03:34', NULL, NULL, 0, 'Produkt25', 'Produkt25', NULL, NULL, 0, '', NULL),
(1761, 34, 'Mata do suszenia naczyń 40x50 MDM-4050PH', 'mata-do-suszenia-naczyn-40x50-mdm-4050ph', 5.99, NULL, 0, 'product-image-58d54361db04f.jpeg', '2017-03-24 17:03:45', NULL, NULL, 0, 'Produkt26', 'Produkt26', NULL, NULL, 0, '', NULL),
(1762, 34, 'Ścierka bawełniana', 'scierka-bawelniana', 1.00, NULL, 0, 'product-image-58d5436d25fa8.jpeg', '2017-03-24 17:03:56', NULL, NULL, 0, 'Produkt27', 'Produkt27', NULL, NULL, 0, '', NULL),
(1763, 34, 'Ścierki kpl.2szt. 50x60 i 40x50 DMF5645-2JL', 'scierki-kpl-2szt-50x60-i-40x50-dmf5645-2jl', 2.99, NULL, 0, 'product-image-58d54377b774d.jpeg', '2017-03-24 17:04:07', NULL, NULL, 0, 'Produkt28', 'Produkt28', NULL, NULL, 0, '', NULL),
(1764, 34, 'Mydelniczka z przyssawkami AS00090', 'mydelniczka-z-przyssawkami-as00090', 2.99, NULL, 0, 'product-image-58d5438163a39.jpeg', '2017-03-24 17:04:17', NULL, NULL, 0, 'Produkt29', 'Produkt29', NULL, NULL, 0, '', NULL),
(1765, 34, 'Kosz na  śmieci 3L stal chromowana MX2046', 'kosz-na-smieci-3l-stal-chromowana-mx2046', 23.96, NULL, 0, 'product-image-58d5438d5acac.jpeg', '2017-03-24 17:04:29', NULL, NULL, 0, 'Produkt30', 'Produkt30', NULL, NULL, 0, '', NULL),
(1766, 35, 'Kwiaty sztuczne ASFLY231', 'kwiaty-sztuczne-asfly231', 6.99, NULL, 0, 'product-image-58d9045fe2d0b.jpeg', '2017-03-27 14:23:59', NULL, NULL, 0, 'Produkt1', 'Produkt1', NULL, NULL, 0, '', NULL),
(1767, 35, 'Kwiat sztuczny FLE080', 'kwiat-sztuczny-fle080', 6.99, NULL, 0, 'product-image-58d9046e03045.jpeg', '2017-03-27 14:24:13', NULL, NULL, 0, 'Produkt2', 'Produkt2', NULL, NULL, 0, '', NULL),
(1768, 35, 'Kwiat sztuczny FLJ047', 'kwiat-sztuczny-flj047', 6.99, NULL, 0, 'product-image-58d90479c6bd7.jpeg', '2017-03-27 14:24:25', NULL, NULL, 0, 'Produkt3', 'Produkt3', NULL, NULL, 0, '', NULL),
(1769, 35, 'Kwiat sztuczny FLJ273', 'kwiat-sztuczny-flj273', 6.99, NULL, 0, 'product-image-58d90484f367f.jpeg', '2017-03-27 14:24:36', NULL, NULL, 0, 'Produkt4', 'Produkt4', NULL, NULL, 0, '', NULL),
(1770, 35, 'Kwiat sztuczny FLO029', 'kwiat-sztuczny-flo029', 6.99, NULL, 0, 'product-image-58d904cee8016.jpeg', '2017-03-27 14:25:50', NULL, NULL, 0, 'Produkt5', 'Produkt5', NULL, NULL, 0, '', NULL),
(1771, 35, 'Kwiat sztuczny FLJ017', 'kwiat-sztuczny-flj017', 6.99, NULL, 0, 'product-image-58d904e31a813.jpeg', '2017-03-27 14:26:10', NULL, NULL, 0, 'Produkt6', 'Produkt6', NULL, NULL, 0, '', NULL),
(1772, 35, 'Kwiat sztuczny FLE190', 'kwiat-sztuczny-fle190', 6.99, NULL, 0, 'product-image-58d9050b47cb0.jpeg', '2017-03-27 14:26:51', NULL, NULL, 0, 'Produkt7', 'Produkt7', NULL, NULL, 0, '', NULL),
(1773, 35, 'Kwiaty sztuczne FLE086', 'kwiaty-sztuczne-fle086', 6.99, NULL, 0, 'product-image-58d90515db565.jpeg', '2017-03-27 14:27:01', NULL, NULL, 0, 'Produkt8', 'Produkt8', NULL, NULL, 0, '', NULL),
(1774, 35, 'Kwiat sztuczny FLD003', 'kwiat-sztuczny-fld003', 6.99, NULL, 0, 'product-image-58d905210874b.jpeg', '2017-03-27 14:27:12', NULL, NULL, 0, 'Produkt9', 'Produkt9', NULL, NULL, 0, '', NULL),
(1775, 35, 'Kwiaty sztuczne FLE024', 'kwiaty-sztuczne-fle024', 6.99, NULL, 0, 'product-image-58d9052deca68.jpeg', '2017-03-27 14:27:25', NULL, NULL, 0, 'Produkt10', 'Produkt10', NULL, NULL, 0, '', NULL),
(1776, 35, 'Kwiaty sztuczne FLR186', 'kwiaty-sztuczne-flr186', 6.99, NULL, 0, 'product-image-58d905392e0d4.jpeg', '2017-03-27 14:27:36', NULL, NULL, 0, 'Produkt11', 'Produkt11', NULL, NULL, 0, '', NULL),
(1777, 35, 'Kwiaty sztuczne FLJ140', 'kwiaty-sztuczne-flj140', 6.99, NULL, 0, 'product-image-58d90542b1818.jpeg', '2017-03-27 14:27:46', NULL, NULL, 0, 'Produkt12', 'Produkt12', NULL, NULL, 0, '', NULL),
(1778, 35, 'Kwiat sztuczny AS00322', 'kwiat-sztuczny-as00322', 5.99, NULL, 0, 'product-image-58d910ae2de7c.jpeg', '2017-03-27 15:16:29', NULL, NULL, 0, 'Produkty13', 'Produkty13', NULL, NULL, 0, '', NULL),
(1779, 35, 'Kwiat sztuczny 11AS0002-1', 'kwiat-sztuczny-11as0002-1', 5.99, NULL, 0, 'product-image-58d910f06091c.jpeg', '2017-03-27 15:17:36', NULL, NULL, 0, 'Produkty14', 'Produkty14', NULL, NULL, 0, '', NULL),
(1780, 35, 'Kwiat sztuczny AST60367A', 'kwiat-sztuczny-ast60367a', 2.99, NULL, 0, 'product-image-58d9114e42ecf.jpeg', '2017-03-27 15:19:09', NULL, NULL, 0, 'Produkty15', 'Produkty15', NULL, NULL, 0, '', NULL),
(1781, 35, 'Kwiat sztuczny ASFLY237', 'kwiat-sztuczny-asfly237', 6.99, NULL, 0, 'product-image-58d9115a8ffda.jpeg', '2017-03-27 15:19:22', NULL, NULL, 0, 'Produkty16', 'Produkty16', '', NULL, 0, '', NULL),
(1782, 35, 'Kwiaty sztuczne FLO028', 'kwiaty-sztuczne-flo028', 6.99, NULL, 0, 'product-image-58d91163bd023.jpeg', '2017-03-27 15:19:31', NULL, NULL, 0, 'Produkty17', 'Produkty17', NULL, NULL, 0, '', NULL),
(1783, 35, 'Kwiat sztuczny FLX019', 'kwiat-sztuczny-flx019', 6.99, NULL, 0, 'product-image-58d9116cbeea9.jpeg', '2017-03-27 15:19:40', NULL, NULL, 0, 'Produkty18', 'Produkty18', NULL, NULL, 0, '', NULL),
(1784, 35, 'Kwiat sztuczny FLO026', 'kwiat-sztuczny-flo026', 6.99, NULL, 0, 'product-image-58d9117623af6.jpeg', '2017-03-27 15:19:49', NULL, NULL, 0, 'Produkty19', 'Produkty19', NULL, NULL, 0, '', NULL),
(1785, 35, 'Kwiat sztuczny FLE406', 'kwiat-sztuczny-fle406', 6.99, NULL, 0, 'product-image-58d911801e586.jpeg', '2017-03-27 15:19:59', NULL, NULL, 0, 'Produkty20', 'Produkty20', NULL, NULL, 0, '', NULL),
(1786, 35, 'Kwiat sztuczny FLE156', 'kwiat-sztuczny-fle156', 6.99, NULL, 0, 'product-image-58d9118aa3d9c.jpeg', '2017-03-27 15:20:10', NULL, NULL, 0, 'Produkty21', 'Produkty21', NULL, NULL, 0, '', NULL),
(1787, 35, 'Kwiat sztuczny FLW051', 'kwiat-sztuczny-flw051', 6.99, NULL, 0, 'product-image-58d91195d773e.jpeg', '2017-03-27 15:20:21', NULL, NULL, 0, 'Produkty22', 'Produkty22', NULL, NULL, 0, '', NULL),
(1788, 35, 'Kwiat sztuczny ASFLY249', 'kwiat-sztuczny-asfly249', 6.99, NULL, 0, 'product-image-58d911bae223c.jpeg', '2017-03-27 15:20:58', NULL, NULL, 0, 'Produkty23', 'Produkty23', NULL, NULL, 0, '', NULL),
(1789, 35, 'Kwiat sztuczny FLJ259', 'kwiat-sztuczny-flj259', 6.99, NULL, 0, 'product-image-58d911c6159a1.jpeg', '2017-03-27 15:21:09', NULL, NULL, 0, 'Produkty24', 'Produkty24', NULL, NULL, 0, '', NULL),
(1790, 35, 'Kwiat sztuczny FLO033', 'kwiat-sztuczny-flo033', 6.99, NULL, 0, 'product-image-58d927354dff9.jpeg', '2017-03-27 16:52:37', NULL, NULL, 0, 'Produkt25', 'Produkt25', NULL, NULL, 0, '', NULL),
(1791, 35, 'Kwiat sztuczny FLJ191', 'kwiat-sztuczny-flj191-1', 6.99, NULL, 0, 'product-image-58d92747cf043.jpeg', '2017-03-27 16:52:55', NULL, NULL, 0, 'Produkt26', 'Produkt26', NULL, NULL, 0, '', NULL),
(1792, 35, 'Kwiat sztuczny AST60369B', 'kwiat-sztuczny-ast60369b', 3.99, NULL, 0, 'product-image-58d92751d4dcf.jpeg', '2017-03-27 16:53:05', NULL, NULL, 0, 'Produkt27', 'Produkt27', NULL, NULL, 0, '', NULL),
(1793, 35, 'Kwiat sztuczny FLJ191', 'kwiat-sztuczny-flj191', 6.99, NULL, 0, 'product-image-58d9275d78cca.jpeg', '2017-03-27 16:53:17', NULL, NULL, 0, 'Produkt28', 'Produkt28', NULL, NULL, 0, '', NULL),
(1794, 35, 'Wiatrak ZL09E1425F', 'wiatrak-zl09e1425f', 3.99, NULL, 0, 'product-image-58d9276e5a2f8.jpeg', '2017-03-27 16:53:34', NULL, NULL, 0, 'Produkt29', 'Produkt29', NULL, NULL, 0, '', NULL),
(1795, 35, 'Gałązka ozdobna AS00283', 'galazka-ozdobna-as00283', 3.99, NULL, 0, 'product-image-58d9277d28bba.jpeg', '2017-03-27 16:53:48', NULL, NULL, 0, 'Produkt30', 'Produkt30', NULL, NULL, 0, '', NULL),
(1796, 35, 'Gałązka ozdobna AS00282', 'galazka-ozdobna-as00282', 2.99, NULL, 0, 'product-image-58d92787d585c.jpeg', '2017-03-27 16:53:59', NULL, NULL, 0, 'Produkt31', 'Produkt31', NULL, NULL, 0, '', NULL),
(1797, 35, 'Gałązka ozdobna AS00281', 'galazka-ozdobna-as00281', 2.99, NULL, 0, 'product-image-58d927977905f.jpeg', '2017-03-27 16:54:15', NULL, NULL, 0, 'Produkt32', 'Produkt32', NULL, NULL, 0, '', NULL),
(1798, 35, 'Kula bukszpanowa 9cm AST62478A', 'kula-bukszpanowa-9cm-ast62478a', 2.99, NULL, 0, 'product-image-58d927a1ef13d.jpeg', '2017-03-27 16:54:25', NULL, NULL, 0, 'Produkt33', 'Produkt33', NULL, NULL, 0, '', NULL),
(1799, 35, 'Kula bukszpanowa 15cm AS00299', 'kula-bukszpanowa-15cm-as00299', 3.99, NULL, 0, 'product-image-58d927ac0036d.jpeg', '2017-03-27 16:54:35', NULL, NULL, 0, 'Produkt34', 'Produkt34', NULL, NULL, 0, '', NULL),
(1800, 35, 'Kula bukszpanowa 23cm AST62483A', 'kula-bukszpanowa-23cm-ast62483a', 11.98, NULL, 0, 'product-image-58d927b62280a.jpeg', '2017-03-27 16:54:45', NULL, NULL, 0, 'Produkt35', 'Produkt35', NULL, NULL, 0, '', NULL),
(1801, 35, 'Gąbka florystyczna ASU0646', 'produkt36-2', 3.99, NULL, 0, 'product-image-58d927c0b97d1.jpeg', '2017-03-27 16:54:56', NULL, NULL, 0, 'Produkt36', 'Produkt36', NULL, NULL, 0, '', NULL),
(1802, 38, 'Kpl. garnków Metlex 3szt.(24cm,20cm,16cm) MX8095, MX8094, MX8093', 'kpl-garnkow-metlex-3szt-24cm-20cm-16cm-mx8095-mx8094-mx8093', 209.70, NULL, 0, 'product-image-58d942e106743.jpeg', '2017-03-27 18:50:40', NULL, NULL, 0, 'Produkt1', 'Produkt1', NULL, NULL, 0, '', NULL),
(1803, 38, 'Pojemnik szklany z pokrywką kpl.3szt. OZ3904', 'pojemnik-szklany-z-pokrywka-kpl-3szt-oz3904', 3.99, NULL, 0, 'product-image-58d942ed684ea.jpeg', '2017-03-27 18:50:53', NULL, NULL, 0, 'Produkt2', 'Produkt2', NULL, NULL, 0, '', NULL),
(1804, 38, 'Pojemnik kpl. 3szt.', 'pojemnik-kpl-3szt', 5.99, NULL, 0, 'product-image-58d942f82d6c2.jpeg', '2017-03-27 18:51:03', NULL, NULL, 0, 'Produkt3', 'Produkt3', NULL, NULL, 0, '', NULL),
(1805, 38, 'Cukiernica z dozownikiem AS00407', 'cukiernica-z-dozownikiem-as00407', 2.99, NULL, 0, 'product-image-58d94304c0531.jpeg', '2017-03-27 18:51:16', NULL, NULL, 0, 'Produkt4', 'Produkt4', NULL, NULL, 0, '', NULL),
(1806, 38, 'MISKA RX-N15574', 'miska-rx-n15574', 5.99, NULL, 0, 'product-image-58d94310b00c5.jpeg', '2017-03-27 18:51:28', NULL, NULL, 0, 'Produkt5', 'Produkt5', NULL, NULL, 0, '', NULL),
(1807, 38, 'MISKA RX-N15572', 'miska-rx-n15572', 5.99, NULL, 0, 'product-image-58d9431f883e0.jpeg', '2017-03-27 18:51:43', NULL, NULL, 0, 'Produkt6', 'Produkt6', NULL, NULL, 0, '', NULL),
(1808, 38, 'Miska RX-N15568', 'miska-rx-n15568', 5.99, NULL, 0, 'product-image-58d94329c0a07.jpeg', '2017-03-27 18:51:53', NULL, NULL, 0, 'Produkt7', 'Produkt7', NULL, NULL, 0, '', NULL),
(1809, 38, 'Miska RX-N15570', 'miska-rx-n15570', 5.99, NULL, 0, 'product-image-58d94334d1c3e.jpeg', '2017-03-27 18:52:04', NULL, NULL, 0, 'Produkt8', 'Produkt8', NULL, NULL, 0, '', NULL),
(1810, 38, 'Kubek ceramiczny RX-N15571', 'kubek-ceramiczny-rx-n15571', 4.99, NULL, 0, 'product-image-58d9434103498.jpeg', '2017-03-27 18:52:16', NULL, NULL, 0, 'Produkt9', 'Produkt9', NULL, NULL, 0, '', NULL),
(1811, 34, 'Kpl. łazienkowy AST62270', 'kpl-lazienkowy-ast62270', 29.95, NULL, 0, 'product-image-58d955f6277ce.jpeg', '2017-03-27 20:12:05', NULL, NULL, 0, 'Produkt31', 'Produkt31', NULL, NULL, 0, '', NULL),
(1812, 34, 'Kpl. turystyczny', 'kpl-turystyczny', 2.99, NULL, 0, 'product-image-58d95603afdf0.jpeg', '2017-03-27 20:12:19', NULL, NULL, 0, 'Produkt32', 'Produkt32', NULL, NULL, 0, '', NULL),
(1813, 34, 'Pojemnik na mydło w płynie AS00028', 'pojemnik-na-mydlo-w-plynie-as00028', 2.99, NULL, 0, 'product-image-58d9560d52774.jpeg', '2017-03-27 20:12:29', NULL, NULL, 0, 'Produkt33', 'Produkt33', NULL, NULL, 0, '', NULL),
(1814, 34, 'Odświerzacz powietrza-kulki żelowe', 'odswierzacz-powietrza-kulki-zelowe', 4.99, NULL, 0, 'product-image-58d9561a3dbc6.jpeg', '2017-03-27 20:12:41', NULL, NULL, 0, 'Produkt34', 'Produkt34', NULL, NULL, 0, '', NULL),
(1815, 34, 'Mgiełka zapachowa', 'mgielka-zapachowa', 5.99, NULL, 0, 'product-image-58d956281103d.jpeg', '2017-03-27 20:12:55', NULL, NULL, 0, 'Produkt35', 'Produkt35', NULL, NULL, 0, '', NULL),
(1816, 34, 'Podgrzewacze zapachowe', 'podgrzewacze-zapachowe', 1.99, NULL, 0, 'product-image-58d956341323c.jpeg', '2017-03-27 20:13:07', NULL, NULL, 0, 'Produkt36', 'Produkt36', NULL, NULL, 0, '', NULL),
(1817, 34, 'Lusterko 4\" AST60177', 'lusterko-4-ast60177', 2.99, NULL, 0, 'product-image-58d9563dc2749.jpeg', '2017-03-27 20:13:17', NULL, NULL, 0, 'Produkt37', 'Produkt37', NULL, NULL, 0, '', NULL),
(1818, 34, 'Lusterko 5\" AS00134', 'lusterko-5-as00134', 3.99, NULL, 0, 'product-image-58d9564755569.jpeg', '2017-03-27 20:13:27', NULL, NULL, 0, 'Produkt38', 'Produkt38', NULL, NULL, 0, '', NULL),
(1819, 34, 'Lusterko 7\" AS00649', 'lusterko-7-as00649', 5.99, NULL, 0, 'product-image-58d95652ea543.jpeg', '2017-03-27 20:13:38', NULL, NULL, 0, 'Produkt39', 'Produkt39', NULL, NULL, 0, '', NULL),
(1820, 38, 'Patelnia IDEAL 24cm', 'patelnia-ideal-24cm', 35.94, NULL, 0, 'product-image-58da9b5c8b327.jpeg', '2017-03-28 19:20:27', NULL, NULL, 0, 'Produkt10', 'Produkt10', NULL, NULL, 0, '', NULL),
(1821, 38, 'Miska 24cm kolor MX0026CL', 'miska-24cm-kolor-mx0026cl', 5.99, NULL, 0, 'product-image-58da9b6a87318.jpeg', '2017-03-28 19:20:42', NULL, NULL, 0, 'Produkt11', 'Produkt11', NULL, NULL, 0, '', NULL),
(1822, 38, 'Cedzak głęboki 22cm MX0022A', 'cedzak-gleboki-22cm-mx0022a', 5.99, NULL, 0, 'product-image-58da9b7d4fb26.jpeg', '2017-03-28 19:21:00', NULL, NULL, 0, 'Produkt12', 'Produkt12', NULL, NULL, 0, '', NULL),
(1823, 38, 'Kubek metalowy Y027122', 'kubek-metalowy-y027122', 2.99, NULL, 0, 'product-image-58da9b8e9104b.jpeg', '2017-03-28 19:21:18', NULL, NULL, 0, 'Produkt13', 'Produkt13', NULL, NULL, 0, '', NULL),
(1824, 38, 'Tłuczek spiralny do ziemniaków - metalowy AS00167', 'tluczek-spiralny-do-ziemniakow-metalowy-as00167', 3.99, NULL, 0, 'product-image-58da9ba5dddc8.jpeg', '2017-03-28 19:21:41', NULL, NULL, 0, 'Produkt14', 'Produkt14', NULL, NULL, 0, '', NULL),
(1825, 38, 'Tłuczek spiralny do ziemniaków', 'tluczek-spiralny-do-ziemniakow', 2.99, NULL, 0, 'product-image-58da9bb1446b5.jpeg', '2017-03-28 19:21:53', NULL, NULL, 0, 'Produkt15', 'Produkt15', NULL, NULL, 0, '', NULL),
(1826, 38, 'Łyżka do lodów AST60328', 'lyzka-do-lodow-ast60328', 5.99, NULL, 0, 'product-image-58da9bbb748e8.jpeg', '2017-03-28 19:22:03', NULL, NULL, 0, 'Produkt16', 'Produkt16', NULL, NULL, 0, '', NULL),
(1827, 38, 'Praska do czosnku AS00170', 'praska-do-czosnku-as00170', 8.97, NULL, 0, 'product-image-58da9bc5d6f7e.jpeg', '2017-03-28 19:22:13', NULL, NULL, 0, 'Produkt17', 'Produkt17', NULL, NULL, 0, '', NULL),
(1828, 38, 'Deska bambusowa AST62178', 'deska-bambusowa-ast62178', 5.99, NULL, 0, 'product-image-58da9bd0b5b48.jpeg', '2017-03-28 19:22:24', NULL, NULL, 0, 'Produkt18', 'Produkt18', NULL, NULL, 0, '', NULL),
(1829, 38, 'Łyżeczki kpl.6szt. NP-Paris', 'lyzeczki-kpl-6szt-np-paris', 3.99, NULL, 0, 'product-image-58da9bdb89794.jpeg', '2017-03-28 19:22:35', NULL, NULL, 0, 'Produkt19', 'Produkt19', NULL, NULL, 0, '', NULL),
(1830, 38, 'Noże kpl. 6 szt. NP-Paris', 'noze-kpl-6-szt-np-paris', 9.95, NULL, 0, 'product-image-58da9be5c7829.jpeg', '2017-03-28 19:22:45', NULL, NULL, 0, 'Produkt20', 'Produkt20', NULL, NULL, 0, '', NULL),
(1831, 38, 'Widelce kpl 6 szt. NP - Paris', 'widelce-kpl-6-szt-np-paris', 5.99, NULL, 0, 'product-image-58da9beec16c7.jpeg', '2017-03-28 19:22:54', NULL, NULL, 0, 'Produkt21', 'Produkt21', NULL, NULL, 0, '', NULL),
(1832, 38, 'Łyżeczki długie kpl. 6 szt. NP-Paris', 'lyzeczki-dlugie-kpl-6-szt-np-paris', 5.99, NULL, 0, 'product-image-58da9bf8e72cc.jpeg', '2017-03-28 19:23:04', NULL, NULL, 0, 'Produkt22', 'Produkt22', NULL, NULL, 0, '', NULL),
(1833, 38, 'Łyżka obidowa kpl.6 szt.  NP-Paris', 'lyzka-obidowa-kpl-6-szt-np-paris', 5.99, NULL, 0, 'product-image-58da9c1087fa0.jpeg', '2017-03-28 19:23:28', NULL, NULL, 0, 'Produkt23', 'Produkt23', NULL, NULL, 0, '', NULL),
(1834, 38, 'Ociekacz na sztućce', 'ociekacz-na-sztucce', 3.99, NULL, 0, 'product-image-58da9c1f843de.jpeg', '2017-03-28 19:23:43', NULL, NULL, 0, 'Produkt24', 'Produkt24', NULL, NULL, 0, '', NULL),
(1835, 38, 'Przecierak nierdzewny', 'przecierak-nierdzewny', 8.97, NULL, 0, 'product-image-58da9c2b1ca09.jpeg', '2017-03-28 19:23:54', NULL, NULL, 0, 'Produkt25', 'Produkt25', NULL, NULL, 0, '', NULL),
(1836, 38, 'Kubek metalowy 11315K', 'kubek-metalowy-11315k', 2.99, NULL, 0, 'product-image-58da9c35c27d6.jpeg', '2017-03-28 19:24:05', NULL, NULL, 0, 'Produkt26', 'Produkt26', NULL, NULL, 0, '', NULL),
(1837, 38, '3 częściowy kpl. garnków do gotowania na parze MX7010', '3-czesciowy-kpl-garnkow-do-gotowania-na-parze-mx7010', 44.95, NULL, 0, 'product-image-58da9c41ac0b3.jpeg', '2017-03-28 19:24:17', NULL, NULL, 0, 'Produkt27', 'Produkt27', NULL, NULL, 0, '', NULL),
(1838, 37, 'Świecznik FLS1513M-P', 'swiecznik-fls1513m-p', 8.97, NULL, 0, 'product-image-58dbeef29d117.jpeg', '2017-03-29 19:29:22', NULL, NULL, 0, 'Produkt1', 'Produkt1', NULL, NULL, 0, '', NULL),
(1839, 37, 'Świecznik FLS1110M-P', 'swiecznik-fls1110m-p', 5.99, NULL, 0, 'product-image-58dbef057e831.jpeg', '2017-03-29 19:29:41', NULL, NULL, 0, 'Produkt2', 'Produkt2', NULL, NULL, 0, '', NULL),
(1840, 37, 'Świecznik FLS1618CT', 'swiecznik-fls1618ct', 14.95, NULL, 0, 'product-image-58dbef138095b.jpeg', '2017-03-29 19:29:55', NULL, NULL, 0, 'Produkt3', 'Produkt3', NULL, NULL, 0, '', NULL),
(1841, 37, 'Lampki Cotton Balls 20szt', 'lampki-cotton-balls-20szt-3', 19.90, 29.95, 0, 'product-image-58dbef1f23ff9.jpeg', '2017-03-29 19:30:06', NULL, NULL, 0, 'Produkt4', 'Produkt4', NULL, NULL, 0, '', NULL),
(1842, 37, 'Lampki Cotton Balls 20szt.', 'lampki-cotton-balls-20szt-2', 19.90, 29.95, 0, 'product-image-58dbef304f1fa.jpeg', '2017-03-29 19:30:24', NULL, NULL, 0, 'Produkt5', 'Produkt5', NULL, NULL, 0, '', NULL),
(1843, 37, 'Lampki Cotton Balls 10szt.', 'lampki-cotton-balls-10szt-3', 9.99, 17.97, 0, 'product-image-58dbef8d1b32e.jpeg', '2017-03-29 19:31:56', NULL, NULL, 0, 'Produkt6', 'Produkt6', NULL, NULL, 0, '', NULL),
(1844, 37, 'Lampki Cotton Balls 20szt.', 'lampki-cotton-balls-20szt-1', 19.90, 29.95, 0, 'product-image-58dbef9690848.jpeg', '2017-03-29 19:32:06', NULL, NULL, 0, 'Produkt7', 'Produkt7', NULL, NULL, 0, '', NULL),
(1845, 37, 'Lampki Cotton Balls 20szt.', 'lampki-cotton-balls-20szt', 19.90, 29.95, 0, 'product-image-58dbefa271cea.jpeg', '2017-03-29 19:32:18', NULL, NULL, 0, 'Produkt8', 'Produkt8', NULL, NULL, 0, '', NULL),
(1846, 37, 'Lampki Cotton Balls 10szt.', 'lampki-cotton-balls-10szt-2', 9.99, 17.79, 0, 'product-image-58dbefac3c49e.jpeg', '2017-03-29 19:32:27', NULL, NULL, 0, 'Produkt9', 'Produkt9', NULL, NULL, 0, '', NULL),
(1847, 37, 'Lampki Cotton Balls 10szt.', 'lampki-cotton-balls-10szt-1', 9.99, 17.97, 0, 'product-image-58dbefb72b981.jpeg', '2017-03-29 19:32:38', NULL, NULL, 0, 'Produkt10', 'Produkt10', NULL, NULL, 0, '', NULL),
(1848, 37, 'Lampki Cotton Balls 10szt', 'lampki-cotton-balls-10szt', 9.99, 17.97, 0, 'product-image-58dbefc36ac65.jpeg', '2017-03-29 19:32:51', NULL, NULL, 0, 'Produkt11', 'Produkt11', NULL, NULL, 0, '', NULL),
(1849, 37, 'Lampion', 'lampion', 9.99, NULL, 0, 'product-image-58dd571de357b.jpeg', '2017-03-30 21:06:05', NULL, NULL, 0, 'Produkt12', 'Produkt12', '', NULL, 0, '', NULL),
(1850, 37, 'Lampion AS00306', 'lampion-as00306', 17.97, NULL, 0, 'product-image-58dd572e91c36.jpeg', '2017-03-30 21:06:22', NULL, NULL, 0, 'Produkt13', 'Produkt13', '', NULL, 0, '', NULL),
(1851, 37, 'Kula rattanowa AS00296', 'kula-rattanowa-as00296', 5.99, NULL, 0, 'product-image-58dd5741d6383.jpeg', '2017-03-30 21:06:41', NULL, NULL, 0, 'Produkt14', 'Produkt14', '', NULL, 0, '', NULL),
(1852, 37, 'Serce rattanowe DXQ15-538', 'serce-rattanowe-dxq15-538', 5.99, NULL, 0, 'product-image-58dd575274146.jpeg', '2017-03-30 21:06:58', NULL, NULL, 0, 'Produkt15', 'Produkt15', '', NULL, 0, '', NULL),
(1853, 37, 'Serce rattanowe AS00305', 'serce-rattanowe-as00305', 5.99, NULL, 0, 'product-image-58dd57665ab7a.jpeg', '2017-03-30 21:07:18', NULL, NULL, 0, 'Produkt16', 'Produkt16', '', NULL, 0, '', NULL),
(1854, 37, 'Serce rattanowe', 'serce-rattanowe', 8.97, NULL, 0, 'product-image-58dd5781c9f1c.jpeg', '2017-03-30 21:07:45', NULL, NULL, 0, 'Produkt17', 'Produkt17', '', NULL, 0, '', NULL),
(1855, 37, 'Serce rattanowe małe DXQ15-542', 'serce-rattanowe-male-dxq15-542', 3.99, NULL, 0, 'product-image-58dd57930dbab.jpeg', '2017-03-30 21:08:02', NULL, NULL, 0, 'Produkt18', 'Produkt18', '', NULL, 0, '', NULL),
(1856, 37, 'Serduszka rattanowe DXQ15-585', 'serduszka-rattanowe-dxq15-585', 11.98, NULL, 0, 'product-image-58dd57a6ddefe.jpeg', '2017-03-30 21:08:22', NULL, NULL, 0, 'Produkt19', 'Produkt19', '', NULL, 0, '', NULL),
(1857, 37, 'Serce rattanowe DXQ15-545', 'serce-rattanowe-dxq15-545', 11.98, NULL, 0, 'product-image-58dd57bc24bde.jpeg', '2017-03-30 21:08:43', NULL, NULL, 0, 'Produkt20', 'Produkt20', '', NULL, 0, '', NULL),
(1858, 37, 'Pufa do przechowywania AS00183', 'pufa-do-przechowywania-as00183', 23.96, NULL, 0, 'product-image-58dd57d08c66d.jpeg', '2017-03-30 21:09:04', NULL, NULL, 0, 'Produkt21', 'Produkt21', '', NULL, 0, '', NULL),
(1859, 37, 'Organizer - pojemnik do przechowywania PTR-28', 'organizer-pojemnik-do-przechowywania-ptr-28', 3.99, NULL, 0, 'product-image-58dd58028e5cb.jpeg', '2017-03-30 21:09:54', NULL, NULL, 0, 'Produkt22', 'Produkt22', '', NULL, 0, '', NULL),
(1860, 37, 'Pudełko AS00265', 'pudelko-as00265', 17.97, NULL, 0, 'product-image-58dd5811a32db.jpeg', '2017-03-30 21:10:09', NULL, NULL, 0, 'Produkt23', 'Produkt23', '', NULL, 0, '', NULL),
(1861, 37, 'Pudełko AS00184', 'pudelko-as00184', 5.99, NULL, 0, 'product-image-58dd581d94b12.jpeg', '2017-03-30 21:10:21', NULL, NULL, 0, 'Produkt24', 'Produkt24', '', NULL, 0, '', NULL),
(1862, 37, 'Organizer PTR-24', 'organizer-ptr-24', 8.97, NULL, 0, 'product-image-58dd5832ba64f.jpeg', '2017-03-30 21:10:42', NULL, NULL, 0, 'Produkt25', 'Produkt25', '', NULL, 0, '', NULL),
(1863, 37, 'Sizal dekoracyjny AST60469', 'sizal-dekoracyjny-ast60469', 1.99, NULL, 0, 'product-image-58dd584873afd.jpeg', '2017-03-30 21:11:03', NULL, NULL, 0, 'Produkt26', 'Produkt26', '', NULL, 0, '', NULL),
(1864, 37, 'Susz dekoracyjny ASI6003', 'susz-dekoracyjny-asi6003', 5.99, NULL, 0, 'product-image-58dd5855f022c.jpeg', '2017-03-30 21:11:17', NULL, NULL, 0, 'Produkt27', 'Produkt27', '', NULL, 0, '', NULL),
(1865, 37, 'Susz dekoracyjny ASI6004', 'susz-dekoracyjny-asi6004', 5.99, NULL, 0, 'product-image-58dd586caa405.jpeg', '2017-03-30 21:11:40', NULL, NULL, 0, 'Produkt28', 'Produkt28', '', NULL, 0, '', NULL),
(1866, 37, 'Motyl dekioracyjny 3szt.', 'motyl-dekioracyjny-3szt', 5.99, NULL, 0, 'product-image-58dd58887dc58.jpeg', '2017-03-30 21:12:08', NULL, NULL, 0, 'Produkt29', 'Produkt29', '', NULL, 0, '', NULL),
(1867, 37, 'Motyl dekoracyjny 3szt. SSB-01', 'motyl-dekoracyjny-3szt-ssb-01', 5.99, NULL, 0, 'product-image-58dd58963dbf7.jpeg', '2017-03-30 21:12:21', NULL, NULL, 0, 'Produkt30', 'Produkt30', '', NULL, 0, '', NULL),
(1868, 37, 'Motyl dekoracyjny 3szt.', 'motyl-dekoracyjny-3szt', 5.99, NULL, 0, 'product-image-58dd58a2d7fd7.jpeg', '2017-03-30 21:12:34', NULL, NULL, 0, 'Produkt31', 'Produkt31', '', NULL, 0, '', NULL),
(1869, 37, 'Motyl dekoracyjny 3szt. SSB-05', 'motyl-dekoracyjny-3szt-ssb-05', 5.99, NULL, 0, 'product-image-58dd58b42bb0d.jpeg', '2017-03-30 21:12:51', NULL, NULL, 0, 'Produkt32', 'Produkt32', '', NULL, 0, '', NULL),
(1870, 37, 'Motyl dekoracyjny 3szt. SSB-09', 'motyl-dekoracyjny-3szt-ssb-09', 5.99, NULL, 0, 'product-image-58dd58ceaf8d3.jpeg', '2017-03-30 21:13:18', NULL, NULL, 0, 'Produkt33', 'Produkt33', '', NULL, 0, '', NULL),
(1871, 37, 'Art.dekoracyjny organza AST60467', 'art-dekoracyjny-organza-ast60467', 11.98, NULL, 0, 'product-image-58dd58da77571.jpeg', '2017-03-30 21:13:30', NULL, NULL, 0, 'Produkt34', 'Produkt34', '', NULL, 0, '', NULL),
(1872, 37, 'Organza AS00347', 'organza-as00347', 5.99, NULL, 0, 'product-image-58dd58e6145af.jpeg', '2017-03-30 21:13:41', NULL, NULL, 0, 'Produkt35', 'Produkt35', '', NULL, 0, '', NULL),
(1873, 37, 'Organza dekoracyjna', 'organza-dekoracyjna', 11.98, NULL, 0, 'product-image-58dd58fe0d3f3.jpeg', '2017-03-30 21:14:05', NULL, NULL, 0, 'Produkt36', 'Produkt36', '', NULL, 0, '', NULL),
(1874, 37, 'Kominek ceramiczny MF-03', 'kominek-ceramiczny-mf-03', 3.99, NULL, 0, 'product-image-58dd590b71115.jpeg', '2017-03-30 21:14:19', NULL, NULL, 0, 'Produkt37', 'Produkt37', '', NULL, 0, '', NULL),
(1875, 37, 'Osłonka na doniczke', 'oslonka-na-doniczke-1', 3.99, NULL, 0, 'product-image-58dd592279cad.jpeg', '2017-03-30 21:14:42', NULL, NULL, 0, 'Produkt38', 'Produkt38', '', NULL, 0, '', NULL),
(1876, 37, 'Osłonka na doniczkę', 'oslonka-na-doniczke', 3.99, NULL, 0, 'product-image-58dd592fd549e.jpeg', '2017-03-30 21:14:55', NULL, NULL, 0, 'Produkt39', 'Produkt39', '', NULL, 0, '', NULL),
(1878, 37, 'Osłonka na doniczkę ASAP150936A', 'oslonka-na-doniczke-asap150936a', 1.99, NULL, 0, 'product-image-58dd595a3fd0f.jpeg', '2017-03-30 21:15:37', NULL, NULL, 0, 'Produkt41', 'Produkt41', '', NULL, 0, '', NULL),
(1879, 37, 'Osłonka metalowa EVG20-4', 'oslonka-metalowa-evg20-4', 5.99, NULL, 0, 'product-image-58dd596a451b7.jpeg', '2017-03-30 21:15:54', NULL, NULL, 0, 'Produkt42', 'Produkt42', '', NULL, 0, '', NULL),
(1880, 37, 'Osłonka metalowa EVG005', 'oslonka-metalowa-evg005', 5.99, NULL, 0, 'product-image-58dd597531d8a.jpeg', '2017-03-30 21:16:04', NULL, NULL, 0, 'Produkt43', 'Produkt43', '', NULL, 0, '', NULL),
(1881, 37, 'Osłonka metalowa na doniczkę EVG006', 'oslonka-metalowa-na-doniczke-evg006', 5.99, NULL, 0, 'product-image-58dd598084403.jpeg', '2017-03-30 21:16:16', NULL, NULL, 0, 'Produkt44', 'Produkt44', '', NULL, 0, '', NULL),
(1882, 36, 'Ręcznik', 'recznik', 6.99, NULL, 0, 'product-image-58e2279fc6ef3.jpeg', '2017-04-03 12:44:47', NULL, NULL, 0, 'Produkt1', 'Produkt1', '', NULL, 0, '', NULL),
(1883, 36, 'Rajstopy damskie 20den', 'rajstopy-damskie-20den', 4.49, NULL, 0, 'product-image-58e227ad500c2.jpeg', '2017-04-03 12:45:00', NULL, NULL, 0, 'Produkt2', 'Produkt2', '', NULL, 0, '', NULL),
(1884, 36, 'Koc polarowy 130x160 WFS-02', 'koc-polarowy-130x160-wfs-02', 17.97, NULL, 0, 'product-image-58e227bbee662.jpeg', '2017-04-03 12:45:15', NULL, NULL, 0, 'Produkt3', 'Produkt3', '', NULL, 0, '', NULL),
(1885, 36, 'Rajstopy dziecięce', 'rajstopy-dzieciece', 5.99, NULL, 0, 'product-image-58e227c5bb1f0.jpeg', '2017-04-03 12:45:25', NULL, NULL, 0, 'Produkt4', 'Produkt4', '', NULL, 0, '', NULL),
(1886, 36, 'Apaszka', 'apaszka', 5.99, NULL, 0, 'product-image-58e227d167998.jpeg', '2017-04-03 12:45:36', NULL, NULL, 0, 'Produkt5', 'Produkt5', '', NULL, 0, '', NULL),
(1887, 36, 'Podkoszulek męski gładki', 'podkoszulek-meski-gladki', 5.99, NULL, 0, 'product-image-58e227dbb1f2f.jpeg', '2017-04-03 12:45:47', NULL, NULL, 0, 'Produkt6', 'Produkt6', '', NULL, 0, '', NULL),
(1888, 36, 'Leginsy damskie', 'leginsy-damskie', 9.95, NULL, 0, 'product-image-58e227e6b9953.jpeg', '2017-04-03 12:45:58', NULL, NULL, 0, 'Produkt7', 'Produkt7', '', NULL, 0, '', NULL),
(1889, 36, 'Stopki męskie/damskie', 'stopki-meskie-damskie', 1.99, NULL, 0, 'product-image-58e227f146243.jpeg', '2017-04-03 12:46:09', NULL, NULL, 0, 'Produkt8', 'Produkt8', '', NULL, 0, '', NULL),
(1890, 36, 'Bokserki chłopięce', 'bokserki-chlopiece', 5.99, NULL, 0, 'product-image-58e227fcb6b4d.jpeg', '2017-04-03 12:46:20', NULL, NULL, 0, 'Produkt9', 'Produkt9', '', NULL, 0, '', NULL),
(1891, 36, 'Skarpety męskie', 'skarpety-meskie', 5.99, NULL, 0, 'product-image-58e2280876a6f.jpeg', '2017-04-03 12:46:32', NULL, NULL, 0, 'Produkt10', 'Produkt10', '', NULL, 0, '', NULL),
(1892, 36, 'Majtki damskie', 'majtki-damskie', 3.99, NULL, 0, 'product-image-58e2282e62efe.jpeg', '2017-04-03 12:47:09', NULL, NULL, 0, 'Produkt11', 'Produkt11', '', NULL, 0, '', NULL),
(1893, 36, 'Majtki dziewczęce', 'majtki-dziewczece', 5.99, NULL, 0, 'product-image-58e2283918751.jpeg', '2017-04-03 12:47:20', NULL, NULL, 0, 'Produkt12', 'Produkt12', '', NULL, 0, '', NULL),
(1894, 36, 'Skarpety damskie', 'skarpety-damskie', 5.99, NULL, 0, 'product-image-58e22849d6f9c.jpeg', '2017-04-03 12:47:37', NULL, NULL, 0, 'Produkt13', 'Produkt13', '', NULL, 0, '', NULL),
(1895, 36, 'Skarpety dziecięce', 'skarpety-dzieciece', 5.99, NULL, 0, 'product-image-58e22855f0d3a.jpeg', '2017-04-03 12:47:49', NULL, NULL, 0, 'Produkt14', 'Produkt14', '', NULL, 0, '', NULL),
(1896, 36, 'Bokserki męskie', 'bokserki-meskie', 5.99, NULL, 0, 'product-image-58e228630b670.jpeg', '2017-04-03 12:48:02', NULL, NULL, 0, 'Produkt15', 'Produkt15', '', NULL, 0, '', NULL),
(1897, 36, 'Leginsy dziewczęce', 'leginsy-dziewczece', 9.95, NULL, 0, 'product-image-58e2286fdf14e.jpeg', '2017-04-03 12:48:15', NULL, NULL, 0, 'Produkt16', 'Produkt16', '', NULL, 0, '', NULL),
(1898, 36, 'Biustonosz', 'biustonosz-1', 9.95, NULL, 0, 'product-image-58e2287ff1f09.jpeg', '2017-04-03 12:48:31', NULL, NULL, 0, 'Produkt17', 'Produkt17', '', NULL, 0, '', NULL),
(1899, 36, 'Biustonosz', 'biustonosz', 9.95, NULL, 0, 'product-image-58e2288c32121.jpeg', '2017-04-03 12:48:43', NULL, NULL, 0, 'Produkt18', 'Produkt18', '', NULL, 0, '', NULL),
(1900, 36, 'Podkolanówki damskie z microfibry', 'podkolanowki-damskie-z-microfibry', 1.99, NULL, 0, 'product-image-58e22898e7ecf.jpeg', '2017-04-03 12:48:56', NULL, NULL, 0, 'Produkt19', 'Produkt19', '', NULL, 0, '', NULL),
(1901, 36, 'Skarpetki dziewczęce', 'skarpetki-dziewczece', 2.99, NULL, 0, 'product-image-58e228a5758c3.jpeg', '2017-04-03 12:49:08', NULL, NULL, 0, 'Produkt20', 'Produkt20', '', NULL, 0, '', NULL),
(1902, 36, 'Skarpetki dziecięce z falbanką', 'skarpetki-dzieciece-z-falbanka', 2.99, NULL, 0, 'product-image-58e22f0f50207.jpeg', '2017-04-03 13:16:30', NULL, NULL, 0, 'Produkt21', 'Produkt21', '', NULL, 0, '', NULL),
(1903, 36, 'Podkolanówki dziecięce', 'podkolanowki-dzieciece', 1.99, NULL, 0, 'product-image-58e22f1cce119.jpeg', '2017-04-03 13:16:44', NULL, NULL, 0, 'Produkt22', 'Produkt22', '', NULL, 0, '', NULL),
(1904, 36, 'Podkolanówki damskie', 'podkolanowki-damskie', 2.99, NULL, 0, 'product-image-58e22f283893b.jpeg', '2017-04-03 13:16:55', NULL, NULL, 0, 'Produkt23', 'Produkt23', '', NULL, 0, '', NULL),
(1905, 36, 'Stopki damskie - baleriny', 'stopki-damskie-baleriny', 1.49, NULL, 0, 'product-image-58e22f32e2980.jpeg', '2017-04-03 13:17:06', NULL, NULL, 0, 'Produkt24', 'Produkt24', '', NULL, 0, '', NULL),
(1906, 36, 'Tenisówki AS00659', 'tenisowki-as00659', 9.95, NULL, 0, 'product-image-58e22f42e5605.jpeg', '2017-04-03 13:17:22', NULL, NULL, 0, 'Produkt25', 'Produkt25', '', NULL, 0, '', NULL),
(1907, 36, 'Kapcie AS00220', 'kapcie-as00220', 5.99, NULL, 0, 'product-image-58e22f4fcea95.jpeg', '2017-04-03 13:17:35', NULL, NULL, 0, 'Produkt26', 'Produkt26', '', NULL, 0, '', NULL),
(1908, 36, 'Kapelusz AST60500', 'kapelusz-ast60500', 5.99, NULL, 0, 'product-image-58e22f5c0b94e.jpeg', '2017-04-03 13:17:47', NULL, NULL, 0, 'Produkt27', 'Produkt27', '', NULL, 0, '', NULL),
(1909, 36, 'Kapelusz AST60489', 'kapelusz-ast60489', 5.99, NULL, 0, 'product-image-58e22f67c955f.jpeg', '2017-04-03 13:17:58', NULL, NULL, 0, 'Produkt28', 'Produkt28', '', NULL, 0, '', NULL),
(1910, 36, 'Kapcie męskie skórzane', 'kapcie-meskie-skorzane', 9.95, NULL, 0, 'product-image-58e22f7369c41.jpeg', '2017-04-03 13:18:11', NULL, NULL, 0, 'Produkt29', 'Produkt29', '', NULL, 0, '', NULL),
(1911, 36, 'Parasol AST60699', 'parasol-ast60699', 5.99, NULL, 0, 'product-image-58e22f8356eb2.jpeg', '2017-04-03 13:18:27', NULL, NULL, 0, 'Produkt30', 'Produkt30', '', NULL, 0, '', NULL),
(1912, 36, 'Parasol KP0482', 'parasol-kp0482', 5.99, NULL, 0, 'product-image-58e2663d4e92d.jpeg', '2017-04-03 17:11:56', NULL, NULL, 0, 'Produkt31', 'Produkt31', '', NULL, 0, '', NULL),
(1913, 36, 'Kapcie damskie skórzane', 'kapcie-damskie-skorzane', 9.95, NULL, 0, 'product-image-58e2664e0beae.jpeg', '2017-04-03 17:12:13', NULL, NULL, 0, 'Produkt32', 'Produkt32', '', NULL, 0, '', NULL),
(1914, 36, 'Japonki AST60517', 'japonki-ast60517', 9.99, NULL, 0, 'product-image-58e2665aec68e.jpeg', '2017-04-03 17:12:26', NULL, NULL, 0, 'Produkt33', 'Produkt33', '', NULL, 0, '', NULL),
(1915, 36, 'Japonki AST60512', 'japonki-ast60512', 5.99, NULL, 0, 'product-image-58e2666806845.jpeg', '2017-04-03 17:12:39', NULL, NULL, 0, 'Produkt34', 'Produkt34', '', NULL, 0, '', NULL),
(1916, 36, 'Kroksy ASP0253', 'kroksy-asp0253', 8.97, 9.95, 0, 'product-image-58e26674131fa.jpeg', '2017-04-03 17:12:51', NULL, NULL, 0, 'Produkt35', 'Produkt35', '', NULL, 0, '', NULL),
(1917, 36, 'Okulary przeciwsłoneczne damskie/męskie', 'okulary-przeciwsloneczne-damskie-meskie', 5.99, NULL, 0, 'product-image-58e26680530a5.jpeg', '2017-04-03 17:13:04', NULL, NULL, 0, 'Produkt36', 'Produkt36', '', NULL, 0, '', NULL),
(1918, 36, 'Okulary przeciwsłoneczne', 'okulary-przeciwsloneczne', 5.99, NULL, 0, 'product-image-58e2668af3fb9.jpeg', '2017-04-03 17:13:14', NULL, NULL, 0, 'Produkt37', 'Produkt37', '', NULL, 0, '', NULL),
(1919, 36, 'Etui na okulary - AST62086', 'etui-na-okulary-ast62086', 3.99, NULL, 0, 'product-image-58e2669655179.jpeg', '2017-04-03 17:13:26', NULL, NULL, 0, 'Produkt38', 'Produkt38', '', NULL, 0, '', NULL),
(1920, 36, 'Etui na okulary AST62089', 'etui-na-okulary-ast62089', 3.99, NULL, 0, 'product-image-58e266a1ece3f.jpeg', '2017-04-03 17:13:37', NULL, NULL, 0, 'Produkt39', 'Produkt39', '', NULL, 0, '', NULL),
(1921, 36, 'Etui na okulary - AST62083', 'etui-na-okulary-ast62083', 3.99, NULL, 0, 'product-image-58e266ae573f6.jpeg', '2017-04-03 17:13:50', NULL, NULL, 0, 'Produkt40', 'Produkt40', '', NULL, 0, '', NULL),
(1922, 36, 'Etui na okulary - AST62079', 'etui-na-okulary-ast62079', 3.99, NULL, 0, 'product-image-58e266b8f3470.jpeg', '2017-04-03 17:14:00', NULL, NULL, 0, 'Produkt41', 'Produkt41', '', NULL, 0, '', NULL),
(1923, 36, 'Etui na okulary AST62087', 'etui-na-okulary-ast62087', 3.99, NULL, 0, 'product-image-58e266c5b8f37.jpeg', '2017-04-03 17:14:13', NULL, NULL, 0, 'Produkt42', 'Produkt42', '', NULL, 0, '', NULL),
(1924, 36, 'Portfel męski', 'portfel-meski-3', 9.95, NULL, 0, 'product-image-58e26e00e71e9.jpeg', '2017-04-03 17:45:04', NULL, NULL, 0, 'Produkt43', 'Produkt43', '', NULL, 0, '', NULL),
(1925, 36, 'Portfel męski', 'portfel-meski-2', 9.95, NULL, 0, 'product-image-58e26e0ce19e7.jpeg', '2017-04-03 17:45:16', NULL, NULL, 0, 'Produkt44', 'Produkt44', '', NULL, 0, '', NULL),
(1926, 36, 'Portfel męski', 'portfel-meski-1', 9.95, NULL, 0, 'product-image-58e26e1915aa7.jpeg', '2017-04-03 17:45:28', NULL, NULL, 0, 'Produkt45', 'Produkt45', '', NULL, 0, '', NULL),
(1927, 36, 'Portfel męski - 01007', 'portfel-meski-01007', 9.95, NULL, 0, 'product-image-58e26e24652fc.jpeg', '2017-04-03 17:45:40', NULL, NULL, 0, 'Produkt46', 'Produkt46', '', NULL, 0, '', NULL),
(1928, 36, 'Portfel męski - 01PAL37', 'portfel-meski-01pal37', 9.95, NULL, 0, 'product-image-58e26e3112b34.jpeg', '2017-04-03 17:45:52', NULL, NULL, 0, 'Produkt47', 'Produkt47', '', NULL, 0, '', NULL),
(1929, 36, 'Portfel męski - 01PAL21', 'portfel-meski-01pal21', 9.95, NULL, 0, 'product-image-58e26e3b56570.jpeg', '2017-04-03 17:46:03', NULL, NULL, 0, 'Produkt48', 'Produkt48', '', NULL, 0, '', NULL),
(1930, 36, 'Portfel męski - 01550', 'portfel-meski-01550', 9.95, NULL, 0, 'product-image-58e26e46de4ba.jpeg', '2017-04-03 17:46:14', NULL, NULL, 0, 'Produkt49', 'Produkt49', '', NULL, 0, '', NULL),
(1931, 36, 'Portfel męski - M4', 'portfel-meski-m4', 9.95, NULL, 0, 'product-image-58e26e537935b.jpeg', '2017-04-03 17:46:27', NULL, NULL, 0, 'Produkt50', 'Produkt50', '', NULL, 0, '', NULL),
(1932, 36, 'Portfel damski - AS00404', 'portfel-damski-as00404', 29.95, NULL, 0, 'product-image-58e26e643f041.jpeg', '2017-04-03 17:46:43', NULL, NULL, 0, 'Produkt51', 'Produkt51', '', NULL, 0, '', NULL),
(1933, 36, 'Portfel damski - AS00506', 'portfel-damski-as00506', 23.96, NULL, 0, 'product-image-58e26e7203310.jpeg', '2017-04-03 17:46:57', NULL, NULL, 0, 'Produkt52', 'Produkt52', '', NULL, 0, '', NULL),
(1934, 36, 'Portfel damski - AS00508', 'portfel-damski-as00508', 17.97, NULL, 0, 'product-image-58e26e7f2dbf8.jpeg', '2017-04-03 17:47:10', NULL, NULL, 0, 'Produkt53', 'Produkt53', '', NULL, 0, '', NULL),
(1935, 36, 'Pasek męski', 'pasek-meski', 5.99, NULL, 0, 'product-image-58e26e8b99592.jpeg', '2017-04-03 17:47:22', NULL, NULL, 0, 'Produkt54', 'Produkt54', '', NULL, 0, '', NULL),
(1936, 39, 'Kubek dymny 250ml', 'kubek-dymny-250ml', 1.99, NULL, 0, 'product-image-58e28946b44d4.jpeg', '2017-04-03 19:41:25', NULL, NULL, 0, 'Produkt1', 'Produkt1', '', NULL, 0, '', NULL),
(1937, 39, 'Kubek - AS00465', 'kubek-as00465', 3.99, NULL, 0, 'product-image-58e28950e8f6e.jpeg', '2017-04-03 19:41:36', NULL, NULL, 0, 'Produkt2', 'Produkt2', '', NULL, 0, '', NULL),
(1938, 39, 'Kubek - AS00083', 'kubek-as00083', 3.99, NULL, 0, 'product-image-58e2895bc2ff5.jpeg', '2017-04-03 19:41:47', NULL, NULL, 0, 'Produkt3', 'Produkt3', '', NULL, 0, '', NULL);
INSERT INTO `product` (`id`, `category_id`, `name`, `slug`, `price`, `promotionPrice`, `isNew`, `photo`, `createdAt`, `updatedAt`, `quantity`, `seoGenerate`, `seoTitle`, `seoKeywords`, `seoDescription`, `content`, `contentShortGenerate`, `contentShort`, `newUntilDate`) VALUES
(1939, 39, 'Kubek róża - duży AS00011', 'kubek-roza-duzy-as00011', 2.99, NULL, 0, 'product-image-58e289674c1c3.jpeg', '2017-04-03 19:41:58', NULL, NULL, 0, 'Produkt4', 'Produkt4', '', NULL, 0, '', NULL),
(1940, 39, 'Kubek róża - mały AS00010', 'kubek-roza-maly-as00010', 1.99, NULL, 0, 'product-image-58e28973ef233.jpeg', '2017-04-03 19:42:11', NULL, NULL, 0, 'Produkt5', 'Produkt5', '', NULL, 0, '', NULL),
(1941, 39, 'Maselnica szklana - AI-8095', 'maselnica-szklana-ai-8095', 8.97, NULL, 0, 'product-image-58e2897f27967.jpeg', '2017-04-03 19:42:22', NULL, NULL, 0, 'Produkt6', 'Produkt6', '', NULL, 0, '', NULL),
(1942, 39, 'Salaterka Kayla 140mm- 1030636', 'salaterka-kayla-140mm-1030636', 3.99, NULL, 0, 'product-image-58e2899356fb5.jpeg', '2017-04-03 19:42:43', NULL, NULL, 0, 'Produkt7', 'Produkt7', '', NULL, 0, '', NULL),
(1943, 39, 'Szklanka - motyw z sową A2108T', 'szklanka-motyw-z-sowa-a2108t', 2.99, NULL, 0, 'product-image-58e2899e55f45.jpeg', '2017-04-03 19:42:53', NULL, NULL, 0, 'Produkt8', 'Produkt8', '', NULL, 0, '', NULL),
(1944, 39, 'Szklanka TC11P17', 'szklanka-tc11p17', 1.49, NULL, 0, 'product-image-58e289a89d56a.jpeg', '2017-04-03 19:43:04', NULL, NULL, 0, 'Produkt9', 'Produkt9', '', NULL, 0, '', NULL),
(1945, 39, 'Szklanka TC11CP17', 'szklanka-tc11cp17', 1.49, NULL, 0, 'product-image-58e289b2f2e85.jpeg', '2017-04-03 19:43:14', NULL, NULL, 0, 'Produkt10', 'Produkt10', '', NULL, 0, '', NULL),
(1946, 39, 'Szklanka gładka 250ml -6szt', 'szklanka-gladka-250ml-6szt', 5.99, NULL, 0, 'product-image-58e289da4c3ed.jpeg', '2017-04-03 19:43:53', NULL, NULL, 0, 'Produkt11', 'Produkt11', '', NULL, 0, '', NULL),
(1947, 39, 'Kpl.szklanek 180ml', 'kpl-szklanek-180ml', 5.99, NULL, 0, 'product-image-58e289e34abb5.jpeg', '2017-04-03 19:44:03', NULL, NULL, 0, 'Produkt12', 'Produkt12', '', NULL, 0, '', NULL),
(1948, 39, 'Kpl.3szt szklanek CHE006', 'kpl-3szt-szklanek-che006', 5.99, NULL, 0, 'product-image-58e289f04925b.jpeg', '2017-04-03 19:44:15', NULL, NULL, 0, 'Produkt13', 'Produkt13', '', NULL, 0, '', NULL),
(1949, 39, 'Kpl.3szt szklanek CHE015', 'kpl-3szt-szklanek-che015', 5.99, NULL, 0, 'product-image-58e289fc33f6a.jpeg', '2017-04-03 19:44:27', NULL, NULL, 0, 'Produkt14', 'Produkt14', '', NULL, 0, '', NULL),
(1950, 39, 'Kpl.literatek 150ml', 'kpl-literatek-150ml', 5.99, NULL, 0, 'product-image-58e28a06def59.jpeg', '2017-04-03 19:44:38', NULL, NULL, 0, 'Produkt15', 'Produkt15', '', NULL, 0, '', NULL),
(1951, 39, 'Kpl.szklanek RCR TRIX - niska', 'kpl-szklanek-rcr-trix-niska', 29.95, NULL, 0, 'product-image-58e28a11d95d1.jpeg', '2017-04-03 19:44:49', NULL, NULL, 0, 'Produkt16', 'Produkt16', '', NULL, 0, '', NULL),
(1952, 39, 'Kpl.szklanek RCR TRIX - wysoka', 'kpl-szklanek-rcr-trix-wysoka', 35.94, NULL, 0, 'product-image-58e28a1d489f6.jpeg', '2017-04-03 19:45:01', NULL, NULL, 0, 'Produkt17', 'Produkt17', '', NULL, 0, '', NULL),
(1953, 39, 'Kpl.kileiszków RCR Fusion', 'produkt18-1', 39.95, NULL, 0, 'product-image-58e296f919e88.jpeg', '2017-04-03 20:39:52', NULL, NULL, 0, 'Produkt18', 'Produkt18', '', NULL, 0, '', NULL),
(1954, 39, 'Kpl.kileiszów RCR Fusion', 'kpl-kileiszow-rcr-fusion', 39.95, NULL, 0, 'product-image-58e2970b5ab87.jpeg', '2017-04-03 20:40:11', NULL, NULL, 0, 'Produkt19', 'Produkt19', '', NULL, 0, '', NULL),
(1956, 39, 'Kpl. kieliszków do wódki 25ml - CHE007', 'kpl-kieliszkow-do-wodki-25ml-che007', 5.99, NULL, 0, 'product-image-58e2972265778.jpeg', '2017-04-03 20:40:34', NULL, NULL, 0, 'Produkt21', 'Produkt21', '', NULL, 0, '', NULL),
(1957, 39, 'Salaterka Twist 120mm - 1030692', 'salaterka-twist-120mm-1030692', 3.99, NULL, 0, 'product-image-58e2972d45f55.jpeg', '2017-04-03 20:40:44', NULL, NULL, 0, 'Produkt22', 'Produkt22', '', NULL, 0, '', NULL),
(1958, 39, 'Talerz szklany LOOP 1030701', 'talerz-szklany-loop-1030701', 3.99, NULL, 0, 'product-image-58e29737f2548.jpeg', '2017-04-03 20:40:55', NULL, NULL, 0, 'Produkt23', 'Produkt23', '', NULL, 0, '', NULL),
(1959, 39, 'Miska BC35M23', 'miska-bc35m23', 2.99, NULL, 0, 'product-image-58e297437184e.jpeg', '2017-04-03 20:41:07', NULL, NULL, 0, 'Produkt24', 'Produkt24', '', NULL, 0, '', NULL),
(1960, 39, 'Salaterka Tavola - 1055312', 'salaterka-tavola-1055312', 2.99, NULL, 0, 'product-image-58e2974e5e847.jpeg', '2017-04-03 20:41:17', NULL, NULL, 0, 'Produkt25', 'Produkt25', '', NULL, 0, '', NULL),
(1961, 39, 'Miska LAV TRU264', 'miska-lav-tru264', 2.99, NULL, 0, 'product-image-58e29758a4f82.jpeg', '2017-04-03 20:41:28', NULL, NULL, 0, 'Produkt26', 'Produkt26', '', NULL, 0, '', NULL),
(1962, 39, 'Miska szklana LKG-4136', 'miska-szklana-lkg-4136', 5.99, NULL, 0, 'product-image-58e29763732d7.jpeg', '2017-04-03 20:41:39', NULL, NULL, 0, 'Produkt27', 'Produkt27', '', NULL, 0, '', NULL),
(1963, 39, 'Bulionówka dymna', 'bulionowka-dymna', 2.99, NULL, 0, 'product-image-58e2976e34eb8.jpeg', '2017-04-03 20:41:49', NULL, NULL, 0, 'Produkt28', 'Produkt28', '', NULL, 0, '', NULL),
(1964, 39, 'Salaterka SYLVIA 180mm', 'salaterka-sylvia-180mm', 5.99, NULL, 0, 'product-image-58e297784c8ed.jpeg', '2017-04-03 20:41:59', NULL, NULL, 0, 'Produkt29', 'Produkt29', '', NULL, 0, '', NULL),
(1965, 39, 'Salaterka Saturn 160mm', 'salaterka-saturn-160mm', 5.99, NULL, 0, 'product-image-58e2978313490.jpeg', '2017-04-03 20:42:10', NULL, NULL, 0, 'Produkt30', 'Produkt30', '', NULL, 0, '', NULL),
(1966, 39, 'Salaterka Nadine 210mm 1001226', 'salaterka-nadine-210mm-1001226', 11.98, NULL, 0, 'product-image-58e29c5107a30.jpeg', '2017-04-03 21:02:40', NULL, NULL, 0, 'Produkt31', 'Produkt31', '', NULL, 0, '', NULL),
(1967, 39, 'Wyciskarka do cytrusów', 'wyciskarka-do-cytrusow', 5.99, NULL, 0, 'product-image-58e29c60496af.jpeg', '2017-04-03 21:02:55', NULL, NULL, 0, 'Produkt32', 'Produkt32', '', NULL, 0, '', NULL),
(1968, 39, 'Pojemnik szklany z pokrywką 270ml OZ6878', 'pojemnik-szklany-z-pokrywka-270ml-oz6878', 2.99, NULL, 0, 'product-image-58e29c6ae12c3.jpeg', '2017-04-03 21:03:06', NULL, NULL, 0, 'Produkt33', 'Produkt33', '', NULL, 0, '', NULL),
(1969, 39, 'Słoik/pojemnik AS0007', 'sloik-pojemnik-as0007', 6.99, NULL, 0, 'product-image-58e29c753fe85.jpeg', '2017-04-03 21:03:16', NULL, NULL, 0, 'Produkt34', 'Produkt34', '', NULL, 0, '', NULL),
(1970, 39, 'Słoik JR4', 'sloik-jr4', 11.98, NULL, 0, 'product-image-58e29c7fd9819.jpeg', '2017-04-03 21:03:27', NULL, NULL, 0, 'Produkt35', 'Produkt35', '', NULL, 0, '', NULL),
(1971, 39, 'Pojemnik na cytrynę', 'pojemnik-na-cytryne', 3.99, NULL, 0, 'product-image-58e29c8bbef05.jpeg', '2017-04-03 21:03:39', NULL, NULL, 0, 'Produkt36', 'Produkt36', '', NULL, 0, '', NULL),
(1972, 39, 'Pojemnik JS1175STM20', 'pojemnik-js1175stm20', 5.99, NULL, 0, 'product-image-58e29c959974a.jpeg', '2017-04-03 21:03:49', NULL, NULL, 0, 'Produkt37', 'Produkt37', '', NULL, 0, '', NULL),
(1973, 39, 'Dzbanek GSQ120M11', 'dzbanek-gsq120m11', 5.99, NULL, 0, 'product-image-58e29ca02b6a1.jpeg', '2017-04-03 21:03:59', NULL, NULL, 0, 'Produkt38', 'Produkt38', '', NULL, 0, '', NULL),
(1974, 39, 'Pojemnik', 'pojemnik-1', 5.99, NULL, 0, 'product-image-58e29caa9fe54.jpeg', '2017-04-03 21:04:10', NULL, NULL, 0, 'Produkt39', 'Produkt39', '', NULL, 0, '', NULL),
(1975, 39, 'Karafka szklana 850ml OZ1306', 'karafka-szklana-850ml-oz1306', 5.99, NULL, 0, 'product-image-58e29cb68cb99.jpeg', '2017-04-03 21:04:22', NULL, NULL, 0, 'Produkt40', 'Produkt40', '', NULL, 0, '', NULL),
(1976, 39, 'Pojemnik JR1', 'pojemnik-jr1', 5.99, NULL, 0, 'product-image-58e2a0334e078.jpeg', '2017-04-03 21:19:15', NULL, NULL, 0, 'Produkt41', 'Produkt41', '', NULL, 0, '', NULL),
(1977, 39, 'Pojemnik JS1175STM11', 'pojemnik-js1175stm11', 5.99, NULL, 0, 'product-image-58e2a041d5130.jpeg', '2017-04-03 21:19:29', NULL, NULL, 0, 'Produkt42', 'Produkt42', '', NULL, 0, '', NULL),
(1978, 39, 'Wazon Nadine 200mm - 1001280', 'wazon-nadine-200mm-1001280', 11.98, NULL, 0, 'product-image-58e2a04c41093.jpeg', '2017-04-03 21:19:39', NULL, NULL, 0, 'Produkt43', 'Produkt43', '', NULL, 0, '', NULL),
(1979, 39, 'Pojemnik', 'pojemnik', 5.99, NULL, 0, 'product-image-58e2a056799f7.jpeg', '2017-04-03 21:19:50', NULL, NULL, 0, 'Produkt44', 'Produkt44', '', NULL, 0, '', NULL),
(1980, 39, 'Pojemnik JR500', 'pojemnik-jr500', 3.99, NULL, 0, 'product-image-58e2a06203770.jpeg', '2017-04-03 21:20:01', NULL, NULL, 0, 'Produkt45', 'Produkt45', '', NULL, 0, '', NULL),
(1981, 39, 'Kieliszek koktajolwy', 'kieliszek-koktajolwy', 4.99, NULL, 0, 'product-image-58e2a06b76dcf.jpeg', '2017-04-03 21:20:11', NULL, NULL, 0, 'Produkt46', 'Produkt46', '', NULL, 0, '', NULL),
(1982, 39, 'Kubek ze słomką AS00148', 'kubek-ze-slomka-as00148', 1.95, 3.99, 0, 'product-image-58e2a074f0d0a.jpeg', '2017-04-03 21:20:20', NULL, NULL, 0, 'Produkt47', 'Produkt47', '', NULL, 0, '', NULL),
(1984, 37, 'Skarbonka buźka AS0106', 'skarbonka-buzka-as0106-1', 5.99, NULL, 0, 'product-image-5901d3e9b2fb8.jpeg', '2017-04-27 13:20:09', NULL, NULL, 0, 'Skarbonka buźka AS0106', 'Skarbonka, buźka, AS0106', '', NULL, 0, '', NULL),
(1985, 37, 'Skarbonka sowa AS01062', 'skarbonka-sowa-as01062', 5.99, NULL, 0, 'product-image-5901dd53c2d7c.jpeg', '2017-04-27 13:28:33', NULL, NULL, 0, 'Skarbonka sowa AS01062', 'Skarbonka, sowa, AS01062', '', NULL, 0, '', NULL),
(1986, 34, 'Zestaw łazienkowy AS01350', 'zestaw-lazienkowy-as01350', 29.95, NULL, 0, 'product-image-5901de542736d.jpeg', '2017-04-27 14:04:35', NULL, NULL, 0, 'Zestaw łazienkowy AS01350', 'Zestaw, łazienkowy, AS01350', '', NULL, 0, '', NULL),
(1987, 34, 'Zestaw łazienkowy AS01349', 'zestaw-lazienkowy-as01349', 29.95, NULL, 0, 'product-image-5901df345b337.jpeg', '2017-04-27 14:08:18', NULL, NULL, 0, 'Zestaw łazienkowy AS01349', 'Zestaw, łazienkowy, AS01349', '', NULL, 0, '', NULL),
(1988, 38, 'Miska ceramiczna AS01347', 'miska-ceramiczna-as01347', 2.99, NULL, 0, 'product-image-5901dffdccc0e.jpeg', '2017-04-27 14:11:41', NULL, NULL, 0, 'Miska ceramiczna AS01347', 'Miska, ceramiczna, AS01347', '', NULL, 0, '', NULL),
(1989, 38, 'Miska AS01342', 'miska-as01342', 2.99, NULL, 0, 'product-image-5901e0b788226.jpeg', '2017-04-27 14:13:15', NULL, NULL, 0, 'Miska AS01342', 'Miska, AS01342', '', NULL, 0, '', NULL),
(1990, 37, 'Skarbonka pies AS01064', 'skarbonka-pies-as01064', 5.99, NULL, 0, 'product-image-5901e17c35f3d.jpeg', '2017-04-27 14:18:03', NULL, NULL, 0, 'Skarbonka pies AS01064', 'Skarbonka, pies, AS01064', '', NULL, 0, '', NULL),
(1991, 37, 'Skarbonka piłka AS01069', 'skarbonka-pilka-as01069', 5.99, NULL, 0, 'product-image-5901e1af6a4ec.jpeg', '2017-04-27 14:18:55', NULL, NULL, 0, 'Skarbonka piłka AS01069', 'Skarbonka, piłka, AS01069', '', NULL, 0, '', NULL),
(1992, 34, 'Ścierka 63x36cm AS01330', 'scierka-63x36cm-as01330', 2.99, NULL, 0, 'product-image-5901e27bd0892.jpeg', '2017-04-27 14:22:19', NULL, NULL, 0, 'Ścierka 63x36cm AS01330', 'Ścierka, 63x36cm, AS01330', '', NULL, 0, '', NULL),
(1993, 37, 'Doniczki kp.3szt AS01704', 'doniczki-kp-3szt-as01704', 49.90, NULL, 0, 'product-image-5901e3044e2a6.jpeg', '2017-04-27 14:24:35', NULL, NULL, 0, 'Doniczki kp.3szt AS01704', 'Doniczki, kp.3szt, AS01704', '', NULL, 0, '', NULL),
(1994, 37, 'Doniczki kpl. 3szt.', 'doniczki-kpl-3szt', 49.90, NULL, 0, 'product-image-5901e392bd994.jpeg', '2017-04-27 14:26:58', NULL, NULL, 0, 'Doniczki kpl. 3szt.', 'Doniczki, kpl., 3szt.', '', NULL, 0, '', NULL),
(1995, 37, 'Obraz AS01781', 'obraz-as01781', 5.99, NULL, 0, 'product-image-5901e4138445b.jpeg', '2017-04-27 14:29:07', NULL, NULL, 0, 'Obraz AS01781', 'Obraz, AS01781', '', NULL, 0, '', NULL),
(1996, 37, 'Pudełko AS01043', 'pudelko-as01043', 5.99, NULL, 0, 'product-image-5901e471d33d6.jpeg', '2017-04-27 14:30:41', NULL, NULL, 0, 'Pudełko', 'Pudełko', '', NULL, 0, '', NULL),
(1997, 34, 'Kosz FLEX 011686', 'kosz-flex-011686', 11.98, NULL, 0, 'product-image-5901e5b6d24ce.jpeg', '2017-04-27 14:36:06', NULL, NULL, 0, 'Kosz FLEX 011686', 'Kosz, FLEX, 011686', '', NULL, 0, '', NULL),
(1998, 36, 'Rajstopy damskie 15DEN', 'rajstopy-damskie-15den', 5.99, NULL, 0, 'product-image-5901e6955d7ba.jpeg', '2017-04-27 14:39:48', NULL, NULL, 0, 'Rajstopy damskie 15DEN', 'Rajstopy, damskie, 15DEN', '', NULL, 0, '', NULL),
(1999, 36, 'Rajstopy dziewczęce TOSIA 40DEN', 'rajstopy-dziewczece-tosia-40den', 3.99, NULL, 0, 'product-image-5901e7f84e21e.jpeg', '2017-04-27 14:45:43', NULL, NULL, 0, 'Rajstopy dziewczęce TOSIA 40DEN', 'Rajstopy, dziewczęce, TOSIA, 40DEN', '', NULL, 0, '', NULL),
(2000, 36, 'Rajstopy dziewczęce gładkie', 'rajstopy-dziewczece-gladkie', 3.99, NULL, 0, 'product-image-5901e87772f7e.jpeg', '2017-04-27 14:47:51', NULL, NULL, 0, 'Rajstopy dziewczęce gładkie', 'Rajstopy, dziewczęce, gładkie', '', NULL, 0, '', NULL),
(2001, 38, 'Wyciskarka do cytrusów AI-2223', 'wyciskarka-do-cytrusow-ai-2223', 5.99, NULL, 0, 'product-image-5901e94f661f6.jpeg', '2017-04-27 14:51:27', NULL, NULL, 0, 'Wyciskacz do cytrusów AI-2223', 'Wyciskacz, cytrusów, AI-2223', '', NULL, 0, '', NULL),
(2002, 38, 'Talerz obiadowy - limirnac E9654', 'talerz-obiadowy-limirnac-e9654', 9.95, NULL, 0, 'product-image-5901ea1b342f5.jpeg', '2017-04-27 14:54:50', NULL, NULL, 0, 'Talerz obiadowy - limirnac E9654', 'Talerz, obiadowy, limirnac, E9654', '', NULL, 0, '', NULL),
(2003, 37, 'Obrus plamoodporny 110x115  MTC 1115', 'obrus-plamoodporny-110x115-mtc-1115', 9.95, NULL, 0, 'product-image-5901eaa90ffa0.jpeg', '2017-04-27 14:57:12', NULL, NULL, 0, 'Obrus plamoodporny 110x115  MTC 1115', 'Obrus, plamoodporny, 110x115, 1115', '', NULL, 0, '', NULL),
(2004, 37, 'Obrus plamoodporny 150x200 MTC1520', 'obrus-plamoodporny-150x200-mtc1520', 17.97, NULL, 0, 'product-image-5901eb275534c.jpeg', '2017-04-27 14:59:18', NULL, NULL, 0, 'Obrus plamoodporny 150x200 MTC1520', 'Obrus, plamoodporny, 150x200, MTC1520', '', NULL, 0, '', NULL),
(2005, 37, 'Nawilżacz ceramiczny do kwiatów HGP-04', 'nawilzacz-ceramiczny-do-kwiatow-hgp-04', 5.99, NULL, 0, 'product-image-5901ec69cf623.jpeg', '2017-04-27 15:04:41', NULL, NULL, 0, 'Nawilżacz ceramiczny do kwiatów HGP-04', 'Nawilżacz, ceramiczny, kwiatów, HGP-04', '', NULL, 0, '', NULL),
(2006, 32, 'Maskotka Delfin AS01196', 'maskotka-delfin-as01196', 14.95, NULL, 0, 'product-image-5901ed23df47f.jpeg', '2017-04-27 15:07:47', NULL, NULL, 0, 'Maskotka Delfin AS01196', 'Maskotka, Delfin, AS01196', '', NULL, 0, '', NULL),
(2007, 43, 'Wąż ogrodowy 15m AS01356', 'waz-ogrodowy-15m-as01356', 19.95, NULL, 1, 'product-image-5901ee6a56c31.jpeg', '2017-04-27 15:13:13', NULL, NULL, 0, 'Wąż ogrodowy 15m AS01356', 'Wąż, ogrodowy, AS01356', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2008, 34, 'Spryskiwacz AS01357', 'spryskiwacz-as01357', 2.99, NULL, 0, 'product-image-5901ef043b4ba.jpeg', '2017-04-27 15:15:47', NULL, NULL, 0, 'Spryskiwacz AS01357', 'Spryskiwacz, AS01357', '', NULL, 0, '', NULL),
(2009, 32, 'Kwiatek pluszowy AS01193', 'kwiatek-pluszowy-as01193', 2.99, NULL, 1, 'product-image-5901f015220d4.jpeg', '2017-04-27 15:20:20', NULL, NULL, 0, 'Kwiatek pluszowy AS01193', 'Kwiatek, pluszowy, AS01193', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2010, 32, 'Wachlarz AS01377', 'wachlarz-as01377', 2.99, NULL, 1, 'product-image-5901f054d0aa6.jpeg', '2017-04-27 15:21:24', NULL, NULL, 0, 'Wachlarz AS01377', 'Wachlarz, AS01377', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2011, 37, 'Kuferek AS01127', 'kuferek-as01127', 5.99, NULL, 1, 'product-image-5901f0ac321cb.jpeg', '2017-04-27 15:22:14', NULL, NULL, 0, 'Kuferek AS01127', 'Kuferek, AS01127', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2012, 37, 'Torebka prezentowa 26x12x32cm AS0109', 'torebka-prezentowa-26x12x32cm-as0109', 3.99, NULL, 0, 'product-image-5901f1003dce0.jpeg', '2017-04-27 15:24:15', NULL, NULL, 0, 'Torebka prezentowa 26x12x32cm AS0109', 'Torebka, prezentowa, 26x12x32cm, AS0109', '', NULL, 0, '', NULL),
(2013, 37, 'Torebka prezentowa 30x12x40cm AS01091', 'torebka-prezentowa-30x12x40cm-as01091', 3.99, NULL, 0, 'product-image-5901f16477776.jpeg', '2017-04-27 15:25:56', NULL, NULL, 0, 'Torebka prezentowa 30x12x40cm AS01091', 'Torebka, prezentowa, 30x12x40cm, AS01091', '', NULL, 0, '', NULL),
(2014, 37, 'Torebka prezentowa 18x10x23cm AS01093', 'torebka-prezentowa-18x10x23cm-as01093', 2.99, NULL, 0, 'product-image-5901f1b75b6ad.jpeg', '2017-04-27 15:27:18', NULL, NULL, 0, 'Torebka prezentowa 18x10x23cm AS01093', 'Torebka, prezentowa, 18x10x23cm, AS01093', '', NULL, 0, '', NULL),
(2015, 37, 'Torebka prezentowa  14x7x15cm AS01080', 'torebka-prezentowa-14x7x15cm-as01080', 2.99, NULL, 0, 'product-image-5901f2134f965.jpeg', '2017-04-27 15:28:50', NULL, NULL, 0, 'Torebka prezentowa  14x7x15cm AS01080', 'Torebka, prezentowa, 14x7x15cm, AS01080', '', NULL, 0, '', NULL),
(2016, 37, 'Torebka prezentowa 18x10x23cm AS01081', 'torebka-prezentowa-18x10x23cm-as01081', 2.99, NULL, 0, 'product-image-5901f2f188178.jpeg', '2017-04-27 15:32:33', NULL, NULL, 0, 'Torebka prezentowa 18x10x23cm AS01081', 'Torebka, prezentowa, 18x10x23cm, AS01081', '', NULL, 0, '', NULL),
(2017, 37, 'Ramka na zdjęcia AS01050', 'ramka-na-zdjecia-as01050', 14.95, NULL, 1, 'product-image-5901f3608af74.jpeg', '2017-04-27 15:33:23', NULL, NULL, 0, 'Ramka na zdjęcia AS01050', 'Ramka, zdjęcia, AS01050', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2018, 37, 'Ramka na zdjęcia HOME AS01056', 'ramka-na-zdjecia-home-as01056', 23.96, NULL, 1, 'product-image-5901f3b66b299.jpeg', '2017-04-27 15:35:50', NULL, NULL, 0, 'Ramka na zdjęcia HOME AS01056', 'Ramka, zdjęcia, HOME, AS01056', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2019, 37, 'Ramka na zdjęcia AS01049', 'ramka-na-zdjecia-as01049', 19.95, NULL, 1, 'product-image-5901f3f9c711f.jpeg', '2017-04-27 15:36:57', NULL, NULL, 0, 'Ramka na zdjęcia AS01049', 'Ramka, zdjęcia, AS01049', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2020, 37, 'Ramka na zdjęcia AS01052', 'ramka-na-zdjecia-as01052', 19.95, NULL, 0, 'product-image-5901f48cd06e6.jpeg', '2017-04-27 15:38:51', NULL, NULL, 0, 'Ramka na zdjęcia AS01052', 'Ramka, zdjęcia, AS01052', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2021, 36, 'Portfel damski AS00509', 'portfel-damski-as00509', 23.96, NULL, 0, 'product-image-5901f504c5867.jpeg', '2017-04-27 15:41:24', NULL, NULL, 0, 'Portfel damski AS00509', 'Portfel, damski, AS00509', '', NULL, 0, '', NULL),
(2022, 37, 'Osłonka plastik - DC-11342', 'oslonka-plastik-dc-11342', 8.97, NULL, 0, 'product-image-5901f5c862849.jpeg', '2017-04-27 15:44:39', NULL, NULL, 0, 'Osłonka plastik - DC-11342', 'Osłonka, plastik, DC-11342', '', NULL, 0, '', NULL),
(2023, 32, 'Zabawka żółwik DC-1307', 'zabawka-zolwik-dc-1307', 5.99, NULL, 1, 'product-image-5901f66b46e46.jpeg', '2017-04-27 15:47:22', NULL, NULL, 0, 'Zabawka żółwik DC-1307', 'Zabawka, żółwik, DC-1307', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2024, 32, 'Dźwig DC-1182', 'dzwig-dc-1182', 3.99, NULL, 1, 'product-image-5901f6f3354e9.jpeg', '2017-04-27 15:49:38', NULL, NULL, 0, 'Dźwig DC-1182', 'Dźwig, DC-1182', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2025, 32, 'Traktor DC-1324', 'traktor-dc-1324', 5.99, NULL, 1, 'product-image-5901f75c4d42b.jpeg', '2017-04-27 15:51:23', NULL, NULL, 0, 'Traktor DC-1324', 'Traktor, DC-1324', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2026, 32, 'Cymbałki DC-1177', 'cymbalki-dc-1177', 3.99, NULL, 0, 'product-image-5902dbae7584e.jpeg', '2017-04-28 08:05:33', NULL, NULL, 0, 'Cymbałki DC-1177', 'Cymbałki, DC-1177', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2027, 37, 'Latarka brelok DC-60462', 'latarka-brelok-dc-60462', 1.00, NULL, 0, 'product-image-590ae08f3872a.jpeg', '2017-04-28 08:07:34', NULL, NULL, 0, 'Latarka DC-60462', 'Latarka, DC-60462', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2028, 31, 'Zestaw biurowy DC-461', 'zestaw-biurowy-dc-461', 1.99, NULL, 0, 'product-image-5902dc6e728cd.jpeg', '2017-04-28 08:08:45', NULL, NULL, 0, 'Zestaw biurowy DC-461', 'Zestaw, biurowy, DC-461', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2029, 38, 'Wykałaczki DC-461', 'wykalaczki-dc-461', 0.50, NULL, 1, 'product-image-5902dd0c2f4e0.jpeg', '2017-04-28 08:11:23', NULL, NULL, 0, 'Wykałaczki DC-461', 'Wykałaczki, DC-461', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2030, 31, 'Długopis 2szt. DC-583', 'dlugopis-2szt-dc-583', 1.00, NULL, 1, 'product-image-5902ddd964293.jpeg', '2017-04-28 08:13:40', NULL, NULL, 0, 'Długopis 2szt. DC-583', 'Długopis, 2szt., DC-583', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2031, 37, 'Latarnia LED NFB-02', 'latarnia-led-nfb-02', 9.99, NULL, 0, 'product-image-5902de172d2c8.jpeg', '2017-04-28 08:15:50', NULL, NULL, 0, 'Latarnia LED NFB-02', 'Latarnia, NFB-02', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2032, 34, 'Ręcznik mały 50x100', 'recznik-maly-50x100', 5.99, NULL, 1, 'product-image-5902debad9935.jpeg', '2017-04-28 08:18:34', NULL, NULL, 0, 'Ręcznik mały 50x100', 'Ręcznik, mały, 50x100', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2033, 34, 'Ręcznik mały 50x100', 'recznik-maly-50x100-1', 6.99, NULL, 1, 'product-image-5902dfafa5b64.jpeg', '2017-04-28 08:21:34', NULL, NULL, 0, 'Ręcznik mały 50x100', 'Ręcznik, mały, 50x100', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2034, 39, 'Wazon szklany 9230435', 'wazon-szklany-9230435', 11.98, NULL, 1, 'product-image-5902e039cc1af.jpeg', '2017-04-28 08:24:57', NULL, NULL, 0, 'Wazon szklany 2599', 'Wazon, szklany, 2599', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2035, 39, 'Świecznik Koniak H-30 2599', 'swiecznik-koniak-h-30-2599', 17.97, NULL, 1, 'product-image-5902e15bc7706.jpeg', '2017-04-28 08:29:47', NULL, NULL, 0, 'Świecznik Koniak 2599', 'Świecznik, Koniak, 2599', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2036, 39, 'Świecznik Cylinderek H-40 2603', 'swiecznik-cylinderek-h-40-2603', 23.96, NULL, 1, 'product-image-5902e1ea0196c.jpeg', '2017-04-28 08:32:08', NULL, NULL, 0, 'Świecznik Cylinderek 2603', 'Świecznik, Cylinderek, 2603', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2037, 39, 'Świecznik Koniak H-34 2589', 'swiecznik-koniak-h-34-2589', 19.95, NULL, 1, 'product-image-5902e2f76c1c5.jpeg', '2017-04-28 08:36:38', NULL, NULL, 0, 'Świecznik Koniak H-34 2589', 'Świecznik, Koniak, H-34, 2589', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2038, 39, 'Świecznik Cylinderek H-35 2602', 'swiecznik-cylinderek-h-35-2602', 19.95, NULL, 1, 'product-image-5902e42ddbce8.jpeg', '2017-04-28 08:41:49', NULL, NULL, 0, 'Świecznik Cylinderek H-35 2602', 'Świecznik, Cylinderek, H-35, 2602', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2039, 39, 'Wazon LEO 2255', 'wazon-leo-2255', 8.97, NULL, 1, 'product-image-5902e4d377c0f.jpeg', '2017-04-28 08:44:34', NULL, NULL, 0, 'Wazon LEO 2255', 'Wazon, 2255', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2040, 39, 'Świecznik Koniak H-40 2600', 'swiecznik-koniak-h-40-2600', 23.96, NULL, 1, 'product-image-5902e58965e04.jpeg', '2017-04-28 08:47:36', NULL, NULL, 0, 'Świecznik Cylinderek H-40 2600', 'Świecznik, Cylinderek, H-40, 2600', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2041, 39, 'Świecznik Cylinderek H-30 2601', 'swiecznik-cylinderek-h-30-2601', 17.97, NULL, 1, 'product-image-5902e815987a3.jpeg', '2017-04-28 08:58:28', NULL, NULL, 0, 'Świecznik Cylinderek H-30 2601', 'Świecznik, Cylinderek, H-30, 2601', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2042, 39, 'Salaterka TWIST 120mm 809378', 'salaterka-twist-120mm-809378', 3.99, NULL, 1, 'product-image-5902e8c61a02f.jpeg', '2017-04-28 09:01:24', NULL, NULL, 0, 'Salaterka TWIST 120mm 809378', 'Salaterka, TWIST, 120mm, 809378', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2043, 39, 'Wazon Kość H-25 0536', 'wazon-kosc-h-25-0536', 3.99, NULL, 1, 'product-image-5902e9f3bacdc.jpeg', '2017-04-28 09:06:27', NULL, NULL, 0, 'Wazon Kość H-25 0536', 'Wazon, Kość, H-25, 0536', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2044, 39, 'Kileiszek Madison 810497', 'kileiszek-madison-810497', 2.99, NULL, 1, 'product-image-5902ea59c5187.jpeg', '2017-04-28 09:08:09', NULL, NULL, 0, 'Kileiszek Madison 810497', 'Kileiszek, Madison, 810497', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2045, 39, 'Pojemnik szklany 0870', 'pojemnik-szklany-0870', 5.99, NULL, 1, 'product-image-5902ead73a4b0.jpeg', '2017-04-28 09:10:14', NULL, NULL, 0, 'Pojemnik szklany 0870', 'Pojemnik, szklany, 0870', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2046, 39, 'Szklanka z uchem 280ml', 'szklanka-z-uchem-280ml', 2.99, NULL, 1, 'product-image-5902eb9894327.jpeg', '2017-04-28 09:13:27', NULL, NULL, 0, 'Szklanka z uchem 280ml', 'Szklanka, uchem, 280ml', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2047, 38, 'Zestaw  tortownica + wyciskacz do cytrusów 023891', 'zestaw-tortownica-wyciskacz-do-cytrusow-023891', 17.97, NULL, 1, 'product-image-5902ecb257dd0.jpeg', '2017-04-28 09:18:09', NULL, NULL, 0, 'Zestaw  tortownica + wyciskacz do cytrusów 023891', 'Zestaw, tortownica, wyciskacz, cytrusów, 023891', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2048, 37, 'Zegar na ścianę M1608272', 'zegar-na-sciane-m1608272', 19.99, NULL, 1, 'product-image-5902ed631dee2.jpeg', '2017-04-28 09:21:06', NULL, NULL, 0, 'Zegar na ścianę M1608272', 'Zegar, ścianę, M1608272', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2049, 38, 'Pojemnik szklany 700ml ZSC-6579', 'pojemnik-szklany-700ml-zsc-6579', 5.99, NULL, 1, 'product-image-5902ee1bdb189.jpeg', '2017-04-28 09:24:11', NULL, NULL, 0, 'Pojemnik szklany 700ml ZSC-6579', 'Pojemnik, szklany, 700ml, ZSC-6579', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2050, 37, 'Doniczka ceramiczna LC006-614', 'doniczka-ceramiczna-lc006-614', 5.99, NULL, 1, 'product-image-5902ee8d12af9.jpeg', '2017-04-28 09:26:04', NULL, NULL, 0, 'Doniczka ceramiczna LC006-614', 'Doniczka, ceramiczna, LC006-614', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2051, 37, 'Osłonka ceramiczna SF1757', 'oslonka-ceramiczna-sf1757', 5.99, NULL, 1, 'product-image-590acd59f25f4.jpeg', '2017-05-04 08:42:33', NULL, NULL, 0, 'Osłonka ceramiczna SF1757', 'Osłonka, ceramiczna, SF1757', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2052, 37, 'Osłonka dekoracyjna YT1603-18', 'oslonka-dekoracyjna-yt1603-18', 5.99, NULL, 1, 'product-image-590ace3ad074f.jpeg', '2017-05-04 08:44:46', NULL, NULL, 0, 'Osłonka dekoracyjna YT1603-18', 'Osłonka, dekoracyjna, YT1603-18', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2053, 34, 'Miska dla zwierząt 25cm B1606178', 'miska-dla-zwierzat-25cm-b1606178', 5.99, NULL, 1, 'product-image-590acecc846bf.jpeg', '2017-05-04 08:48:43', NULL, NULL, 0, 'Miska dla zwierząt 25cm B1606178', 'Miska, zwierząt, 25cm, B1606178', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2054, 37, 'Osłonka ceramiczna SF1758', 'oslonka-ceramiczna-sf1758', 5.99, NULL, 1, 'product-image-590acf5f33939.jpeg', '2017-05-04 08:51:09', NULL, NULL, 0, 'Osłonka ceramiczna SF1758', 'Osłonka, ceramiczna, SF1758', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2055, 38, 'Nóż do warzyw Denis 8505', 'noz-do-warzyw-denis-8505', 2.99, NULL, 1, 'product-image-590ad09c43617.jpeg', '2017-05-04 08:56:27', NULL, NULL, 0, 'Nóż do warzyw Denis 8505', 'Nóż, warzyw, Denis, 8505', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2056, 34, 'Miska dla zwierząt 20cm B1606177', 'miska-dla-zwierzat-20cm-b1606177', 5.99, NULL, 1, 'product-image-590ad104be56f.jpeg', '2017-05-04 08:58:11', NULL, NULL, 0, 'Miska dla zwierząt 20cm B1606177', 'Miska, zwierząt, 20cm, B1606177', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2057, 38, 'Skrobak do warzyw Denis Lux 1563', 'skrobak-do-warzyw-denis-lux-1563', 2.99, NULL, 1, 'product-image-590ad2bc82b34.jpeg', '2017-05-04 09:05:31', NULL, NULL, 0, 'Skrobak do warzyw Denis Lux 1563', 'Skrobak, warzyw, Denis, 1563', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2058, 38, 'Pojemnik szklany z pokrywką 330ml OZ3904', 'pojemnik-szklany-z-pokrywka-330ml-oz3904', 3.99, NULL, 1, 'product-image-590ad30727ef2.jpeg', '2017-05-04 09:06:45', NULL, NULL, 0, 'Pojemnik szklany z pokrywką 330ml OZ3904', 'Pojemnik, szklany, pokrywką, 330ml, OZ3904', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2059, 37, 'Świca zapachowa mix - HA402', 'swica-zapachowa-mix-ha402', 3.99, NULL, 1, 'product-image-590ad3b0dd3b3.jpeg', '2017-05-04 09:09:35', NULL, NULL, 0, 'Świca zapachowa mix - HA402', 'Świca, zapachowa, HA402', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2060, 37, 'Świeca zapachowa mix- HA-C120P5-18', 'swieca-zapachowa-mix-ha-c120p5-18', 5.99, NULL, 1, 'product-image-590ad435e2b7c.jpeg', '2017-05-04 09:11:48', NULL, NULL, 0, 'Świeca zapachowa mix- HA-C120P5-18', 'Świeca, zapachowa, mix-, HA-C120P5-18', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2061, 39, 'Szklanka LATTE 240ml QS210', 'szklanka-latte-240ml-qs210', 1.99, 2.99, 0, 'product-image-590ad5361ea3d.jpeg', '2017-05-04 09:16:05', NULL, NULL, 0, 'Szklanka LATTE 240ml QS210', 'Szklanka, LATTE, 240ml, QS210', '', NULL, 0, '', NULL),
(2062, 37, 'Olejek zapachowy z patyczkami HA-RD50PB-32', 'olejek-zapachowy-z-patyczkami-ha-rd50pb-32', 5.99, NULL, 1, 'product-image-590ad5b114ab5.jpeg', '2017-05-04 09:18:07', NULL, NULL, 0, 'Olejek zapachowy z patyczkami HA-RD50PB-32', 'Olejek, zapachowy, patyczkami, HA-RD50PB-32', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2063, 39, 'Komplet kieliszków 25ml - CHE007', 'komplet-kieliszkow-25ml-che007', 5.99, NULL, 1, 'product-image-590ad6a4600bd.jpeg', '2017-05-04 09:22:11', NULL, NULL, 0, 'Komplet kieliszków 25ml - CHE007', 'Komplet, kieliszków, 25ml, CHE007', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2064, 35, 'Kwiat sztuczny czosnek DPF1018', 'kwiat-sztuczny-czosnek-dpf1018', 5.99, NULL, 1, 'product-image-590ad748c3ab4.jpeg', '2017-05-04 09:23:36', NULL, NULL, 0, 'Kwiat sztuczny czosnek DPF1018', 'Kwiat, sztuczny, czosnek, DPF1018', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2065, 39, 'Miska szklana śr.17cm LKG-4129', 'miska-szklana-sr-17cm-lkg-4129', 3.99, NULL, 1, 'product-image-590ad7eb797b4.jpeg', '2017-05-04 09:27:38', NULL, NULL, 0, 'Miska szklana śr.17cm LKG-4129', 'Miska, szklana, śr.17cm, LKG-4129', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2066, 39, 'Pojemnik szklany 230ml ZBT-122', 'pojemnik-szklany-230ml-zbt-122', 3.99, NULL, 1, 'product-image-590ad857893c2.jpeg', '2017-05-04 09:29:26', NULL, NULL, 0, 'Pojemnik szklany 230ml ZBT-122', 'Pojemnik, szklany, 230ml, ZBT-122', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2067, 34, 'Wkład do mopa z mikrofibry 10x41cm WDGS-01', 'wklad-do-mopa-z-mikrofibry-10x41cm-wdgs-01', 5.99, NULL, 0, 'product-image-590ad9598c51b.jpeg', '2017-05-04 09:32:12', NULL, NULL, 0, 'Wkład do mopa z mikrofibry 10x41cm WDGS-01', 'Wkład, mopa, mikrofibry, 10x41cm, WDGS-01', '', NULL, 0, '', NULL),
(2068, 34, 'Wkład do mopa z mikrofibry WDGS-07', 'wklad-do-mopa-z-mikrofibry-wdgs-07', 3.99, NULL, 0, 'product-image-590ad9d9e3c60.jpeg', '2017-05-04 09:35:53', NULL, NULL, 0, 'Wkład do mopa z mikrofibry WDGS-07', 'Wkład, mopa, mikrofibry, WDGS-07', '', NULL, 0, '', NULL),
(2069, 38, 'Wykałaczki bambusowe kpl.4szt. MHF7435', 'wykalaczki-bambusowe-kpl-4szt-mhf7435', 2.99, NULL, 1, 'product-image-590ada48a0aa1.jpeg', '2017-05-04 09:37:43', NULL, NULL, 0, 'Wykałaczki bambusowe kpl.4szt. MHF7435', 'Wykałaczki, bambusowe, kpl.4szt., MHF7435', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2070, 31, 'Kreda w wiaderku 20szt. PF013', 'kreda-w-wiaderku-20szt-pf013', 5.99, NULL, 1, 'product-image-590adab0badae.jpeg', '2017-05-04 09:39:27', NULL, NULL, 0, 'Kreda w wiaderku 20szt. PF013', 'Kreda, wiaderku, 20szt., PF013', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2071, 34, 'Zestaw łazienkowy 18380M', 'zestaw-lazienkowy-18380m', 11.98, NULL, 1, 'product-image-590adb567f776.jpeg', '2017-05-04 09:42:13', NULL, NULL, 0, 'Zestaw łazienkowy 18380M', 'Zestaw, łazienkowy, 18380M', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2072, 39, 'Kpl.kieliszków do piwa MIS571', 'kpl-kieliszkow-do-piwa-mis571', 29.95, NULL, 1, 'product-image-590adca4a4972.jpeg', '2017-05-04 09:47:47', NULL, NULL, 0, 'Kpl.kieliszków do piwa MIS571', 'Kpl.kieliszków, piwa, MIS571', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2073, 39, 'Kpl.kieliszków EMP573', 'kpl-kieliszkow-emp573', 35.94, NULL, 1, 'product-image-590add0bbeffb.jpeg', '2017-05-04 09:49:30', NULL, NULL, 0, 'Kpl.kieliszków EMP573', 'Kpl.kieliszków, EMP573', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2074, 39, 'Salaterka CARMEN 270ml 1030719', 'salaterka-carmen-270ml-1030719', 19.95, NULL, 1, 'product-image-590add7e998bc.jpeg', '2017-05-04 09:51:25', NULL, NULL, 0, 'Salaterka CARMEN 270ml 1030719', 'Salaterka, CARMEN, 270ml, 1030719', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2075, 38, 'Kpl.obiadowy 12el. L4121', 'kpl-obiadowy-12el-l4121', 41.88, NULL, 1, 'product-image-590addee3f5fd.jpeg', '2017-05-04 09:53:16', NULL, NULL, 0, 'Kpl.obiadowy 12el. L4121', 'Kpl.obiadowy, 12el., L4121', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2076, 37, 'Obrazek 30x30 FB-0218', 'obrazek-30x30-fb-0218', 9.95, NULL, 1, 'product-image-590adead6069c.jpeg', '2017-05-04 09:56:28', NULL, NULL, 0, 'Obrazek 30x30 FB-0218', 'Obrazek, 30x30, FB-0218', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2077, 42, 'Woda toaletowa  PLAYER MEN', 'woda-toaletowa-player-men', 8.97, NULL, 0, 'product-image-59140b50316cf.jpeg', '2017-05-11 08:57:19', NULL, NULL, 0, 'Woda toaletowa PLAYER', 'Woda, toaletowa, PLAYER', '', NULL, 0, '', NULL),
(2078, 42, 'Tusz do rzęs 3D SUPER BLACK', 'tusz-do-rzes-3d-super-black', 5.99, NULL, 0, 'product-image-59140ccc24ee2.jpeg', '2017-05-11 09:03:39', NULL, NULL, 0, 'Tusz do rzęs 3D SUPER BLACK', 'Tusz, rzęs, SUPER, BLACK', '', NULL, 0, '', NULL),
(2079, 42, 'Fluid Colors Day', 'fluid-colors-day', 5.99, NULL, 0, 'product-image-59140e15ac699.jpeg', '2017-05-11 09:09:09', NULL, NULL, 0, 'Fluid Colors Day', 'Fluid, Colors', '', NULL, 0, '', NULL),
(2080, 42, 'Zmywacz do paznokci z gąbką', 'zmywacz-do-paznokci-z-gabka', 1.99, NULL, 0, 'product-image-59140e92d0822.jpeg', '2017-05-11 09:11:14', NULL, NULL, 0, 'Zmywacz do paznokci z gąbką', 'Zmywacz, paznokci, gąbką', '', NULL, 0, '', NULL),
(2081, 42, 'Woda toaletowa PLAYER WOMEN', 'woda-toaletowa-player-women', 8.97, NULL, 0, 'product-image-59140efea770b.jpeg', '2017-05-11 09:13:01', NULL, NULL, 0, 'Woda toaletowa PLAYER WOMEN', 'Woda, toaletowa, PLAYER, WOMEN', '', NULL, 0, '', NULL),
(2082, 42, 'Perfumy 20ml', 'perfumy-20ml', 5.99, NULL, 0, 'product-image-59140f8ea20b4.jpeg', '2017-05-11 09:15:26', NULL, NULL, 0, 'Perfumy 20ml', 'Perfumy, 20ml', '', NULL, 0, '', NULL),
(2083, 42, 'Korektor antybakteryjny', 'korektor-antybakteryjny', 2.99, NULL, 0, 'product-image-59140fdb98a5c.jpeg', '2017-05-11 09:16:42', NULL, NULL, 0, 'Korektor antybakteryjny', 'Korektor, antybakteryjny', '', NULL, 0, '', NULL),
(2084, 42, 'Silikonowa baza pod makijaż', 'silikonowa-baza-pod-makijaz', 5.99, NULL, 0, 'product-image-59141018aa029.jpeg', '2017-05-11 09:17:44', NULL, NULL, 0, 'Silikonowa baza pod makijaż', 'Silikonowa, baza, makijaż', '', NULL, 0, '', NULL),
(2085, 42, 'Krem do rąk', 'krem-do-rak', 2.99, NULL, 0, 'product-image-591410b4a0db8.jpeg', '2017-05-11 09:20:20', NULL, NULL, 0, 'Krem do rąk', 'Krem, rąk', '', NULL, 0, '', NULL),
(2086, 42, 'Lakier do paznokci DEL SOL COLOR - MIX', 'lakier-do-paznokci-del-sol-color-mix', 2.99, NULL, 0, 'product-image-591411075852a.jpeg', '2017-05-11 09:21:42', NULL, NULL, 0, 'Lakier do paznokci DEL SOL COLOR - MIX', 'Lakier, paznokci, COLOR', '', NULL, 0, '', NULL),
(2087, 37, 'Zegar AS01396', 'zegar-as01396', 14.95, NULL, 1, 'product-image-591411abdf0b9.jpeg', '2017-05-11 09:24:27', NULL, NULL, 0, 'Zegar AS01396', 'Zegar, AS01396', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2088, 37, 'Zegar AS01399', 'zegar-as01399', 14.95, NULL, 1, 'product-image-5914120b327b1.jpeg', '2017-05-11 09:26:02', NULL, NULL, 0, 'Zegar AS01399', 'Zegar, AS01399', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2089, 37, 'Zdjęcia AS01399', 'zdjecia-as01399', 14.95, NULL, 1, 'product-image-5914124ec198c.jpeg', '2017-05-11 09:27:10', NULL, NULL, 0, 'Zdjęcia AS01399', 'Zdjęcia, AS01399', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2090, 37, 'Zegar AS01400', 'zegar-as01400', 14.95, NULL, 1, 'product-image-591412a8b6f37.jpeg', '2017-05-11 09:28:40', NULL, NULL, 0, 'Zegar AS01400', 'Zegar, AS01400', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2091, 35, 'Kwiat sztuczny w doniczce AS01121', 'kwiat-sztuczny-w-doniczce-as01121', 5.99, NULL, 1, 'product-image-5915749c63bdd.jpeg', '2017-05-12 10:38:51', NULL, NULL, 0, 'Kwiat sztuczny w doniczce AS01121', 'Kwiat, sztuczny, doniczce, AS01121', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2092, 31, 'Tablica suchościeralna 30x20cm AS01515', 'tablica-suchoscieralna-30x20cm-as01515', 5.99, NULL, 1, 'product-image-59157509975ca.jpeg', '2017-05-12 10:40:40', NULL, NULL, 0, 'Tablica suchościeralna 30x20cm AS01515', 'Tablica, suchościeralna, 30x20cm, AS01515', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2093, 39, 'Komplet  2 kubków+łyżeczki AS01028', 'komplet-2-kubkow-lyzeczki-as01028', 11.98, NULL, 1, 'product-image-591575f21de7e.jpeg', '2017-05-12 10:44:33', NULL, NULL, 0, 'Komplet  2 kubków+łyżeczki AS01028', 'Komplet, kubków+łyżeczki, AS01028', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2094, 37, 'Brelok do kluczy AS01016', 'brelok-do-kluczy-as01016', 2.99, NULL, 1, 'product-image-59157678a151b.jpeg', '2017-05-12 10:46:48', NULL, NULL, 0, 'Brelok do kluczy AS01016', 'Brelok, kluczy, AS01016', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2095, 37, 'Figurka ceramiczna pies AS01011', 'figurka-ceramiczna-pies-as01011', 5.99, NULL, 1, 'product-image-591576f44e068.jpeg', '2017-05-12 10:48:51', NULL, NULL, 0, 'Figurka ceramiczna pies AS01011', 'Figurka, ceramiczna, pies, AS01011', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2096, 38, 'Kpl do przypraw - AS01374', 'kpl-do-przypraw-as01374', 2.99, NULL, 1, 'product-image-591577694aa13.jpeg', '2017-05-12 10:50:48', NULL, NULL, 0, 'Kpl do przypraw - AS01374', 'przypraw, AS01374', '', NULL, 0, '', '2017-05-30 00:00:00'),
(2097, 38, 'Deski do krojenia +nóż - AST60730', 'deski-do-krojenia-noz-ast60730', 5.99, NULL, 1, 'product-image-5915780d803ab.jpeg', '2017-05-12 10:53:01', NULL, NULL, 0, 'Deski do krojenia +nóż - AST60730', 'Deski, krojenia, +nóż, AST60730', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2098, 38, 'Kubek ceramiczny AS01029', 'kubek-ceramiczny-as01029', 3.99, NULL, 0, 'product-image-591579e58bb14.jpeg', '2017-05-12 11:01:24', NULL, NULL, 0, 'Kubek ceramiczny AS01029', 'Kubek, ceramiczny, AS01029', '', NULL, 0, '', NULL),
(2099, 38, 'Kubek ceramiczny AS01030', 'kubek-ceramiczny-as01030', 3.99, NULL, 0, 'product-image-59157a1f5a001.jpeg', '2017-05-12 11:02:22', NULL, NULL, 0, 'Kubek ceramiczny AS01030', 'Kubek, ceramiczny, AS01030', '', NULL, 0, '', NULL),
(2100, 37, 'Osłonka na doniczkę plastikowa - AS01459', 'oslonka-na-doniczke-plastikowa-as01459', 1.99, NULL, 1, 'product-image-59157a9e9e284.jpeg', '2017-05-12 11:04:29', NULL, NULL, 0, 'Osłonka na doniczkę plastikowa - AS01459', 'Osłonka, doniczkę, plastikowa, AS01459', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2101, 34, 'Kosz na śmieci As01391', 'kosz-na-smieci-as01391', 2.99, NULL, 1, 'product-image-59157ae441c94.jpeg', '2017-05-12 11:05:39', NULL, NULL, 0, 'Kosz na śmieci As01391', 'Kosz, śmieci, As01391', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2102, 37, 'Lampka na baterię AS01007', 'lampka-na-baterie-as01007', 17.97, NULL, 1, 'product-image-59157b67eb467.jpeg', '2017-05-12 11:07:50', NULL, NULL, 0, 'Lampka na baterię AS01007', 'Lampka, baterię, AS01007', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2103, 37, 'Lampka na baterię AS01006', 'lampka-na-baterie-as01006', 11.98, NULL, 1, 'product-image-59157bc63a23f.jpeg', '2017-05-12 11:09:25', NULL, NULL, 0, 'Lampka na baterię AS01006', 'Lampka, baterię, AS01006', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2104, 37, 'Latarka z magnesem- AS01302', 'latarka-z-magnesem-as01302', 9.95, NULL, 1, 'product-image-59157c4a0486b.jpeg', '2017-05-12 11:11:37', NULL, NULL, 0, 'Lampka z magnesem- AS01302', 'Lampka, magnesem-, AS01302', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2105, 37, 'Magnes na lodówkę - AS01033', 'magnes-na-lodowke-as01033', 2.99, NULL, 1, 'product-image-59157cf0c1a34.jpeg', '2017-05-12 11:14:24', NULL, NULL, 0, 'Magnes na lodówkę - AS01033', 'Magnes, lodówkę, AS01033', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2106, 37, 'Magnes na lodówkę - AS01034', 'magnes-na-lodowke-as01034', 2.99, NULL, 1, 'product-image-59157d6b361fb.jpeg', '2017-05-12 11:16:26', NULL, NULL, 0, 'Magnes na lodówkę - AS01034', 'Magnes, lodówkę, AS01034', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2107, 37, 'Torebka prezentowa 14x7x15 - AS01088', 'torebka-prezentowa-14x7x15-as01088', 1.99, NULL, 0, 'product-image-59157dd9e17a8.jpeg', '2017-05-12 11:18:17', NULL, NULL, 0, 'Torebka prezentowa 14x7x15 - AS01088', 'Torebka, prezentowa, 14x7x15, AS01088', '', NULL, 0, '', NULL),
(2108, 37, 'Torebka prezentowa 14x7x15 - AS01092', 'torebka-prezentowa-14x7x15-as01092', 1.99, NULL, 0, 'product-image-59157e2fe5d20.jpeg', '2017-05-12 11:19:43', NULL, NULL, 0, 'Torebka prezentowa 14x7x15 - AS01092', 'Torebka, prezentowa, 14x7x15, AS01092', '', NULL, 0, '', NULL),
(2109, 38, 'Moździerz - AS01393', 'mozdzierz-as01393', 17.97, NULL, 1, 'product-image-59157eae1bb7e.jpeg', '2017-05-12 11:21:48', NULL, NULL, 0, 'Moździerz - AS01393', 'Moździerz, AS01393', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2110, 36, 'Zestaw gumek do włosów - AS01147', 'zestaw-gumek-do-wlosow-as01147', 1.00, NULL, 0, 'product-image-59157f47277e4.jpeg', '2017-05-12 11:24:21', NULL, NULL, 0, 'Zestaw gumek do włosów - AS01147', 'Zestaw, gumek, włosów, AS01147', '', NULL, 0, '', NULL),
(2111, 36, 'Przebranie karnawałowe - pszczoła - AS01208', 'przebranie-karnawalowe-pszczola-as01208', 11.98, NULL, 1, 'product-image-59157fda8d2a0.jpeg', '2017-05-12 11:26:49', NULL, NULL, 0, 'Przebranie karnawałowe - pszczoła - AS01208', 'Przebranie, karnawałowe, pszczoła, AS01208', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2112, 36, 'Skrzydła wróżki - AS01206', 'skrzydla-wrozki-as01206', 5.99, NULL, 1, 'product-image-5915802ea87b2.jpeg', '2017-05-12 11:28:13', NULL, NULL, 0, 'Skrzydła wróżki - AS01206', 'Skrzydła, wróżki, AS01206', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2113, 36, 'Skrzydła AS01207', 'skrzydla-as01207', 5.99, NULL, 1, 'product-image-591580f897c5b.jpeg', '2017-05-12 11:31:35', NULL, NULL, 0, 'Skrzydła AS01207', 'Skrzydła, AS01207', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2114, 36, 'Przebranie karnawałowe - Biedronka - AS01210', 'przebranie-karnawalowe-biedronka-as01210', 11.98, NULL, 1, 'product-image-59158171a53df.jpeg', '2017-05-12 11:33:36', NULL, NULL, 0, 'Przebranie karnawałowe - Biedronka - AS01210', 'Przebranie, karnawałowe, Biedronka, AS01210', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2115, 34, 'Kosz na bieliznę - AS01394', 'kosz-na-bielizne-as01394', 23.96, NULL, 1, 'product-image-591581ba6f069.jpeg', '2017-05-12 11:34:49', NULL, NULL, 0, 'Kosz na bieliznę - AS01394', 'Kosz, bieliznę, AS01394', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2116, 37, 'Latarnie kpl.3szt.', 'latarnie-kpl-3szt', 210.00, NULL, 1, 'product-image-5919949964e94.jpeg', '2017-05-15 13:44:24', NULL, NULL, 0, 'Latarnie kpl.3szt.', 'Latarnie, kpl.3szt.', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2117, 39, 'Patera CBP-03', 'patera-cbp-03', 17.97, NULL, 1, 'product-image-59199a7966fb1.jpeg', '2017-05-15 14:09:28', NULL, NULL, 0, 'Patera CBP-03', 'Patera, CBP-03', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2118, 37, 'Osłonka ceramiczna YT3628', 'oslonka-ceramiczna-yt3628', 5.99, NULL, 1, 'product-image-59199ae0c2b8d.jpeg', '2017-05-15 14:11:12', NULL, NULL, 0, 'Osłonka ceramiczna YT3628', 'Osłonka, ceramiczna, YT3628', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2119, 37, 'Osłonka ceramiczna SF1626', 'oslonka-ceramiczna-sf1626', 5.99, NULL, 1, 'product-image-59199b26aa272.jpeg', '2017-05-15 14:12:22', NULL, NULL, 0, 'Osłonka ceramiczna SF1626', 'Osłonka, ceramiczna, SF1626', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2120, 34, 'Moskitiera na drzwi 75x220cm NFB-06', 'moskitiera-na-drzwi-75x220cm-nfb-06', 9.95, NULL, 1, 'product-image-59199bb95eca2.jpeg', '2017-05-15 14:14:49', NULL, NULL, 0, 'Moskitiera na drzwi 75x220cm NFB-06', 'Moskitiera, drzwi, 75x220cm, NFB-06', '', NULL, 0, '', '2017-05-16 00:00:00'),
(2121, 37, 'Osłonka metalowa 13x10x13cm - FNY1604-01', 'oslonka-metalowa-13x10x13cm-fny1604-01', 5.99, NULL, 1, 'product-image-59199c1580502.jpeg', '2017-05-15 14:16:20', NULL, NULL, 0, 'Osłonka metalowa 13x10x13cm - FNY1604-01', 'Osłonka, metalowa, 13x10x13cm, FNY1604-01', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2122, 37, 'Osłonka metalowa 8x6x7,5cm - FNY1604-02', 'oslonka-metalowa-8x6x7-5cm-fny1604-02', 3.99, NULL, 1, 'product-image-59199c56af6a1.jpeg', '2017-05-15 14:17:26', NULL, NULL, 0, 'Osłonka metalowa 8x6x7,5cm - FNY1604-02', 'Osłonka, metalowa, 8x6x7,5cm, FNY1604-02', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2123, 37, 'Ramka na zdjęcia 15x20cm - NB9045-68', 'ramka-na-zdjecia-15x20cm-nb9045-68', 5.99, NULL, 1, 'product-image-59199cc9ada4b.jpeg', '2017-05-15 14:19:21', NULL, NULL, 0, 'Ramka na zdjęcia 15x20cm - NB9045-68', 'Ramka, zdjęcia, 15x20cm, NB9045-68', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2124, 37, 'Lampka biurowa na baterie H23cm - 18488M', 'lampka-biurowa-na-baterie-h23cm-18488m', 5.99, NULL, 1, 'product-image-59199d5ce06f5.jpeg', '2017-05-15 14:21:48', NULL, NULL, 0, 'Lampka biurowa na baterie H23cm - 18488M', 'Lampka, biurowa, baterie, H23cm, 18488M', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2125, 43, 'Zraszacz ogrodowy OXG-128', 'zraszacz-ogrodowy-oxg-128', 5.99, NULL, 1, 'product-image-59199e38a3fde.jpeg', '2017-05-15 14:25:28', NULL, NULL, 0, 'Zraszacz ogrodowy OXG-128', 'Zraszacz, ogrodowy, OXG-128', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2126, 37, 'Osłonka metalowa - FNY1604-24', 'oslonka-metalowa-fny1604-24', 5.99, NULL, 1, 'product-image-59199e7dc8d28.jpeg', '2017-05-15 14:26:36', NULL, NULL, 0, 'Osłonka metalowa - FNY1604-24', 'Osłonka, metalowa, FNY1604-24', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2127, 37, 'Ramka na zdjęcia 15x20cm', 'ramka-na-zdjecia-15x20cm', 5.99, NULL, 1, 'product-image-59199ef1cf035.jpeg', '2017-05-15 14:28:33', NULL, NULL, 0, 'Ramka na zdjęcia 15x20cm', 'Ramka, zdjęcia, 15x20cm', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2128, 37, 'Świecznik 9,5x9,5xH8,5cm - LY-26', 'swiecznik-9-5x9-5xh8-5cm-ly-26', 5.99, NULL, 1, 'product-image-59199f5939696.jpeg', '2017-05-15 14:30:16', NULL, NULL, 0, 'Świecznik 9,5x9,5xH8,5cm - LY-26', 'Świecznik, 9,5x9,5xH8,5cm, LY-26', '', NULL, 0, '', '2017-05-31 00:00:00'),
(2129, 39, 'Talerzyki deserowe kpl.6szt - CBP-02', 'talerzyki-deserowe-kpl-6szt-cbp-02', 14.95, NULL, 1, 'product-image-5919a044598e4.jpeg', '2017-05-15 14:34:12', NULL, NULL, 0, 'Talerzyki deserowe kpl.6szt - CBP-02', 'Talerzyki, deserowe, kpl.6szt, CBP-02', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2130, 37, 'Zawieszka serce z rattanu 20x30cm - LY-18', 'zawieszka-serce-z-rattanu-20x30cm-ly-18', 5.99, NULL, 1, 'product-image-5919a0ac2a109.jpeg', '2017-05-15 14:35:55', NULL, NULL, 0, 'Zawieszka serce z rattanu 20x30cm - LY-18', 'Zawieszka, serce, rattanu, 20x30cm, LY-18', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2131, 37, 'Poszewka dekoracyjna 43x43cm - MHF4185', 'poszewka-dekoracyjna-43x43cm-mhf4185', 5.99, NULL, 1, 'product-image-5919a17580a2e.jpeg', '2017-05-15 14:39:17', NULL, NULL, 0, 'Poszewka dekoracyjna 43x43cm - MHF4185', 'Poszewka, dekoracyjna, 43x43cm, MHF4185', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2132, 38, 'Zapalarka gazowa - HB8341', 'zapalarka-gazowa-hb8341', 2.99, NULL, 1, 'product-image-5919a229615d4.jpeg', '2017-05-15 14:42:16', NULL, NULL, 0, 'Zapalarka gazowa - HB8341', 'Zapalarka, gazowa, HB8341', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2133, 39, 'Patera 25x19,5cm - CBP-01', 'patera-25x19-5cm-cbp-01', 17.97, NULL, 1, 'product-image-5919a2d34cbd6.jpeg', '2017-05-15 14:45:06', NULL, NULL, 0, 'Patera 25x19,5cm - CBP-01', 'Patera, 25x19,5cm, CBP-01', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2134, 39, 'Kubek szklany z nadrukiem 310ml - KTG-241', 'kubek-szklany-z-nadrukiem-310ml-ktg-241', 2.99, NULL, 1, 'product-image-5919a39035825.jpeg', '2017-05-15 14:48:15', NULL, NULL, 0, 'Kubek szklany z nadrukiem 310ml - KTG-241', 'Kubek, szklany, nadrukiem, 310ml, KTG-241', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2135, 39, 'Karafka 250ml - OZ5202', 'karafka-250ml-oz5202', 2.99, NULL, 1, 'product-image-5919a3f263a42.jpeg', '2017-05-15 14:49:53', NULL, NULL, 0, 'Karafka 250ml - OZ5202', 'Karafka, 250ml, OZ5202', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2136, 37, 'Słoik dekoracyjny ze światłem LED - NS-03', 'sloik-dekoracyjny-ze-swiatlem-led-ns-03', 6.99, NULL, 1, 'product-image-5919a4603ee63.jpeg', '2017-05-15 14:51:43', NULL, NULL, 0, 'Słoik dekoracyjny ze światłem LED - NS-03', 'Słoik, dekoracyjny, światłem, NS-03', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2137, 37, 'Organizer do szafy 33x28xH15cm - PTR-32', 'organizer-do-szafy-33x28xh15cm-ptr-32', 8.97, NULL, 1, 'product-image-5919a59c80379.jpeg', '2017-05-15 14:56:59', NULL, NULL, 0, 'Organizer do szafy 33x28xH15cm - PTR-32', 'Organizer, szafy, 33x28xH15cm, PTR-32', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2138, 32, 'Samochód - AS01214', 'samochod-as01214', 5.99, NULL, 1, 'product-image-591aff1600f6d.jpeg', '2017-05-16 15:31:01', NULL, NULL, 0, 'Samochód - AS01214', 'Samochód, AS01214', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2139, 37, 'Szkatułka na biżuterię - AS01048', 'szkatulka-na-bizuterie-as01048', 5.99, NULL, 1, 'product-image-591affa9bb1d6.jpeg', '2017-05-16 15:33:29', NULL, NULL, 0, 'Szkatułka na biżuterię - AS01048', 'Szkatułka, biżuterię, AS01048', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2140, 37, 'Szkatułka na biżuterię - AS01048', 'szkatulka-na-bizuterie-as01048-1', 5.99, NULL, 1, 'product-image-591affe9883f2.jpeg', '2017-05-16 15:34:32', NULL, NULL, 0, 'Szkatułka na biżuterię - AS01048', 'Szkatułka, biżuterię, AS01048', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2141, 37, 'Szkatułka na biżuterię - AS01047', 'szkatulka-na-bizuterie-as01047', 5.99, NULL, 1, 'product-image-591b0042207a1.jpeg', '2017-05-16 15:36:01', NULL, NULL, 0, 'Szkatułka na biżuterię - AS01047', 'Szkatułka, biżuterię, AS01047', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2142, 32, 'Skarbonka - AS01506', 'skarbonka-as01506', 3.99, NULL, 1, 'product-image-591b008b46e1d.jpeg', '2017-05-16 15:37:15', NULL, NULL, 0, 'Skarbonka - AS01506', 'Skarbonka, AS01506', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2143, 32, 'Skarbonka - AS01503', 'skarbonka-as01503', 2.99, NULL, 1, 'product-image-591b00e675fe8.jpeg', '2017-05-16 15:38:46', NULL, NULL, 0, 'Skarbonka - AS01503', 'Skarbonka, AS01503', '', NULL, 0, '', '2017-06-15 00:00:00');
INSERT INTO `product` (`id`, `category_id`, `name`, `slug`, `price`, `promotionPrice`, `isNew`, `photo`, `createdAt`, `updatedAt`, `quantity`, `seoGenerate`, `seoTitle`, `seoKeywords`, `seoDescription`, `content`, `contentShortGenerate`, `contentShort`, `newUntilDate`) VALUES
(2144, 32, 'Skarbonka - AS01508', 'skarbonka-as01508', 3.99, NULL, 1, 'product-image-591b01314d715.jpeg', '2017-05-16 15:40:00', NULL, NULL, 0, 'Skarbonka - AS01508', 'Skarbonka, AS01508', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2145, 32, 'Skarbonka - AS01507', 'skarbonka-as01507', 3.99, NULL, 1, 'product-image-591b019145a35.jpeg', '2017-05-16 15:41:36', NULL, NULL, 0, 'Skarbonka - AS01507', 'Skarbonka, AS01507', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2146, 32, 'Skarbonka - AS01505', 'skarbonka-as01505', 2.99, NULL, 1, 'product-image-591b0246bf937.jpeg', '2017-05-16 15:44:38', NULL, NULL, 0, 'Skarbonka - AS01505', 'Skarbonka, AS01505', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2147, 37, 'Figurka drewniana 32cm - AS00571', 'figurka-drewniana-32cm-as00571', 14.95, NULL, 1, 'product-image-591b0373e056c.jpeg', '2017-05-16 15:49:39', NULL, NULL, 0, 'Figurka drewniana 32cm - AS00571', 'Figurka, drewniana, 32cm, AS00571', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2148, 32, 'Zabawka nakręcana - Myszka AS01228', 'zabawka-nakrecana-myszka-as01228', 3.99, NULL, 1, 'product-image-591b03cce3551.jpeg', '2017-05-16 15:51:08', NULL, NULL, 0, 'Zabawka nakręcana - Myszka AS01228', 'Zabawka, nakręcana, Myszka, AS01228', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2149, 32, 'Zabawka nakręcana - Wielbłąd AS01233', 'zabawka-nakrecana-wielblad-as01233', 3.99, NULL, 1, 'product-image-591b0427c64b7.jpeg', '2017-05-16 15:52:39', NULL, NULL, 0, 'Zabawka nakręcana - Wielbłąd AS01233', 'Zabawka, nakręcana, Wielbłąd, AS01233', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2150, 32, 'Zabawka nakręcana - Konik -  AS01230', 'zabawka-nakrecana-konik-as01230', 3.99, NULL, 1, 'product-image-591b046f223cd.jpeg', '2017-05-16 15:53:50', NULL, NULL, 0, 'Zabawka nakręcana - Konik -  AS01230', 'Zabawka, nakręcana, Konik, AS01230', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2151, 32, 'Zabawka nakręcana - Gąsienica - AS01231', 'zabawka-nakrecana-gasienica-as01231', 3.99, NULL, 1, 'product-image-591b04e01e9a7.jpeg', '2017-05-16 15:55:43', NULL, NULL, 0, 'Zabawka nakręcana - Gąsienica - AS01231', 'Zabawka, nakręcana, Gąsienica, AS01231', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2152, 37, 'Magnes na lodówkę - AS01042', 'magnes-na-lodowke-as01042', 2.99, NULL, 1, 'product-image-591b053fa392c.jpeg', '2017-05-16 15:57:19', NULL, NULL, 0, 'Magnes na lodówkę - AS01042', 'Magnes, lodówkę, AS01042', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2153, 37, 'Magnes na lodówkę - AS01035', 'magnes-na-lodowke-as01035', 2.99, NULL, 1, 'product-image-591b05860a5cb.jpeg', '2017-05-16 15:58:29', NULL, NULL, 0, 'Magnes na lodówkę - AS01035', 'Magnes, lodówkę, AS01035', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2154, 37, 'Magnes na lodówkę - AS01037', 'magnes-na-lodowke-as01037', 2.99, NULL, 1, 'product-image-591b05b900081.jpeg', '2017-05-16 15:59:20', NULL, NULL, 0, 'Magnes na lodówkę - AS01032', 'Magnes, lodówkę, AS01032', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2155, 43, 'Końcówka do węża ogrodowego - AS01611', 'koncowka-do-weza-ogrodowego-as01611', 5.99, NULL, 1, 'product-image-591b0627848e8.jpeg', '2017-05-16 16:01:11', NULL, NULL, 0, 'Końcówka do węża ogrodowego - AS01611', 'Końcówka, węża, ogrodowego, AS01611', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2156, 43, 'Zestaw pędzli - AS01638', 'zestaw-pedzli-as01638', 5.99, NULL, 1, 'product-image-591b069b01fec.jpeg', '2017-05-16 16:03:06', NULL, NULL, 0, 'Zestaw pędzli - AS01368', 'Zestaw, pędzli, AS01368', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2157, 38, 'Foremki do ciasteczek 4szt. - AS01306', 'foremki-do-ciasteczek-4szt-as01306', 2.99, NULL, 1, 'product-image-591be8d8206d6.jpeg', '2017-05-17 08:08:23', NULL, NULL, 0, 'Foremki do ciasteczek 4szt. - AS01306', 'Foremki, ciasteczek, 4szt., AS01306', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2158, 43, 'Lampki rowerowe 2szt. - AS01085', 'lampki-rowerowe-2szt-as01085', 5.99, NULL, 1, 'product-image-591be98333552.jpeg', '2017-05-17 08:11:14', NULL, NULL, 0, 'Lampki rowerowe 2szt. - AS01085', 'Lampki, rowerowe, 2szt., AS01085', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2159, 31, 'Zestaw ołówków + gumka + temperówka - AS01697', 'zestaw-olowkow-gumka-temperowka-as01697', 2.99, NULL, 1, 'product-image-591bea5d1a2b6.jpeg', '2017-05-17 08:14:52', NULL, NULL, 0, 'Zestaw ołówków + gumka + temperówka - AS01697', 'Zestaw, ołówków, gumka, temperówka, AS01697', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2160, 43, 'Żyłka do kosiarki 30m - AS01427', 'zylka-do-kosiarki-30m-as01427', 2.99, NULL, 1, 'product-image-591bead303bb6.jpeg', '2017-05-17 08:16:50', NULL, NULL, 0, 'Żyłka do kosiarki 30m - AS01427', 'Żyłka, kosiarki, AS01427', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2161, 43, 'Końcówki do węża ogrodowego - AS01612', 'koncowki-do-weza-ogrodowego-as01612', 5.99, NULL, 0, 'product-image-591bebe3587af.jpeg', '2017-05-17 08:21:22', NULL, NULL, 0, 'Końcówki do węża ogrodowego - AS01612', 'Końcówki, węża, ogrodowego, AS01612', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2162, 43, 'Zestaw śrubokrętów - 2szt. - AS01625', 'zestaw-srubokretow-2szt-as01625', 2.99, NULL, 1, 'product-image-591bec21d8657.jpeg', '2017-05-17 08:22:25', NULL, NULL, 0, 'Zestaw śrubokrętów - 2szt. - AS01625', 'Zestaw, śrubokrętów, 2szt., AS01625', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2163, 43, 'Zestaw śrubokrętów - 5szt. - AS01632', 'zestaw-srubokretow-5szt-as01632', 5.99, NULL, 1, 'product-image-591becb18900e.jpeg', '2017-05-17 08:24:48', NULL, NULL, 0, 'Zestaw śrubokrętów - 5szt. - AS01632', 'Zestaw, śrubokrętów, 5szt., AS01632', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2164, 31, 'Długopisy kpl.4szt. - AS01690', 'dlugopisy-kpl-4szt-as01690', 2.99, NULL, 1, 'product-image-591bedd2d79ee.jpeg', '2017-05-17 08:29:38', NULL, NULL, 0, 'Długopisy kpl.4szt. - AS01690', 'Długopisy, kpl.4szt., AS01690', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2165, 31, 'Cienkopisy 12szt. - AS00560', 'cienkopisy-12szt-as00560', 2.99, NULL, 1, 'product-image-591bee1cac466.jpeg', '2017-05-17 08:30:52', NULL, NULL, 0, 'Cienkopisy 12szt. - AS00560', 'Cienkopisy, 12szt., AS00560', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2166, 31, 'Szkło powiększające - AS01620', 'szklo-powiekszajace-as01620', 2.99, NULL, 1, 'product-image-591bee6821bec.jpeg', '2017-05-17 08:32:07', NULL, NULL, 0, 'Szkło powiększające - AS01620', 'Szkło, powiększające, AS01620', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2167, 43, 'Taśma miernicza - AS01290', 'tasma-miernicza-as01290', 5.99, NULL, 1, 'product-image-591beeabd64eb.jpeg', '2017-05-17 08:33:15', NULL, NULL, 0, 'Taśma miernicza - AS01290', 'Taśma, miernicza, AS01290', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2168, 42, 'Plastry 100szt. - AS01452', 'plastry-100szt-as01452', 2.99, NULL, 1, 'product-image-591d46f66dc3f.jpeg', '2017-05-18 09:02:13', NULL, NULL, 0, 'Plastry - AS01452', 'Plastry, AS01452', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2169, 42, 'Plastry 50szt. - AS01451', 'plastry-50szt-as01451', 1.99, NULL, 1, 'product-image-591d47800b684.jpeg', '2017-05-18 09:04:31', NULL, NULL, 0, 'Plastry 50szt. - AS01451', 'Plastry, 50szt., AS01451', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2170, 38, 'Foremki do babeczek - AS01378', 'foremki-do-babeczek-as01378', 2.99, NULL, 1, 'product-image-591d482fe2f87.jpeg', '2017-05-18 09:07:27', NULL, NULL, 0, 'Foremki do babeczek - AS01378', 'Foremki, babeczek, AS01378', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2171, 43, 'Nóż do tapet - AS01288', 'noz-do-tapet-as01288', 2.99, NULL, 1, 'product-image-591d48ed628dd.jpeg', '2017-05-18 09:10:36', NULL, NULL, 0, 'Nóż do tapet - AS01288', 'Nóż, tapet, AS01288', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2172, 43, 'Nóż do tapet + wkłady - AS01287', 'noz-do-tapet-wklady-as01287', 2.99, NULL, 1, 'product-image-591d49bfc8f07.jpeg', '2017-05-18 09:14:07', NULL, NULL, 0, 'Nóż do tapet + wkłady - AS01287', 'Nóż, tapet, wkłady, AS01287', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2173, 34, 'Klamerki do bielizny 24szt. - AS01415', 'klamerki-do-bielizny-24szt-as01415', 2.99, NULL, 1, 'product-image-591d4ab46d98d.jpeg', '2017-05-18 09:18:12', NULL, NULL, 0, 'Klamerki do bielizny 24szt. - AS01415', 'Klamerki, bielizny, 24szt., AS01415', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2174, 38, 'Komplet drewniany mini', 'komplet-drewniany-mini', 5.99, NULL, 1, 'product-image-591d4b4025edd.jpeg', '2017-05-18 09:20:31', NULL, NULL, 0, 'Komplet drewniany mini', 'Komplet, drewniany, mini', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2175, 31, 'Długopisy 5szt - AS01689', 'dlugopisy-5szt-as01689', 2.99, NULL, 0, 'product-image-591d4bb21f6e0.jpeg', '2017-05-18 09:22:25', NULL, NULL, 0, 'Długopisy 5szt - AS01689', 'Długopisy, 5szt, AS01689', '', NULL, 0, '', NULL),
(2176, 37, 'Zegar - AS01397', 'zegar-as01397', 14.95, NULL, 1, 'product-image-591d4c494861c.jpeg', '2017-05-18 09:24:56', NULL, NULL, 0, 'Zegar - AS01397', 'Zegar, AS01397', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2177, 37, 'Kule dekoracyjne 3szt. - IV1774', 'kule-dekoracyjne-3szt-iv1774', 6.99, NULL, 1, 'product-image-591d80658fc14.jpeg', '2017-05-18 13:07:17', NULL, NULL, 0, 'Kule dekoracyjne 3szt. - IV1774', 'Kule, dekoracyjne, 3szt., IV1774', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2178, 32, 'Piłeczka z uśmiechem - CWN-16', 'pileczka-z-usmiechem-cwn-16', 2.99, NULL, 1, 'product-image-591d80eb6d82a.jpeg', '2017-05-18 13:09:31', NULL, NULL, 0, 'Piłeczka - CWN-16', 'Piłeczka, CWN-16', '', NULL, 0, '', '2017-06-16 00:00:00'),
(2179, 37, 'Wieszak ozdobny HOME - NBB-0143', 'wieszak-ozdobny-home-nbb-0143', 9.95, NULL, 1, 'product-image-591d813fa12c0.jpeg', '2017-05-18 13:10:55', NULL, NULL, 0, 'Wieszak ozdobny HOME - NBB-0143', 'Wieszak, ozdobny, HOME, NBB-0143', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2180, 32, 'Skakanka 2,2m - M142057', 'skakanka-2-2m-m142057', 5.99, NULL, 1, 'product-image-591d817f38dd5.jpeg', '2017-05-18 13:11:58', NULL, NULL, 0, 'Skakanka - M142057', 'Skakanka, M142057', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2181, 37, 'Ramka na zdjęcia 13x18cm - NB9052-57', 'ramka-na-zdjecia-13x18cm-nb9052-57', 5.99, NULL, 1, 'product-image-591d81e579ff5.jpeg', '2017-05-18 13:13:41', NULL, NULL, 0, 'Ramka na zdjęcia 13x18cm - NB9052-57', 'Ramka, zdjęcia, 13x18cm, NB9052-57', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2182, 37, 'Ramka na zdjęcia 13x18cm - NB9120-57', 'ramka-na-zdjecia-13x18cm-nb9120-57', 5.99, NULL, 1, 'product-image-591d82812084d.jpeg', '2017-05-18 13:16:16', NULL, NULL, 0, 'Ramka na zdjęcia 13x18cm - NB9120-57', 'Ramka, zdjęcia, 13x18cm, NB9120-57', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2183, 37, 'Latarenka 12,5cm - GL-73', 'latarenka-12-5cm-gl-73', 6.99, NULL, 1, 'product-image-591d82b095d5d.jpeg', '2017-05-18 13:17:03', NULL, NULL, 0, 'Latarenka - GL-73', 'Latarenka, GL-73', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2184, 43, 'Pompka do roweru 23cm - B1606181', 'pompka-do-roweru-23cm-b1606181', 5.99, NULL, 1, 'product-image-591d8ae000ca4.jpeg', '2017-05-18 13:51:59', NULL, NULL, 0, 'Pompka do roweru 23cm - B1606181', 'Pompka, roweru, 23cm, B1606181', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2185, 37, 'Lampion 160851-1', 'lampion-160851-1', 17.97, NULL, 1, 'product-image-591ee635e484f.jpeg', '2017-05-19 14:33:57', NULL, NULL, 0, 'Lampion 160851-1', 'Lampion, 160851-1', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2186, 38, 'Strzykawka do marynaty 30ml -  NG77174A', 'strzykawka-do-marynaty-30ml-ng77174a', 2.99, NULL, 1, 'product-image-591ee79b3cd85.jpeg', '2017-05-19 14:39:54', NULL, NULL, 0, 'Strzykawka do marynaty 30ml -', 'Strzykawka, marynaty, 30ml', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2187, 38, 'Dzbanek z zaparzaczem  1250ml - NGY01423', 'dzbanek-z-zaparzaczem-1250ml-ngy01423', 9.99, NULL, 1, 'product-image-591ee8683d50f.jpeg', '2017-05-19 14:43:19', NULL, NULL, 0, 'Dzbanek z zaparzaczem  1250ml - NGY01423', 'Dzbanek, zaparzaczem, 1250ml, NGY01423', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2188, 37, 'Doniczka ceramiczna - 20199', 'doniczka-ceramiczna-20199', 5.99, NULL, 1, 'product-image-591ee8d2bfaa7.jpeg', '2017-05-19 14:45:06', NULL, NULL, 0, 'Doniczka ceramiczna - 20199', 'Doniczka, ceramiczna, 20199', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2189, 37, 'Doniczka ceramiczna -20192', 'doniczka-ceramiczna-20192', 5.99, NULL, 1, 'product-image-591ee91629480.jpeg', '2017-05-19 14:46:13', NULL, NULL, 0, 'Doniczka ceramiczna -20192', 'Doniczka, ceramiczna, -20192', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2190, 38, 'Waga kuchenna 1kg - NG77840', 'waga-kuchenna-1kg-ng77840', 5.99, NULL, 0, 'product-image-591ee9805b6b6.jpeg', '2017-05-19 14:48:00', NULL, NULL, 0, 'Waga kuchenna 1kg - WG77840', 'Waga, kuchenna, WG77840', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2191, 38, 'Pojemnik szklany - CA8125-1CL', 'pojemnik-szklany-ca8125-1cl', 3.99, NULL, 1, 'product-image-591eea4b11d56.jpeg', '2017-05-19 14:51:22', NULL, NULL, 0, 'Pojemnik szklany - CA8125-1CL', 'Pojemnik, szklany, CA8125-1CL', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2192, 38, 'Pojemnik szklany - CA8114-2CL', 'pojemnik-szklany-ca8114-2cl', 8.97, NULL, 1, 'product-image-591eea9c412ba.jpeg', '2017-05-19 14:52:43', NULL, NULL, 0, 'Pojemnik szklany - CA8114-2CD', 'Pojemnik, szklany, CA8114-2CD', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2193, 39, 'Butelka szklana - CA8083R', 'butelka-szklana-ca8083r', 3.99, NULL, 1, 'product-image-591eeba67ed48.jpeg', '2017-05-19 14:57:10', NULL, NULL, 0, 'Butelka szklana - CA8083R', 'Butelka, szklana, CA8083R', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2194, 35, 'Kwiaty sztuczne - FLD001', 'kwiaty-sztuczne-fld001', 6.99, NULL, 1, 'product-image-591eec6e89c85.jpeg', '2017-05-19 15:00:29', NULL, NULL, 0, 'Kwiaty sztuczne - FLD001', 'Kwiaty, sztuczne, FLD001', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2197, 35, 'Kwiaty sztuczne - FLE228', 'kwiaty-sztuczne-fle228', 6.99, NULL, 1, 'product-image-591ef0cd53088.jpeg', '2017-05-19 15:19:09', NULL, NULL, 0, 'Kwiaty sztuczne - FLE228', 'Kwiaty, sztuczne, FLE228', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2198, 35, 'Kwiaty sztuczne - FLO028', 'kwiaty-sztuczne-flo028-1', 6.99, NULL, 1, 'product-image-591ef12f972c9.jpeg', '2017-05-19 15:20:47', NULL, NULL, 0, 'Kwiaty sztuczne - FLO028', 'Kwiaty, sztuczne, FLO028', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2199, 35, 'Kwiat sztuczny - FLO001', 'kwiat-sztuczny-flo001', 6.99, NULL, 1, 'product-image-591ef52070ea3.jpeg', '2017-05-19 15:37:36', NULL, NULL, 0, 'Kwiat sztuczny - FLO001', 'Kwiat, sztuczny, FLO001', '', NULL, 0, '', '2017-06-15 00:00:00'),
(2200, 37, 'Latarnie drewniane kpl.3szt. JB08 31x31x85cm; 21,5x21,5x56cm; 13x13x42cm', 'latarnie-drewniane-kpl-3szt-jb08-31x31x85cm-21-5x21-5x56cm-13x13x42cm', 277.00, NULL, 1, 'product-image-592c25048095f.jpeg', '2017-05-29 15:41:24', NULL, NULL, 0, 'Latarnie drewniane kpl.3szt. JB08 31x31x85cm; 21,5x21,5x56cm; 13x13x42cm', 'Latarnie, drewniane, kpl.3szt., JB08, 31x31x85cm;, 21,5x21,5x56cm;, 13x13x42cm', '', NULL, 0, '', '2017-06-30 00:00:00'),
(2201, 37, 'Latarnia drewniana kpl. 2szt. JB06  22x22x47cm; 17x17x37cm;', 'latarnia-drewniana-kpl-2szt-jb06-22x22x47cm-17x17x37cm', 138.00, NULL, 1, 'product-image-592c25bccf4b9.jpeg', '2017-05-29 15:44:28', NULL, NULL, 0, 'Latarnia drewniana kpl. 2szt.  22x22x47cm; 17x17x37cm;', 'Latarnia, drewniana, kpl., 2szt., 22x22x47cm;, 17x17x37cm;', '', NULL, 0, '', '2017-06-30 00:00:00'),
(2202, 37, 'Latarnia drewniana kpl. 2 szt. JB16, 13x13x30cm; 17x17x52cm;', 'latarnia-drewniana-kpl-2-szt-jb16-13x13x30cm-17x17x52cm', 118.00, NULL, 1, 'product-image-592c266b13cb4.jpeg', '2017-05-29 15:47:22', NULL, NULL, 0, 'Latarnia drewniana kpl. 2 szt. JB16, 13x13x30cm; 17x17x52cm;', 'Latarnia, drewniana, kpl., szt., JB16,, 13x13x30cm;, 17x17x52cm;', '', NULL, 0, '', '2017-06-30 00:00:00'),
(2203, 37, 'Płotek ogrodowy 47x36cm, OXG-14', 'plotek-ogrodowy-47x36cm-oxg-14', 2.99, NULL, 1, 'product-image-592c2745c3afc.jpeg', '2017-05-29 15:51:01', NULL, NULL, 0, 'Płotek ogrodowy 47x36cm, OXG-14', 'Płotek, ogrodowy, 47x36cm,, OXG-14', '', NULL, 0, '', '2017-06-30 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `product_attributes`
--

CREATE TABLE `product_attributes` (
  `attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `product_category`
--

CREATE TABLE `product_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `seoGenerate` tinyint(1) DEFAULT NULL,
  `seoTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoKeywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `product_category`
--

INSERT INTO `product_category` (`id`, `name`, `slug`, `createdAt`, `updatedAt`, `seoGenerate`, `seoTitle`, `seoKeywords`, `seoDescription`, `description`, `photo`) VALUES
(31, 'Art. Szkolne', 'artykuly-szkolne', '2017-03-21 20:17:12', NULL, 0, 'Artykuły Szkolne', NULL, NULL, NULL, 'product-category-image-58d54a5bc7809.jpeg'),
(32, 'Zabawki', 'zabawki', '2017-03-21 20:17:22', NULL, 0, 'Zabawki', NULL, NULL, NULL, 'product-category-image-58d54649339c2.jpeg'),
(34, 'Czysty dom', 'czysty-dom', '2017-03-24 16:54:13', NULL, 0, 'Czysty dom', NULL, NULL, NULL, 'product-category-image-58d5495228db8.jpeg'),
(35, 'Kwiaty', 'kwiaty', '2017-03-24 17:40:14', NULL, 0, 'Kwiaty', NULL, NULL, NULL, 'product-category-image-58d926d712e8f.jpeg'),
(36, 'Galanteria', 'galanteria', '2017-03-27 15:19:55', NULL, 0, 'Galanteria', NULL, NULL, NULL, 'product-category-image-58d949febba1e.jpeg'),
(37, 'Artykuły dekoracyjne', 'artykuly-dekoracyjne', '2017-03-27 15:20:04', NULL, 0, 'Artykuły dekoracyjne', NULL, NULL, NULL, 'product-category-image-58d94a4caf9b3.jpeg'),
(38, 'Kuchnia', 'kuchnia', '2017-03-27 19:07:46', NULL, 0, 'Kuchnia', NULL, NULL, NULL, 'product-category-image-58d947c2a771d.jpeg'),
(39, 'Szkło', 'szklo', '2017-03-27 19:08:36', NULL, 0, 'Szkło', NULL, NULL, NULL, 'product-category-image-58d94a2ca15e6.jpeg'),
(42, 'Kosmetyki', 'kosmetyki', '2017-05-11 08:53:24', NULL, 1, NULL, NULL, NULL, NULL, 'product-category-image-59140a6541f36.jpeg'),
(43, 'Narzędzia', 'narzedzia', '2017-05-18 13:40:47', NULL, 0, 'Narzędzia', NULL, NULL, NULL, 'product-category-image-591d9061e13aa.jpeg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `settings`
--

INSERT INTO `settings` (`id`, `label`, `description`, `value`) VALUES
(1, 'seo_title', 'Domyślna wartość meta tagu \"title\"', 'Sklepy Ho-Ho / artykuły wielobranżowe w niskich cenach'),
(2, 'seo_keywords', 'Domyślna wartość meta tagu \"keywords\"', NULL),
(3, 'seo_description', 'Domyślna wartość meta tagu \"description\"', 'Artykuły po 5,99, 2,99 i nie tylko! Obecnie działamy na runku lubelskim, mazowieckim, podlaskim, warmińsko-mazurskim, świętokrzyskim i śląskim.'),
(4, 'email_to', 'Adres e-mail na jaki wysyłana jest wiadomość z formularza kontaktowego', 'biuroastek@op.pl'),
(28, 'contact_page_address', 'Adres na podstronie kontaktowej', 'ul. Nowa 31 <br/>21-345 Wola Osowińska'),
(29, 'contact_page_email', 'E-mail na podstronie kontaktowej', 'biuroastek@op.pl'),
(30, 'limit_products', 'Ilość produktów wyświetlanych na jednej stronie listowania', '12'),
(31, 'contact_page_cell_phone_prefix', 'Prefix numeru telefonu komórkowego na podstronie kontaktowej', '48'),
(32, 'contact_page_cell_phone_number', 'Numer telefonu komórkowego na podstronie kontaktowej', '502 438 740'),
(33, 'contact_page_phone_prefix', 'Prefix numeru telefonu stacjonarnego na podstronie kontaktowej', '81'),
(34, 'contact_page_phone_number', 'Numer telefonu stacjonarnego na podstronie kontaktowej', '851 02 52'),
(35, 'contact_page_company_name', 'Nazwa firmy na podstronie kontaktowej', 'F.H. \"ASTEK\"'),
(37, 'lokal_text', 'Tekst w ramce na podstronie współpraca', 'Posiadasz taki lokal?\r\nZadzwoń: 500 088 828'),
(38, 'shop_text', 'Tekst na główniej podstronie sklepów', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `shop`
--

CREATE TABLE `shop` (
  `id` int(11) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `seoGenerate` tinyint(1) DEFAULT NULL,
  `seoTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoKeywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `weekHours` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weekendHours` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `shop`
--

INSERT INTO `shop` (`id`, `city_id`, `name`, `slug`, `address`, `phone`, `photo`, `createdAt`, `updatedAt`, `seoGenerate`, `seoTitle`, `seoKeywords`, `seoDescription`, `gallery_id`, `weekHours`, `weekendHours`) VALUES
(8, 31, 'Bełżyce', 'belzyce', 'Ul.Spółdzielcza 2', NULL, 'shop-image-53ad1ef5c28c6.jpeg', '2013-12-20 09:41:15', NULL, 1, 'to change', 'to change', 'to change', 8, 'Pn-Pt 9-17', 'Sob 9-14'),
(9, 12, 'Biała Podlaska', 'biala-podlaska', 'Pl.Szkolny Dwór 10', NULL, 'shop-image-5379bde57abcf.jpeg', '2013-12-20 09:42:40', NULL, 1, 'to change', 'to change', 'to change', 28, 'Pn-Pt 9-17', 'Sob 9-14'),
(10, 12, 'Biała Podlaska', 'biala-podlaska-1', 'Ul.Prosta 4', NULL, 'shop-image-538c6aa3ed041.jpeg', '2013-12-20 09:43:29', NULL, 1, 'to change', 'to change', 'to change', 29, 'Pn-Pt 9-17', 'Sob 9-14'),
(11, 93, 'Bielsk Podlaski', 'bielsk-podlaski', 'Ul.Mickiewicza 38', NULL, 'shop-image-52b41c9e7a1f9.jpeg', '2013-12-20 09:46:24', NULL, 1, 'to change', 'to change', 'to change', 15, 'pn-pt 9-17', 'sobota 9-14'),
(13, 28, 'Biłgoraj', 'bilgoraj', 'Ul.Kościuszki 24', NULL, 'shop-image-52b41ef9c94ab.jpeg', '2013-12-20 09:58:07', NULL, 1, 'to change', 'to change', 'to change', 20, 'Pn-Pt 9-17', 'Sob 9-14'),
(14, 28, 'Biłgoraj', 'bilgoraj-1', 'Ul.Widok 4', NULL, 'shop-image-538307b3483df.jpeg', '2013-12-20 09:59:03', NULL, 1, 'to change', 'to change', 'to change', 12, 'Pn-Pt 9-17', 'Sob 9-14'),
(15, 11, 'Chełm', 'chelm', 'Ul.Lubelska 17', NULL, 'shop-image-52b42d20ccbf5.jpeg', '2013-12-20 10:00:19', NULL, 1, 'to change', 'to change', 'to change', 30, 'Pn-Pt 9-17', 'Sob 9-14'),
(16, 36, 'Ciechanowiec', 'ciechanowiec', 'Plac Ks.Kluka 3a', NULL, 'shop-image-530492f98313c.jpeg', '2013-12-20 10:01:13', NULL, 1, 'to change', 'to change', 'to change', 31, NULL, NULL),
(17, 34, 'Hajnówka', 'hajnowka', 'ul.Wierobieja 2', NULL, 'shop-image-543f908d97dbe.jpeg', '2013-12-20 10:02:31', NULL, 1, 'to change', 'to change', 'to change', 32, 'Pn-Pt 9-17', 'Sob 9-14'),
(18, 8, 'Hrubieszów', 'hrubieszow', 'ul.Narutowicza 54', NULL, 'shop-image-52b41fda3b6b4.jpeg', '2013-12-20 10:03:25', NULL, 1, 'to change', 'to change', 'to change', 33, 'pn-pt 9-17', 'sobota 9-14'),
(19, 94, 'Janów Lubelski', 'janow-lubelski', 'ul.Wesoła 5', NULL, 'shop-image-53830778f390e.jpeg', '2013-12-20 10:04:26', NULL, 1, 'to change', 'to change', 'to change', 21, 'pn-pt 9.00-17.00', 'sobota 9.00-14.00'),
(20, 10, 'Kozienice', 'kozienice', 'ul.Radomska 33', NULL, 'shop-image-540ea097c0e4c.jpeg', '2013-12-20 10:05:23', NULL, 1, 'to change', 'to change', 'to change', 19, 'Pn-Pt 9-17', 'Sob 9-14'),
(21, 7, 'Krasnystaw', 'krasnystaw', 'ul.Okszei 8', NULL, 'shop-image-52bd7eeb0fbce.jpeg', '2013-12-20 10:06:19', NULL, 1, 'to change', 'to change', 'to change', 34, 'pn-pt 9-17', 'sobota 9-14'),
(22, 15, 'Kraśnik', 'krasnik', 'ul.Urzędowska 36', NULL, 'shop-image-52b42da665052.jpeg', '2013-12-20 10:07:06', NULL, 1, 'to change', 'to change', 'to change', 35, NULL, NULL),
(24, 14, 'Lipsko', 'lipsko', 'ul.Zwoleńska 8 A', NULL, 'shop-image-55c49fc1618ca.jpeg', '2013-12-20 10:08:48', NULL, 1, 'to change', 'to change', 'to change', 37, 'Pn-Pt 9-17', 'Sob 9-14'),
(25, 4, 'Lubartów', 'lubartow', 'ul.Legionów 37', NULL, 'shop-image-52b42dde7daf6.jpeg', '2013-12-20 10:09:24', NULL, 1, 'to change', 'to change', 'to change', 38, NULL, NULL),
(26, 5, 'Lublin', 'lublin', 'ul.Wolska 11 a', NULL, 'shop-image-53ad1f12b1c8a.jpeg', '2013-12-20 10:15:35', NULL, 1, 'to change', 'to change', 'to change', 11, 'pn-pt 10-18', 'Soboty 9-14'),
(27, 37, 'Łapy', 'lapy', 'ul.Główna 1', NULL, 'shop-image-52b80772491b4.jpeg', '2013-12-20 10:17:58', NULL, 1, 'to change', 'to change', 'to change', 39, NULL, NULL),
(29, 3, 'Łosice', 'losice', 'ul.Rynek 3', NULL, 'shop-image-538d6b95bd85c.jpeg', '2013-12-20 10:19:19', NULL, 1, 'to change', 'to change', 'to change', 41, 'pn-pt 9-17', 'sobota 9-14'),
(31, 38, 'Międzyrzec Podlaski', 'miedzyrzec-podlaski', 'ul.Brzeska 23', NULL, 'shop-image-52b42af0583b6.jpeg', '2013-12-20 10:21:06', NULL, 1, 'to change', 'to change', 'to change', 43, 'pn-pt 9-17', 'sobota 9-14'),
(32, 38, 'Międzyrzec Podlaski', 'miedzyrzec-podlaski-1', 'ul.Lubelska', NULL, 'shop-image-52b42a977740c.jpeg', '2013-12-20 10:21:53', NULL, 1, 'to change', 'to change', 'to change', 80, 'pn-pt 9-17', 'sobota 9-14'),
(33, 1, 'Mińsk Mazowiecki', 'minsk-mazowiecki', 'ul.Warszawska 111', NULL, 'shop-image-538308018b439.jpeg', '2013-12-20 10:22:41', NULL, 1, 'to change', 'to change', 'to change', 7, 'Pn-Pt 9-17', 'Sob 9-14'),
(34, 2, 'Opole Lubelskie', 'opole-lubelskie', 'ul.Piekarska 4', NULL, 'shop-image-52b42c46e65ba.jpeg', '2013-12-20 10:23:20', NULL, 1, 'to change', 'to change', 'to change', 13, 'Pn-Pt 9-17', 'Sob 9-14'),
(36, 39, 'Ostrów Mazowiecka', 'ostrow-mazowiecka', 'ul.3 maja 9', NULL, 'shop-image-52b42e3c8c2ea.jpeg', '2013-12-20 10:24:38', NULL, 1, 'to change', 'to change', 'to change', 46, NULL, NULL),
(37, 26, 'Poniatowa', 'poniatowa', 'ul.Przechodnia 39', NULL, 'shop-image-540ea0748b2f2.jpeg', '2013-12-20 10:25:08', NULL, 1, 'to change', 'to change', 'to change', 22, 'Pn-Pt 9-17', 'Sob 9-14'),
(39, 24, 'Radom', 'radom', 'ul.Krauza 17a', NULL, 'shop-image-5535e83e588dc.jpeg', '2013-12-20 10:33:32', NULL, 1, 'to change', 'to change', 'to change', 47, NULL, NULL),
(40, 23, 'Radzyń Podlaski', 'radzyn-podlaski', 'ul.Chomiczewskiego 15', NULL, 'shop-image-52b428b7a186b.jpeg', '2013-12-20 10:34:20', NULL, 1, 'to change', 'to change', 'to change', 4, 'pn-pt 9-17', 'sobota  9-14'),
(41, 90, 'Radzyń Podlaski', 'radzyn-podlaski-1', 'ul.Pocztowa 4', NULL, 'shop-image-52b428ec95748.jpeg', '2013-12-20 10:35:11', NULL, 1, 'to change', 'to change', 'to change', 48, 'pn-pt 9-17', 'sobota 9-14'),
(42, 89, 'Radzyń Podlaski', 'radzyn-podlaski-2', 'ul.Warszawska 63-65', NULL, 'shop-image-538c6daa531c2.jpeg', '2013-12-20 10:35:57', NULL, 1, 'to change', 'to change', 'to change', 17, 'pn 9-17', 'sobota 9-14'),
(43, 40, 'Ryki', 'ryki', 'ul.Rynek Stary 1/5', NULL, 'shop-image-52b4202ec8b94.jpeg', '2013-12-20 10:37:19', NULL, 1, 'to change', 'to change', 'to change', 9, 'Pn-Pt 9-17', 'Sob 9-14'),
(45, 22, 'Siedlce', 'siedlce', 'ul.Wojskowa 18', NULL, 'shop-image-52b42be108d02.jpeg', '2013-12-20 10:43:09', NULL, 1, 'to change', 'to change', 'to change', 5, 'Pn-Pt 9-17', 'Sob 9-14'),
(46, 21, 'Siemiatycze', 'siemiatycze', 'Pl.Jana Pawła 14', NULL, 'shop-image-538d6db08e31e.jpeg', '2013-12-20 10:44:15', NULL, 1, 'to change', 'to change', 'to change', 18, 'Pn-Pt 9-17', 'Sob 9-14'),
(48, 43, 'Stoczek Łukowski', 'stoczek-lukowski', 'Pl.Kościuszki 2', NULL, 'shop-image-538307dae7379.jpeg', '2013-12-20 10:54:21', NULL, 1, 'to change', 'to change', 'to change', 14, 'Pn-Pt 9-17', 'Sob 9-14'),
(50, 97, 'Tomaszów Lubelski', 'tomaszow-lubelski', 'ul.Lwowska 19', NULL, 'shop-image-52b41ff3a04bc.jpeg', '2013-12-20 11:00:13', NULL, 1, 'to change', 'to change', 'to change', 52, 'Pn-Pt 9-17', 'Sob 9-14'),
(51, 96, 'Tomaszów Lubelski', 'tomaszow-lubelski-1', 'ul.Lwowska 56', NULL, 'shop-image-52b42b6b2263f.jpeg', '2013-12-20 11:01:13', NULL, 1, 'to change', 'to change', 'to change', 53, 'Pn-Pt 9-17', 'Sob 9-14'),
(52, 95, 'Tomaszów Lubelski', 'tomaszow-lubelski-2', 'ul.Lwowska 14', NULL, 'shop-image-52b4200d90489.jpeg', '2013-12-20 11:02:07', NULL, 1, 'to change', 'to change', 'to change', 25, 'Pn-pt 9-17', 'Sob 9-14'),
(54, 88, 'Włodawa', 'wlodawa-1', 'ul.Czerwonego Krzyża 14a', NULL, 'shop-image-538d70dfbb209.jpeg', '2013-12-20 11:03:48', NULL, 1, 'to change', 'to change', 'to change', 26, 'Pn-Pt 9-17', 'Sob 9-14'),
(55, 45, 'Wysokie Mazowieckie', 'wysokie-mazowieckie', 'ul.Jagielońska 35', NULL, 'shop-image-538d737154a7d.jpeg', '2013-12-20 11:06:02', NULL, 1, 'to change', 'to change', 'to change', 55, 'Pn-Pt 9-17', 'Sob 9-14'),
(57, 18, 'Węgrów', 'wegrow', 'Węgrów ul.Rynkowa 3', NULL, 'shop-image-52b8065d7d338.jpeg', '2013-12-23 10:45:21', NULL, 1, 'to change', 'to change', 'to change', 54, NULL, NULL),
(58, 15, 'Kraśnik', 'krasnik-1', 'ul.Lubelska 46', NULL, 'shop-image-52c67362be98e.jpeg', '2014-01-03 09:19:08', NULL, 1, 'to change', 'to change', 'to change', 56, NULL, NULL),
(59, 87, 'Siedlce', 'siedlce-1', 'ul.Pułaskiego 10', NULL, 'shop-image-53830753b4af4.jpeg', '2014-01-21 14:50:29', NULL, 1, 'to change', 'to change', 'to change', 57, 'Pn-Pt 9-17', 'Sob 9-14'),
(60, 47, 'Starachowice', 'starachowice', 'ul.Niska 6', NULL, 'shop-image-532167b0ec62f.jpeg', '2014-02-14 13:56:30', NULL, 1, 'to change', 'to change', 'to change', 59, NULL, NULL),
(62, 42, 'Sokołów Podlaski', 'sokolow-podlaski-1', 'ul.Magistracka 1', NULL, 'shop-image-538d95c6675f4.jpeg', '2014-03-13 09:16:57', NULL, 1, 'to change', 'to change', 'to change', 50, 'Pn-Pt 9-17', 'Sob 9-14'),
(63, 85, 'Hrubieszów', 'hrubieszow-1', 'ul.Ludna 9', NULL, 'shop-image-53a12f6bb562d.jpeg', '2014-06-02 11:27:04', NULL, 1, 'to change', 'to change', 'to change', 62, 'pn-pt 9-17', 'sobota 9-14'),
(64, 52, 'Skarżysko Kamienna', 'skarzysko-kamienna', 'ul.Krasińskiego 20', NULL, 'shop-image-53c4cef55e047.jpeg', '2014-06-02 11:31:32', NULL, 1, 'to change', 'to change', 'to change', 60, NULL, NULL),
(65, 84, 'Brańsk', 'bransk', 'ul.Rynek 43', NULL, 'shop-image-53ccb55eb7e9c.jpeg', '2014-07-15 08:11:34', NULL, 1, 'to change', 'to change', 'to change', 63, 'Pn-Pt 8-16', 'Sob 9-14'),
(66, 53, 'Terespol', 'terespol', 'ul.Wojska Polskiego 134', NULL, 'shop-image-54080bc53e8ba.jpeg', '2014-09-04 08:26:24', NULL, 1, 'to change', 'to change', 'to change', 65, 'pn-pt 9-17', 'sobota 9-14'),
(67, 83, 'Łuków', 'lukow-1', 'Staropijarska 20', NULL, 'shop-image-542e47ea8ef3c.jpeg', '2014-09-23 08:46:13', NULL, 1, 'to change', 'to change', 'to change', 66, 'pn-pt 10-18', 'sobota 9-14'),
(68, 16, 'Ostrowiec Świętokrzyski', 'ostrowiec-swietokrzyski-1', 'ul.Górzysta 6', NULL, 'shop-image-55b77ae804a05.jpeg', '2015-01-23 11:39:27', NULL, 1, 'to change', 'to change', 'to change', 67, 'Pn-Pt 9-17', 'Sob 9-14'),
(70, 55, 'Kielce', 'kielce', 'ul.Sienkiewicza 68', NULL, 'shop-image-5535e85ef1625.jpeg', '2015-02-24 14:18:54', NULL, 1, 'to change', 'to change', 'to change', 69, 'pn-pt 10.00-18.00', 'sobota 10.00-15.00'),
(71, 25, 'Puławy', 'pulawy-1', 'ul.Piłsudskiego 24', NULL, 'shop-image-55b7782785881.jpeg', '2015-06-03 12:24:47', NULL, 1, 'to change', 'to change', 'to change', 70, 'Pn-Pt 9-17', 'Sob 9-14'),
(73, 56, 'Garwolin', 'garwolin', 'ul.Kościuszki 1', NULL, 'shop-image-55c49d9a03e58.jpeg', '2015-07-22 11:29:46', NULL, 1, 'to change', 'to change', 'to change', 71, 'Pn-Pt 9-17', 'Sob 9-14'),
(74, 57, 'Łochów', 'lochow', 'ul.1 Maja 2 pietro 1', NULL, 'shop-image-5613d06d9a321.jpeg', '2015-10-06 15:35:48', NULL, 1, 'to change', 'to change', 'to change', 72, 'Pn-Pt 9-17', 'Sob 9-14'),
(75, 98, 'Lublin', 'lublin-1', 'ul.Zamojska 12', NULL, 'shop-image-56695aa1e6e86.jpeg', '2015-10-06 15:56:09', NULL, 1, 'to change', 'to change', 'to change', 73, 'Pn-Pt 9-17', 'Sob 9-14'),
(76, 24, 'Radom', 'radom-1', 'ul.Słowackiego 19', NULL, 'shop-image-569c926c7445e.jpeg', '2015-10-09 08:41:43', NULL, 1, 'to change', 'to change', 'to change', 74, 'pn-pt 9-17', 'sobota 9-14'),
(77, 81, 'Łuków', 'lukow-2', 'ul.Międzyrzecka 6 a', NULL, 'shop-image-5630a611a5dce.jpeg', '2015-10-09 08:44:36', NULL, 1, 'to change', 'to change', 'to change', 75, 'pn-pt 9-17', 'sobota 9-14'),
(78, 58, 'Parczew', 'parczew', 'ul.Kościelna 54 A', NULL, 'shop-image-563a1b51bd857.jpeg', '2015-10-30 11:43:54', NULL, 1, 'to change', 'to change', 'to change', 76, 'pn-pt 9-17', 'sobota 9-14'),
(79, 15, 'Kraśnik', 'krasnik-2', 'Aleja Niepodległości 25', NULL, 'shop-image-564ed04ae16b0.jpeg', '2015-11-20 08:48:26', NULL, 1, 'to change', 'to change', 'to change', 77, 'pn-pt 9-17', 'sobota 9-14'),
(80, 59, 'Sandomierz', 'sandomierz', 'ul.Mickiewicza 46 b', NULL, 'shop-image-566171e60a0d6.jpeg', '2015-11-20 09:38:23', NULL, 1, 'to change', 'to change', 'to change', 78, 'pn-pt 9-17', 'sobota 9-14'),
(81, 80, 'Dęblin', 'deblin', 'ul.Warszawska 34', NULL, 'shop-image-566171bdcf4db.jpeg', '2015-12-04 11:56:31', NULL, 1, 'to change', 'to change', 'to change', 79, 'Pn-Pt 9-17', 'Sob 9-14'),
(84, 79, 'Gołdap', 'goldap', 'ul. Królewiecka 11', NULL, NULL, '2017-04-05 15:11:02', NULL, 1, 'to change', 'to change', 'to change', NULL, 'pn-pt 9-17', 'sobota 9-14'),
(85, 78, 'Kętrzyn', 'ketrzyn', 'ul.Kołobrzeska 1b/1', NULL, NULL, '2017-04-05 15:14:36', NULL, 1, 'to change', 'to change', 'to change', NULL, 'pn-pt 9-17', 'sobota 9-14'),
(86, 77, 'Białobrzegi', 'bialobrzegi', 'ul.Królewiecka', NULL, NULL, '2017-04-05 15:18:39', NULL, 1, 'to change', 'to change', 'to change', NULL, 'pn-pt 9-17', 'sobota 9-14'),
(87, 65, 'Suwałki', 'suwalki', 'ul.Noniewicza 57', NULL, NULL, '2017-04-05 15:21:03', NULL, 1, NULL, NULL, NULL, NULL, 'pn-pt 9-17', 'sobota 9-14'),
(88, 66, 'Olecko', 'olecko', 'ul.Plac Wolności 11', NULL, NULL, '2017-04-05 15:23:24', NULL, 1, 'to change', 'to change', 'to change', NULL, 'pn-pt 9-17', 'sobota 9-14'),
(89, 67, 'Augustów', 'augustow', 'ul.Hoża 11c', NULL, NULL, '2017-04-05 15:25:29', NULL, 1, 'to change', 'to change', 'to change', NULL, 'pn-pt 9-17', 'sobota 9-14'),
(90, 68, 'Zamość', 'zamosc', 'ul.Gminna 40', NULL, NULL, '2017-04-05 15:27:59', NULL, 1, 'to change', 'to change', 'to change', NULL, 'pn-pt 9-17', 'sobota 9-14'),
(91, 69, 'Chełm', 'chelm-1', 'ul. Lwowska 18c', NULL, NULL, '2017-04-05 15:30:15', NULL, 1, 'to change', 'to change', 'to change', NULL, 'pn-pt 9-17', 'sobota 9-14'),
(92, 70, 'Stalowa Wola', 'stalowa-wola', 'ul. Jana Pawła II 10', NULL, NULL, '2017-04-05 15:32:32', NULL, 1, 'to change', 'to change', 'to change', NULL, 'Pn-Pt 9-17', 'Sob 9-14'),
(93, 71, 'Ełk', 'elk', 'ul. Słowackiego 23/1', NULL, NULL, '2017-04-05 15:34:00', NULL, 1, 'to change', 'to change', 'to change', NULL, 'pn-pt 9-17', 'sobota 9-14'),
(94, 76, 'Mrągowo', 'mragowo', 'ul.Warszawska 6', NULL, NULL, '2017-04-05 15:36:56', NULL, 1, 'to change', 'to change', 'to change', NULL, 'pn-pt 9-17', 'sobota 9-14'),
(95, 75, 'Kock', 'kock-1', 'ul. Piłsudskiego 15/17', NULL, NULL, '2017-05-15 15:24:43', NULL, 1, 'to change', 'to change', 'to change', NULL, 'pn-pt 8-18', 'sob 9-14');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `slider`
--

INSERT INTO `slider` (`id`, `title`, `url`, `content`, `photo`, `created_at`, `updated_at`) VALUES
(4, 'Narzędzia  kuchenne', '/kategorie/kuchnia', '<div>\r\n	Wyposażenie kuchni- akcesoria kuchenne, garnki, patelnie, noże i wiele innych. Szeroki wyb&oacute;r produkt&oacute;w do kuchni.</div>', 'slider-image-58cac4602cc04.jpeg', '2017-02-17 10:18:54', NULL),
(5, 'Szkło', '/kategorie/szklo', '<div>\r\n	Niezbędne w każdym domu. Tylko u nas w tak atrakcyjnej cenie. Zapraszamy do zapoznania się&nbsp;</div>\r\n<div>\r\n	z naszą ofertą!&nbsp;</div>', 'slider-image-58cac4c5cdd58.jpeg', '2017-02-17 10:19:15', NULL),
(6, 'Kwiaty', '/kategorie/kwiaty', '<div>\r\n	W naszej ofercie znajdziesz kwiaty sztuczne, gałązki dekoracyjne oraz wiele innych dodatk&oacute;w do tworzenia kompozycji, wieńc&oacute;w i wiązanek kwiatowych.</div>', 'slider-image-58db6dfeaefeb.jpeg', '2017-03-06 09:15:25', NULL),
(7, 'Artykuły dekoracyjne', '/kategorie/artykuly-dekoracyjne', '<div>\r\n	Te wszystkie przedmioty tworzą niezwykłą atmosferę w domu. Artykuły derkoracyjne to r&oacute;wnież doskonały pomysł na prezent.</div>', 'slider-image-58cac4ff208e3.jpeg', '2017-03-06 09:15:51', NULL),
(8, 'Zabawki', '/kategorie/zabawki', '<div>\r\n	Szeroki wyb&oacute;r zabawek dla dziewczynek i chłopc&oacute;w w r&oacute;żnym wieku.</div>', 'slider-image-58cac66a17c47.jpeg', '2017-03-06 09:16:08', NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `acl_classes`
--
ALTER TABLE `acl_classes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Indexes for table `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  ADD KEY `IDX_46C8B806EA000B10` (`class_id`),
  ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Indexes for table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Indexes for table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Indexes for table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Indexes for table `attribute`
--
ALTER TABLE `attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_form_message`
--
ALTER TABLE `contact_form_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1FC0F36A4E7AF8F` (`gallery_id`);

--
-- Indexes for table `fos_group`
--
ALTER TABLE `fos_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_4B019DDB5E237E06` (`name`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`);

--
-- Indexes for table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `IDX_B3C77447A76ED395` (`user_id`),
  ADD KEY `IDX_B3C77447FE54D947` (`group_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F02A543B4E7AF8F` (`gallery_id`);

--
-- Indexes for table `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D754D550727ACA70` (`parent_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1DD399504E7AF8F` (`gallery_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_2074E5754E7AF8F` (`gallery_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D34A04AD12469DE2` (`category_id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`attribute_id`,`product_id`),
  ADD KEY `IDX_A2FCC15BB6E62EFA` (`attribute_id`),
  ADD KEY `IDX_A2FCC15B4584665A` (`product_id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_E545A0C5EA750E8` (`label`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_AC6A4CA28BAC62AF` (`city_id`),
  ADD KEY `IDX_AC6A4CA24E7AF8F` (`gallery_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `acl_classes`
--
ALTER TABLE `acl_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `acl_entries`
--
ALTER TABLE `acl_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `attribute`
--
ALTER TABLE `attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT dla tabeli `contact_form_message`
--
ALTER TABLE `contact_form_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT dla tabeli `doctor`
--
ALTER TABLE `doctor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `fos_group`
--
ALTER TABLE `fos_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=692;
--
-- AUTO_INCREMENT dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT dla tabeli `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2204;
--
-- AUTO_INCREMENT dla tabeli `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT dla tabeli `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT dla tabeli `shop`
--
ALTER TABLE `shop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT dla tabeli `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Ograniczenia dla tabeli `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `doctor`
--
ALTER TABLE `doctor`
  ADD CONSTRAINT `FK_1FC0F36A4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_group` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD CONSTRAINT `FK_F02A543B4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D550727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_item` (`id`);

--
-- Ograniczenia dla tabeli `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_1DD399504E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `FK_2074E5754E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `product_category` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD CONSTRAINT `FK_A2FCC15B4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_A2FCC15BB6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `attribute` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `shop`
--
ALTER TABLE `shop`
  ADD CONSTRAINT `FK_AC6A4CA24E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_AC6A4CA28BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
