<?php

namespace Ls\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AttributesType extends AbstractType {

    public function __construct($attributes) 
    {
        $this->attributes = $attributes;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('attributes', 'choice', array(
                    'choices' => $this->attributes,
                    'label' => false,
                    'multiple' => true,
                    'expanded' => true,
                    'attr' => array(
                        'id' => 'attributes-select'
                    ),
                    'required' => false,
                ))
                ->add('submit', 'submit', array(
                    'attr' => array(
                        'class' => 'hidden'
                    )
                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $collectionConstraint = new Collection(array());

        $resolver->setDefaults(array(
            'constraints' => $collectionConstraint
        ));
    }

    public function getName() {
        return 'lscms_bundle';
    }

}
