<?php

namespace Ls\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;

class ContactType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('companyName', 'text', array(
                    'label' => 'Nazwa firmy',
                    'attr' => array(),
                    'required' => false
                ))
                ->add('fullName', 'text', array(
                    'label' => 'Imię i nazwisko',
                    'attr' => array()
                ))
                ->add('email', 'text', array(
                    'label' => 'Adres E-mail',
                    'attr' => array(),
                    'required' => true
                ))
                ->add('phone', 'text', array(
                    'label' => 'Telefon',
                    'attr' => array(),
                    'required' => true
                ))
                ->add('message', 'textarea', array(
                    'label' => 'Treść',
                    'attr' => array(),
                    'required' => true
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $collectionConstraint = new Collection(array(
            'companyName' => array(),
            'fullName' => array(
                new NotBlank(array('message' => 'Podaj imię i nazwisko.'))
            ),
            'phone' => array(),
            'email' => array(),
            'message' => array()
        ));

        $resolver->setDefaults(array(
            'constraints' => $collectionConstraint
        ));
    }

    public function getName() {
        return 'ls_cmsbundle_contact';
    }

}
