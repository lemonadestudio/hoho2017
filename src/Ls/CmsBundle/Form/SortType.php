<?php

namespace Ls\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;

// Co za typ!!
class SortType extends AbstractType {

    public function __construct($selected) 
    {
        if ($selected != '') {
            $this->selected = $selected;
        } else {
            $this->selected = 'createdAt_asc';
        }
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('condition', 'choice', array(
                    'label' => 'Sortowanie:',
                    'attr' => array(
                        'placeholder' => 'Sortowanie',
                        'id' => 'sort-select'
                    ),
                    'required' => false,
                    'choices' => [
                        'price_desc' => 'Ceny od najwyższej',
                        'price_asc' => 'Ceny od najniższej',
                        'name_asc' => 'Alfabetycznie rosnąco',
                        'name_desc' => 'Alfabetycznie malejąco',
                        'createdAt_asc' => 'Data dodania rosnąco',
                        'createdAt_desc' => 'Data dodania malejąco',
                    ],
                    'data' => $this->selected,
                ))
                ->add('submit', 'submit', array(
                    'attr' => array(
                        'class' => 'hidden'
                    )
                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $collectionConstraint = new Collection(array());

        $resolver->setDefaults(array(
            'constraints' => $collectionConstraint
        ));
    }

    public function getName() {
        return 'ls_cmsbundle_sort';
    }

}
