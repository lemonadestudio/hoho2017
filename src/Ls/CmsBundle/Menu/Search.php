<?php

namespace Ls\CmsBundle\Menu;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Search {

    private $container;
    private $method;

    public function __construct(ContainerInterface $container, $method = '') {
        $this->container = $container;
        $this->method = $method;
    }

//    For another, not working categories, you must add another case.
    public function search($search = '') {
        switch ($this->method) {
            case 'aktualnosc' :
                return $this->aktualnosc();

            case 'galeria' :
                return $this->galeria();

            case 'podstrona' :
                return $this->podstrona();
                
            case 'productCategories' :
                return $this->productCategories();
                
            case 'cities' :
                return $this->cities();

            default:
                break;
        }

        return array();
    }

    public function aktualnosc($search = '') {

        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsCmsBundle:News', 's')
                ->where('s.title LIKE :search')
                ->orderBy('s.title', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();

        $ret_arr = array();
        foreach ($items as $item) {
            $ret_arr[] = array(
                'parameters' => array('slug' => $item->getSlug()),
                'title' => $item->getTitle()
            );
        }

        return $ret_arr;
    }

    public function galeria($search = '') {

        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsCmsBundle:Gallery', 's')
                ->where('s.title LIKE :search')
                ->orderBy('s.title', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();

        $ret_arr = array();
        foreach ($items as $item) {
            $ret_arr[] = array(
                'parameters' => array('slug' => $item->getSlug()),
                'title' => $item->getTitle()
            );
        }

        return $ret_arr;
    }

    public function podstrona($search = '') {

        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsCmsBundle:Pages', 's')
                ->where('s.title LIKE :search')
                ->orderBy('s.title', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();

        $ret_arr = array();
        foreach ($items as $item) {
            $ret_arr[] = array(
                'parameters' => array('slug' => $item->getSlug()),
                'title' => $item->getTitle()
            );
        }

        return $ret_arr;
    }

    public function productCategories($search = '') {

        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsCmsBundle:ProductCategory', 's')
                ->where('s.name LIKE :search')
                ->orderBy('s.name', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();

        $ret_arr = array();
        foreach ($items as $item) {
            $ret_arr[] = array(
                'parameters' => array('slug' => $item->getSlug()),
                'title' => $item->getName()
            );
        }

        return $ret_arr;
    }

    public function cities($search = '') {

        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsCmsBundle:City', 's')
                ->where('s.name LIKE :search')
                ->orderBy('s.name', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();

        $ret_arr = array();
        foreach ($items as $item) {
            $ret_arr[] = array(
                'parameters' => array('citySlug' => $item->getSlug()),
                'title' => $item->getName()
            );
        }

        return $ret_arr;
    }

}
