<?php

// src/Ls/CmsBundle/Menu/Builder.php

namespace Ls\CmsBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Ls\CmsBundle\Entity\MenuItem;

class Builder extends ContainerAware {

    public function managedMenu(FactoryInterface $factory, array $options) {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'menu_level_0');
        $menu->setCurrentUri($this->container->get('request')->getRequestUri());

        $em = $this->container->get('doctrine')->getManager();
        $menu_items = $em->createQueryBuilder()
                ->select('m')
                ->from('LsCmsBundle:MenuItem', 'm')
                ->where('m.location = :location')
                ->andWhere('m.parent is NULL')
                ->orderBy('m.arrangement', 'ASC')
                ->setParameter('location', $options['location'])
                ->getQuery()
                ->getResult();


        foreach ($menu_items as $item) {
            $child = $this->buildMenu($item, $menu);
            $children = $item->getChildren();
            if (count($children) > 0) {
                foreach ($children as $subitem) {
                    $subchild = $this->buildMenu($subitem, $child);
                    $subchildren = $subitem->getChildren();
                    if (count($subchildren) > 0) {
                        foreach ($subchildren as $subsubitem) {
                            $this->buildMenu($subsubitem, $subchild);
                        }
                    }
                }
            }
        }

        return $menu;
    }

    /**
     *
     * @param MenuItem $item
     * @param  ItemInterface $menu
     */
    public function checkItem($item) {
        $item_arr = $this->getMenuItemArray($item);

        if (!$item_arr) {
            return false;
        }

        return true;
    }

    public function buildMenu($item, &$menu) {
        $item_arr = $this->getMenuItemArray($item);

        if (!$item_arr) {
            return;
        }

        $option = array();
        $option['uri'] = $item_arr['uri'];
        $option['linkAttributes'] = array_merge(array('title' => $item_arr['title']), $item_arr['linkAttributes']);
        if (count($item_arr['itemAttributes']) > 0) {
            $option['attributes'] = $item_arr['itemAttributes'];
        }

        return $menu->addChild($item_arr['title'], $option);
    }

    public function getMenuItemArray($item) {
        $router = $this->container->get('router');
        if (!$item->getUrl() && !$item->getRoute() && !$item->getOnclick()) {
            return false;
        }

        $uri = false;
        $itemAttributes = array();
        $linkAttributes = array();
        switch ($item->getType()) {
            case 'url':
                $uri = $item->getUrl();
                if ($item instanceof MenuItem) {
                    if (count($item->getChildren()) > 0) {
                        $linkAttributes['class'] = 'has-submenu';
                        $itemAttributes['class'] = 'has-submenu';
                    }
                }
                break;

            case 'route':
                if ($item->getRoute()) {
                    $route = $router->getRouteCollection()->get($item->getRoute());
                    if ($route) {
                        $parameters = array();
                        foreach ($item->getRouteParametersArray() as $k => $param) {
                            if (preg_match('@\{' . $k . '\}@', $route->getPath())) {
                                $parameters[$k] = $param;
                            }
                        }

                        try {
                            $uri = $router->generate($item->getRoute(), $parameters);
                        } catch (\Exception $e) {
                            // return false;
                        }
                    }
                }
                $class = array();
                if ($item instanceof MenuItem) {
                    if (count($item->getChildren()) > 0) {
                        $class[] = 'has-submenu';
                    }
                    if ($item->getRoute() == 'lscms_homepage') {
                        $class[] = 'home';
                    }
                }
                if(count($class > 0)) {
                    $itemAttributes['class'] = implode(' ', $class);
                }
                break;

            case 'button':
                $uri = '#';
                if ($item instanceof MenuItem) {
                    if (count($item->getChildren()) > 0) {
                        $linkAttributes['class'] = 'has-submenu';
                        $itemAttributes['class'] = 'has-submenu';
                    }
                }
                $linkAttributes['onclick'] = 'return ' . $item->getOnClick() . ';';
                break;

            default:
                break;
        }

        if ($uri === false) {
            return false;
        }

        return array(
            'title' => $item->getTitle(),
            'uri' => $uri,
            'linkAttributes' => $linkAttributes,
            'itemAttributes' => $itemAttributes,
        );
    }

}
