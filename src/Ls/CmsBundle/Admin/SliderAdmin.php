<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SliderAdmin extends Admin {

    // setup the default sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'published_at'
    );

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $helps = array();
        $formMapper
                ->with('Treść')
                ->add('title', null, array('label' => 'Tytuł', 'required' => true))
                ->add('url', null, array('label' => 'Url'))
                ->add('content', 'textarea', array('label' => 'Treść', 'attr' => array('class' => 'wysiwyg'), 'required' => false))
                ->with('Zdjęcie')
                ->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => false))
        ;
        $formMapper->setHelps($helps);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('title', null, array('label' => 'Tytuł'))
                ->add('content', null, array('label' => 'Treść'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('title', null, array('label' => 'Tytuł'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\Slider:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('slider-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('slider-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function saveFile($entity) {
        $entity->upload();
    }

}
