<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PopupAdmin extends Admin {
	protected $datagridValues = array(
			'_page'       => 1,
			'_sort_order' => 'ASC', // sort direction
			'_sort_by' => 'url' // field name
	);

	protected function configureFormFields(FormMapper $formMapper) {
        $helps = array();

        $formMapper->add('cookieId', "integer", array('label' => 'Identifikator ciastka:', 'required' => true));
        $helps['cookieId'] = "Zwiększyć numer jeżeli następuje zmiana popup'a lub linku (aby pokazało się osobom które zamknęły poprzedni popup)";

        $formMapper->add('url', null, array('label' => 'Link:', 'required' => false));
        $helps['url'] = "link pusty = popup się nie wyświetla";

        $formMapper->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => false));

        $formMapper->setHelps($helps);
    }

	public function getTemplate($name){
	    if($name == 'list') {
	        return 'LsCmsBundle:Admin\Popup:main.html.twig';
	    }
	    return parent::getTemplate($name);
	}


	protected function configureRoutes(RouteCollection $collection) {

			    $collection->remove('create');
// 				$collection->remove('edit');
// 				$collection->remove('batch');
				$collection->remove('show');
				$collection->remove('delete');

	}

	public function prePersist($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('popup-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('popup-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function saveFile($entity) {
        $entity->upload();
    }

}