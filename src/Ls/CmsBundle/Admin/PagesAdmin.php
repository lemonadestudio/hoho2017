<?php

// src/Ls/CmsBundle/Admin/PagesAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PagesAdmin extends Admin {

    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'title'
    );

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $em = $this->modelManager->getEntityManager('Ls\CmsBundle\Entity\Gallery');
        $query = $em->createQueryBuilder()
                ->select('g')
                ->from('LsCmsBundle:Gallery', 'g')
                ->where('g.attachable = 1')
                ->getQuery();
        $helps = array();
        $formMapper
                ->add('title', null, array('label' => 'Tytuł', 'required' => true));
        if ($this->getRoot()->getSubject()->getId() > 0) {
            $formMapper->add('slug', null, array('label' => 'Końcówka adresu URL', 'required' => false));
            $helps['slug'] = 'Element linku URL identyfikujący podstronę np. http://www.test.pl/<b>podstrona</b>';
        }
        $formMapper
                ->add('content_short_generate', null, array('label' => 'Automatycznie generuj krótką treść'))
                ->add('content_short', 'textarea', array('label' => 'Krótka treść', 'required' => false, 'attr' => array('rows' => 5)))
                ->add('content', null, array('label' => 'Treść', 'attr' => array('class' => 'wysiwyg')))
        ;
        $formMapper->setHelps($helps);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('title', null, array('label' => 'Tytuł'))
                ->add('content', null, array('label' => 'Treść'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('title', null, array('label' => 'Tytuł'))
                ->add('gallery', null, array('label' => 'Galeria', 'template' => 'LsCmsBundle:Admin\Pages:list_gallery.html.twig'))
                ->add('content_short', null, array('label' => 'Krótka treść'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\Pages:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('pages-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('pages-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function saveFile($entity) {
        $entity->upload();
    }

}
