<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CityAdmin extends Admin {

    // setup the default sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureRoutes(RouteCollection $collection) {
        
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $helps = array();
        $formMapper
            ->with('Pozycja na mapie')
            ->add('mapPositionFromTop', null, array('label' => 'Długość geograficzna', 'required' => false, 'attr'=> array('class' => 'admin_lat') ))
            ->add('mapPositionFromLeft', null, array('label' => 'Szerokość geograficzna', 'required' => false, 'attr' => array('class' => 'admin_long') ));
        $formMapper
                ->with('Treść')
                ->add('name', null, array('label' => 'Nazwa', 'required' => true))
                ->add('postal_code', null, array('label' => 'Kod pocztowy', 'required' => false, ));
        
        if ($this->getRoot()->getSubject()->getId() > 0) {
            $formMapper->add('slug', null, array('label' => 'Końcówka adresu URL', 'required' => false));
            $helps['slug'] = 'Element linku URL identyfikujący miejscowość np. http://www.test.pl/miasta/<b>miasto</b>';
        }
        
        
        
        $formMapper->setHelps($helps);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name', null, array('label' => 'Nazwa'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('name', null, array('label' => 'Nazwa'))
                ->add('createdAt', null, array('label' => 'Data utworzenia'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\City:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

}
