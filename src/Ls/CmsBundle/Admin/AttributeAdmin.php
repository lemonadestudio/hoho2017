<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AttributeAdmin extends Admin {

    // setup the default sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureFormFields(FormMapper $formMapper) {
        $em = $this->modelManager->getEntityManager('Ls\CmsBundle\Entity\Attribute');
        $query = $em->createQueryBuilder()
                ->select('c')
                ->from('LsCmsBundle:Attribute', 'c')
                ->getQuery();

        $helps = array();
        $formMapper
                ->with('Treść')
                ->add('name', null, array('label' => 'Nazwa', 'required' => true));
        
        if ($this->getRoot()->getSubject()->getId() > 0) {
            $formMapper->add('slug', null, array('label' => 'Końcówka adresu URL', 'required' => false));
            $helps['slug'] = 'Element identyfikujący atrybut. Domyślnie generowany automatycznie';
        }
        
        $formMapper->setHelps($helps);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name', null, array('label' => 'Nazwa'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('name', null, array('label' => 'Nazwa'))
                ->add('slug', null, array('label' => 'Slug'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\Attribute:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

}
