<?php

// src/Ls/CmsBundle/Admin/GalleryPhotoAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class GalleryPhotoAdmin extends Admin {

    protected $parentAssociationMapping = 'gallery';

    public function createQuery($context = 'list') {
        $query = parent::createQuery($context);

        $query->orderBy('o.gallery', 'ASC');
        $query->addOrderBy('o.arrangement', 'ASC');

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('moveup', $this->getRouterIdParameter() . '/moveup');
        $collection->add('movedown', $this->getRouterIdParameter() . '/movedown');
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
        $collection->add('addmany', 'addmany');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('Ogólne')
                ->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => true))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('gallery', null, array('label' => 'Galeria'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('gallery', null, array('label' => 'Galeria'))
                ->add('image', null, array('label' => 'Zdjęcie', 'template' => 'LsCmsBundle:Admin\GalleryPhoto:list_photo.html.twig'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'movedown' => array(),
                        'moveup' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\GalleryPhoto:edit.html.twig';
                break;

            case 'list':
                return 'LsCmsBundle:Admin\GalleryPhoto:list.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('gallery-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('gallery-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function saveFile($entity) {
        $entity->upload();
    }

}
