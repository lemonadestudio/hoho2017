<?php

// src/Ls/CmsBundle/Admin/SettingsAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SettingsAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('Ogólne')
                ->add('label', null, array('label' => 'Etykieta'))
                ->add('description', null, array('label' => 'Opis', 'required' => false))
                ->add('value', null, array('label' => 'Wartość', 'required' => false))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('label', null, array('label' => 'Etykieta'))
                ->add('value', null, array('label' => 'Wartość'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('description', null, array('label' => 'Opis'))
                ->add('value', null, array('label' => 'Wartość', 'template' => 'LsCmsBundle:Admin\Settings:list_value.html.twig'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }
}
