<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ShopAdmin extends Admin {

    // setup the default sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $em = $this->modelManager->getEntityManager('Ls\CmsBundle\Entity\City');
        
        $galleriesQuery = $em->createQueryBuilder()
                ->select('g')
                ->from('LsCmsBundle:Gallery', 'g')
                ->where('g.attachable = 1')
                ->getQuery();
        
        $citiesQuery = $em->createQueryBuilder()
                ->select('c')
                ->from('LsCmsBundle:City', 'c')
                ->getQuery();

        $helps = array();
        $formMapper
                ->with('Treść')
                ->add('name', null, array('label' => 'Nazwa', 'required' => true));
        
        if ($this->getRoot()->getSubject()->getId() > 0) {
            $formMapper->add('slug', null, array('label' => 'Końcówka adresu URL', 'required' => false));
            $helps['slug'] = 'Element linku URL identyfikujący sklep np. http://www.test.pl/sklepy/<b>sklep</b>';
        }
        
        $formMapper
                ->add('address', null, array('label' => 'Adres', 'required' => false))
                
                // to sa godziny otwarcia, moze sie zmienic znow na telefon takze nazwa zmiennej zostaje taka
                ->add('phone', null, array('label' => 'Telefon', 'required' => false))
                // Następuje zmiana, dwa dodatkowe pola odpowiedzialne za godziny otwarcia, co jest wymuszone zmianami we froncie. 21.02.2017 - T.Jacek
                ->add('weekHours', null, array('label' => 'Godziny otwarcia w tygodniu', 'required' => false))
                ->add('weekendHours', null, array('label' => 'Godziny otwarcia w weekend', 'required' => false))
                
                ->add('city', 'sonata_type_model', array('query' => $citiesQuery, 'label' => 'Miejscowość', 'required' => false))
                ->add('gallery', 'sonata_type_model', array('query' => $galleriesQuery, 'label' => 'Galeria', 'required' => false))
                ->with('Zdjęcie')
                ->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => false))
        ;
        $formMapper->setHelps($helps);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name', null, array('label' => 'Nazwa'))
                ->add('city', null, array('label' => 'Miejscowość'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('name', null, array('label' => 'Nazwa'))
                ->add('city', null, array('label' => 'Miejscowość'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\Shop:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('shop-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('shop-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function saveFile($entity) {
        $entity->upload();
    }

}
