<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ContactFormMessageAdmin extends Admin {

    // setup the default sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id'
    );

    protected function configureRoutes(RouteCollection $collection) {
        $collection->remove('create');
        $collection->remove('edit');
// 				$collection->remove('batch');
// 				$collection->remove('show');
// 				$collection->remove('delete');
    }

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('sentAt', null, array('label' => 'Data wysłania'))
                ->add('senderName', null, array('label' => 'Wysyłający'))
                ->add('senderEmail', null, array('label' => 'Adres e-mail wysyłającego'))
                ->add('senderMessage', null, array('label' => 'Wiadomość', 'template' => 'LsCmsBundle:Admin/ContactFormMessage:message.html.twig'));
//                ->add('senderMessage', null, array('template' => 'XDCmsBundle:CRUD:contactformmessage_message.html.twig'));
    }
    
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

    public function configureFormFields(FormMapper $formMapper)
    {
    }

}
