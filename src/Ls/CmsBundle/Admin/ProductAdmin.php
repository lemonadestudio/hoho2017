<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProductAdmin extends Admin {

    // setup the default sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
        $collection->add('generateAllegroTemplate', $this->getRouterIdParameter() . '/generateAllegroTemplate');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $em = $this->modelManager->getEntityManager('Ls\CmsBundle\Entity\ProductCategory');
        $query = $em->createQueryBuilder()
                ->select('c')
                ->from('LsCmsBundle:ProductCategory', 'c')
                ->getQuery();
        
        $em_attr = $this->modelManager->getEntityManager('Ls\CmsBundle\Entity\Attribute');
        $attributes = $em_attr->createQueryBuilder()
                ->select('a')
                ->from('LsCmsBundle:Attribute', 'a')
                ->getQuery();

        $helps = array();
        $formMapper
                ->with('Treść')
                ->add('name', null, array('label' => 'Nazwa', 'required' => true));
        
        if ($this->getRoot()->getSubject()->getId() > 0) {
            $formMapper->add('slug', null, array('label' => 'Końcówka adresu URL', 'required' => false));
            $helps['slug'] = 'Element linku URL identyfikujący produkt np. http://www.test.pl/produkty/<b>produkt</b>';
        }
        
        $formMapper
                ->add('price', null, array('label' => 'Cena', 'attr' => array()))
                ->add('promotionPrice', null, array('label' => 'Cena promocyjna', 'attr' => array()))
                ->add('isNew', null, array('label' => 'Czy jest nowością?', 'attr' => array()))
                ->add('newUntilDate', 'date', array('label' => 'Nowość aktualna do', 'required' => false, 'attr' => array()))
                ->add('quantity', null, array('label' => 'Ilość', 'attr' => array()))
                ->add('category', 'sonata_type_model', array('query' => $query, 'label' => 'Kategoria', 'required' => false))
                ->with('Zdjęcie')
                ->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => false));
        $formMapper
                ->with('Atrybuty')
                ->add('attributes', 'sonata_type_model', array('query' => $attributes, 'multiple' => true, 'expanded' => true, 'label' => 'Atrybuty:', 'required' => false));
                
        $formMapper->setHelps($helps);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name', null, array('label' => 'Nazwa'))
                ->add('category', null, array('label' => 'Kategoria'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('name', null, array('label' => 'Nazwa'))
                ->add('createdAt', 'date', array('label' => 'Data utworzenia'))
                ->add('quantity', null, array('label' => 'Ilość'))
                ->add('price', null, array('label' => 'Cena'))
                ->add('category', null, array('label' => 'Kategoria'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array(),
                        'generateAllegroTemplate' => array()
                    )
                ))
        ;
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\Product:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('product-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('product-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function saveFile($entity) {
        $entity->upload();
    }

}
