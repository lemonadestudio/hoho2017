$(document).click(function (e) {
    var container = $('#search');
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        cancel = false;
        container.css('width', 0);
        $(container).css('opacity', 0);
        $('i.search').css('opacity', 1);
        $('#search input').css('display', 'none');
    }
    $('#shop-info').fadeOut();
});
$(function(){
    $('.colorbox').colorbox({
        maxWidth: '90%'
    });
    
    $('.selectmenu').selectmenu({
        width: 261
    });
    
    var cancel = false;
    var container = $('#search');
    
    $('.search-trigger').hover(function() {
        cancel = false
        $(container).css('width', 235);
        $(container).css('opacity', 1);
        $('i.search').css('opacity', 0);
        $('#search input').css('display', 'inline-block');
    }, function() {
        if (!cancel) {
            $(container).css('width', 0);
            $(container).css('opacity', 0);
            $('i.search').css('opacity', 1);
            $('#search input').css('display', 'none');
        }
    });
    $(container).on('click', null, function(e) {
        e.preventDefault();

        cancel = (cancel) ? false : true;
    });
    
    $('#search .go').on('click', null, function() {
        $('#search form').submit();
    });
    $('#mobile-nav-trigger').on('click', null, function(e) {
        e.preventDefault();
        
        $(this).toggleClass('open');
        $('#scrolled').nanoScroller({ 
                preventPageScrolling: true,
                alwaysVisible: true,
                 scroll: 'top'
        });
        $('.main-nav-section .mnav').fadeToggle();
        $('body, html').toggleClass('scroll-blocked');
    });
    
    $('#return-to-top-btn, #return-to-top-btn-mobile').on('click', null, function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: '0px'}, 1000);
    });
    
    $('ul.slider-thumbs li:nth-child(5)').hover(function() {
        $('ul.slider-thumbs li:nth-child(4)').css('opacity', 0);
    }, function() {
        $('ul.slider-thumbs li:nth-child(4)').css('opacity', '');
    });
    
    $('.ajax-error').on('click', null, function() {
        $(this).fadeOut();
    });
    
    $('.products ul li .img-container a, .promoted .img-container a, .newest .img-container a').colorbox({
        maxWidth: '90%'
    });
    
    
    $(function() {
        $('#lscms_bundle_attributes label[for="' + localStorage['selected'] + '"]').toggleClass('selected');
        $('#lscms_bundle_attributes input').each(function() {
            $(this).click(function() {
                var id = $(this).attr('id');
                var label = $('#lscms_bundle_attributes label[for="' + id + '"]');
                var input = $(id);

                $(label).addClass('selected');
                localStorage['selected'] = id;
            });
        });
    });
    $(function() {
        $('#remove-select').click(function() {
            $('selected').removeClass('selected');
            localStorage.clear();
        });
    })
    $(function() {
        $('#ls_cmsbundle_sort_condition').change(function() {
            this.form.submit();
        });
    });
    $(function() {
        $('#lscms_bundle_attributes input').change(function() {
            this.form.submit();
        });
    });
    $('.search-box form').on('submit', null, function(e) {
        e.preventDefault();
        var path = $('#front-map').data('path');
        var data = $('.search-box form').serialize();
        
        $.ajax({
            dataType: 'JSON',
            method: 'POST',
            url: path,
            data: data,
            success: function(response) {
                if (response.error) {
                    $('.ajax-error').fadeIn();
                    $('.ajax-error').html(response.error);
                    initMap();
                } else {
                    $('.ajax-error').fadeOut();
                    $('#front-map').attr('data-lat', response.lat);
                    $('#front-map').attr('data-long', response.long);
                    initMap();
                }
            }
        });
    });
});

$(document).on('click', function(e) {
    if ($(e.target).hasClass('popup-banner')) {
        closePopupBanner();
    }
});

function closePopupBanner() {
    $('.popup-banner').fadeOut();

    $.ajax({
        method: 'POST',
        url: "/close-popup",
    });
}

$(document).ready(function () {
    var mainSlider = $('#bx-mobile-slider').bxSlider({
        auto: false,
        touchEnabled: true,
        pager: true,
        controls: false,
        onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
            $('.active-slide').removeClass('active-slide');
            $('#bx-mobile-slider>.item').eq(currentSlideHtmlObject + 1).addClass('active-slide');
            console.log(currentSlideHtmlObject);
            mainContentSlider.goToSlide(currentSlideHtmlObject);
        },
        onSliderLoad: function () {
            $('#bx-mobile-slider>.item').eq(1).addClass('active-slide');
        }
    });
    
    var mainContentSlider = $('#bx-mobile-content-slider').bxSlider({
        mode: 'fade',
        auto: false,
        touchEnabled: false,
        pager: false,
        controls: false
    });
});