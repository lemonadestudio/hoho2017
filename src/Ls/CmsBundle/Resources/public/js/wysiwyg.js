
$(function() {

    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: '/bundles/lscms/css/editor.css',
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}},
            {name: 'Wstęp', element: 'p', attributes: {'class': 'wstep'}},
            {name: 'Wyróżnienie', element: 'p', attributes: {'class': 'warning'}}
        ],
        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=flash',
    };

    fckconfig = jQuery.extend(true, {
        height: '400px',
        width: '700px'
    }, fckconfig_common);

    $('.wysiwyg').ckeditor(fckconfig);
    var editor = $('.wysiwyg').ckeditor().editor;
});