<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Utils\Tools;
use Ls\CmsBundle\Entity\Attribute;

/**
 * Product
 * @ORM\Table(name="product")
 * @ORM\Entity
 */
class Product {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;
    
    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;
    
    /**
     * @ORM\Column(type="decimal", nullable=true, scale=2)
     * @var decimal
     */
    private $price;
    
    /**
     * @ORM\Column(type="decimal", nullable=true, scale=2)
     * @var decimal
     */
    private $promotionPrice;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var integer
     */
    private $quantity;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $isNew;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $newUntilDate;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $contentShortGenerate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $contentShort;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seoGenerate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seoTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seoKeywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seoDescription;
    
    /**
     * @ORM\ManyToOne(targetEntity="ProductCategory", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="SET NULL")
     *
     */
    private $category;
    
    /**
     * @ORM\ManyToMany(targetEntity="Attribute", inversedBy="products")
     * @ORM\JoinTable(name="product_attributes",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="attribute_id", referencedColumnName="id")}
     *      )
     */
    private $attributes;

    /**
     * @Assert\File(maxSize="2097152")
     */
    protected $file;
    
    protected $promotionListWidth = 195;
    protected $promotionListHeight = 156;
    
    protected $detailWidth = 257;
    protected $detailHeight = 203;
    
    protected $listWidth = 195;
    protected $listHeight = 156;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->seoGenerate = true;
        $this->contentShortGenerate = true;
        $this->createdAt = new \DateTime();
        $this->attributes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function __toString() {
        if (is_null($this->getName()))
            return '';
        
        return $this->getName();
    }
    
    public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'detail':
                $size['width'] = $this->detailWidth;
                $size['height'] = $this->detailHeight;
                break;
            case 'promotionList':
                $size['width'] = $this->promotionListWidth;
                $size['height'] = $this->promotionListHeight;
                break;
            case 'list':
                $size['width'] = $this->listWidth;
                $size['height'] = $this->listHeight;
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            switch ($type) {
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
                case 'promotionList':
                    $sThumbName = Tools::thumbName($this->photo, '_pl');
                    break;
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            switch ($type) {
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
                case 'promotionList':
                    $sThumbName = Tools::thumbName($this->photo, '_pl');
                    break;
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->deletePhoto();
        $this->file = $file;
        if (empty($this->photo)) {
            $this->setPhoto('empty');
        } else {
            $this->setPhoto('');
        }
    }

    public function getFile() {
        return $this->file;
    }

    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_d = Tools::thumbName($filename, '_d');
            $filename_pl = Tools::thumbName($filename, '_pl');
            $filename_l = Tools::thumbName($filename, '_l');
            
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_d)) {
                @unlink($filename_d);
            }
            if (file_exists($filename_pl)) {
                @unlink($filename_pl);
            }
            if (file_exists($filename_l)) {
                @unlink($filename_l);
            }
        }
    }

    public function getPhotoAbsolutePath() {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath() {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/product';
    }

    public function upload() {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
        $thumb = ThumbFactory::create($sSourceName);
        $dimensions = $thumb->getCurrentDimensions();

        //zmniejszenie zdjecia oryginalnego jesli jest za duze
        if ($dimensions['width'] > 1024 || $dimensions['height'] > 768) {
            $thumb->resize(1024, 768);
            $thumb->save($sSourceName);
        }

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameD = Tools::thumbName($sSourceName, '_d');
        $aThumbSizeD = $this->getThumbSize('detail');
        $thumb->adaptiveResize($aThumbSizeD['width'], $aThumbSizeD['height']);
        $thumb->save($sThumbNameD);
        
        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNamePl = Tools::thumbName($sSourceName, '_pl');
        $aThumbSizePl = $this->getThumbSize('promotionList');
        $thumb->adaptiveResize($aThumbSizePl['width'], $aThumbSizePl['height']);
        $thumb->save($sThumbNamePl);
        
        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameL = Tools::thumbName($sSourceName, '_l');
        $aThumbSizeL = $this->getThumbSize('list');
        $thumb->adaptiveResize($aThumbSizeL['width'], $aThumbSizeL['height']);
        $thumb->save($sThumbNameL);

        unset($this->file);
    }

    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = ThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Product
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set promotionPrice
     *
     * @param float $promotionPrice
     * @return Product
     */
    public function setPromotionPrice($promotionPrice)
    {
        $this->promotionPrice = $promotionPrice;
    
        return $this;
    }

    /**
     * Get promotionPrice
     *
     * @return float 
     */
    public function getPromotionPrice()
    {
        return $this->promotionPrice;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Product
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set isNew
     *
     * @param boolean $isNew
     * @return Product
     */
    public function setIsNew($isNew)
    {
        $this->isNew = $isNew;
    
        return $this;
    }

    /**
     * Get isNew
     *
     * @return boolean 
     */
    public function getIsNew()
    {
        return $this->isNew;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Product
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    
        return $this;
    }

    /**
     * Get photo
     *
     * @return string 
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set contentShortGenerate
     *
     * @param boolean $contentShortGenerate
     * @return Product
     */
    public function setContentShortGenerate($contentShortGenerate)
    {
        $this->contentShortGenerate = $contentShortGenerate;
    
        return $this;
    }

    /**
     * Get contentShortGenerate
     *
     * @return boolean 
     */
    public function getContentShortGenerate()
    {
        return $this->contentShortGenerate;
    }

    /**
     * Set contentShort
     *
     * @param string $contentShort
     * @return Product
     */
    public function setContentShort($contentShort)
    {
        $this->contentShort = $contentShort;
    
        return $this;
    }

    /**
     * Get contentShort
     *
     * @return string 
     */
    public function getContentShort()
    {
        return $this->contentShort;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Product
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Product
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Product
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set seoGenerate
     *
     * @param boolean $seoGenerate
     * @return Product
     */
    public function setSeoGenerate($seoGenerate)
    {
        $this->seoGenerate = $seoGenerate;
    
        return $this;
    }

    /**
     * Get seoGenerate
     *
     * @return boolean 
     */
    public function getSeoGenerate()
    {
        return $this->seoGenerate;
    }

    /**
     * Set seoTitle
     *
     * @param string $seoTitle
     * @return Product
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    
        return $this;
    }

    /**
     * Get seoTitle
     *
     * @return string 
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * Set seoKeywords
     *
     * @param string $seoKeywords
     * @return Product
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seoKeywords = $seoKeywords;
    
        return $this;
    }

    /**
     * Get seoKeywords
     *
     * @return string 
     */
    public function getSeoKeywords()
    {
        return $this->seoKeywords;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     * @return Product
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;
    
        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string 
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Set category
     *
     * @param \Ls\CmsBundle\Entity\ProductCategory $category
     * @return Product
     */
    public function setCategory(\Ls\CmsBundle\Entity\ProductCategory $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Ls\CmsBundle\Entity\ProductCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set newUntilDate
     *
     * @param \DateTime $newUntilDate
     * @return Product
     */
    public function setNewUntilDate($newUntilDate)
    {
        $this->newUntilDate = $newUntilDate;
    
        return $this;
    }

    /**
     * Get newUntilDate
     *
     * @return \DateTime 
     */
    public function getNewUntilDate()
    {
        return $this->newUntilDate;
    }

    /**
     * Add attributes
     *
     * @param \Ls\CmsBundle\Entity\Attribute $attributes
     * @return Product
     */
    public function addAttribute(\Ls\CmsBundle\Entity\Attribute $attributes)
    {
        $attributes->addProduct($this);
        $this->attributes[] = $attributes;
    
        return $this;
    }

    /**
     * Remove attributes
     *
     * @param \Ls\CmsBundle\Entity\Attribute $attributes
     */
    public function removeAttribute(\Ls\CmsBundle\Entity\Attribute $attributes)
    {
        $this->attributes->removeElement($attributes);
    }

    /**
     * Get attributes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
}