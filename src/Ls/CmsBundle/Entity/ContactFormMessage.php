<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Utils\Tools;

/**
 * ContactFormMessage
 * @ORM\Table(name="contact_form_message")
 * @ORM\Entity
 */
class ContactFormMessage {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $senderName;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $senderEmail;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $senderMessage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $senderIp;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $sentAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set senderName
     *
     * @param string $senderName
     * @return ContactFormMessage
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;
    
        return $this;
    }

    /**
     * Get senderName
     *
     * @return string 
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * Set senderEmail
     *
     * @param string $senderEmail
     * @return ContactFormMessage
     */
    public function setSenderEmail($senderEmail)
    {
        $this->senderEmail = $senderEmail;
    
        return $this;
    }

    /**
     * Get senderEmail
     *
     * @return string 
     */
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }

    /**
     * Set senderMessage
     *
     * @param string $senderMessage
     * @return ContactFormMessage
     */
    public function setSenderMessage($senderMessage)
    {
        $this->senderMessage = $senderMessage;
    
        return $this;
    }

    /**
     * Get senderMessage
     *
     * @return string 
     */
    public function getSenderMessage()
    {
        return $this->senderMessage;
    }

    /**
     * Set senderIp
     *
     * @param string $senderIp
     * @return ContactFormMessage
     */
    public function setSenderIp($senderIp)
    {
        $this->senderIp = $senderIp;
    
        return $this;
    }

    /**
     * Get senderIp
     *
     * @return string 
     */
    public function getSenderIp()
    {
        return $this->senderIp;
    }

    /**
     * Set sentAt
     *
     * @param \DateTime $sentAt
     * @return ContactFormMessage
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;
    
        return $this;
    }

    /**
     * Get sentAt
     *
     * @return \DateTime 
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }
}