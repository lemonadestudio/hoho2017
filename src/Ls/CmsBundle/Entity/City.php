<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Utils\Tools;

/**
 * City
 * @ORM\Table(name="city")
 * @ORM\Entity
 */
class City {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $postal_code;
    
    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seoGenerate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seoTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seoKeywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seoDescription;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var float
     */
    private $mapPositionFromTop;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var float
     */
    private $mapPositionFromLeft;
    
    /**
     * @ORM\OneToMany(
     *   targetEntity="Shop",
     *   mappedBy="city"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $shops;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->seoGenerate = true;
        $this->createdAt = new \DateTime();
    }
    
    public function __toString() {
        if (is_null($this->getName()))
            return '';
        
        return $this->getName();
    }
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return City
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return City
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return City
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set seoGenerate
     *
     * @param boolean $seoGenerate
     * @return City
     */
    public function setSeoGenerate($seoGenerate)
    {
        $this->seoGenerate = $seoGenerate;
    
        return $this;
    }

    /**
     * Get seoGenerate
     *
     * @return boolean 
     */
    public function getSeoGenerate()
    {
        return $this->seoGenerate;
    }

    /**
     * Set seoTitle
     *
     * @param string $seoTitle
     * @return City
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    
        return $this;
    }

    /**
     * Get seoTitle
     *
     * @return string 
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * Set seoKeywords
     *
     * @param string $seoKeywords
     * @return City
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seoKeywords = $seoKeywords;
    
        return $this;
    }

    /**
     * Get seoKeywords
     *
     * @return string 
     */
    public function getSeoKeywords()
    {
        return $this->seoKeywords;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     * @return City
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;
    
        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string 
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Set mapPositionFromTop
     *
     * @param integer $mapPositionFromTop
     * @return City
     */
    public function setMapPositionFromTop($mapPositionFromTop)
    {
        $this->mapPositionFromTop = $mapPositionFromTop;
    
        return $this;
    }

    /**
     * Get mapPositionFromTop
     *
     * @return integer 
     */
    public function getMapPositionFromTop()
    {
        return $this->mapPositionFromTop;
    }

    /**
     * Set mapPositionFromLeft
     *
     * @param integer $mapPositionFromLeft
     * @return City
     */
    public function setMapPositionFromLeft($mapPositionFromLeft)
    {
        $this->mapPositionFromLeft = $mapPositionFromLeft;
    
        return $this;
    }

    /**
     * Get mapPositionFromLeft
     *
     * @return integer 
     */
    public function getMapPositionFromLeft()
    {
        return $this->mapPositionFromLeft;
    }

    /**
     * Add shops
     *
     * @param \Ls\CmsBundle\Entity\Shop $shops
     * @return City
     */
    public function addShop(\Ls\CmsBundle\Entity\Shop $shops)
    {
        $this->shops[] = $shops;
    
        return $this;
    }

    /**
     * Remove shops
     *
     * @param \Ls\CmsBundle\Entity\Shop $shops
     */
    public function removeShop(\Ls\CmsBundle\Entity\Shop $shops)
    {
        $this->shops->removeElement($shops);
    }

    /**
     * Get shops
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getShops()
    {
        return $this->shops;
    }

    /**
     * Set postal_code
     *
     * @param string $postalCode
     * @return City
     */
    public function setPostalCode($postalCode)
    {
        $this->postal_code = $postalCode;
    
        return $this;
    }

    /**
     * Get postal_code
     *
     * @return string 
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }
}