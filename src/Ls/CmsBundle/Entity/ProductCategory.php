<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Utils\Tools;

/**
 * ProductCategory
 * @ORM\Table(name="product_category")
 * @ORM\Entity
 */
class ProductCategory {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;
    
    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seoGenerate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seoTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seoKeywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seoDescription;
    
    /**
     * @ORM\OneToMany(
     *   targetEntity="Product",
     *   mappedBy="category"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $products;

    /**
     * @Assert\File(maxSize="2097152")
     */
    protected $file;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;
    
    protected $sliderDesktopWidth = 1170;
    protected $sliderDesktopHeight = 270;
    
    protected $sliderTabletWidth = 768;
    protected $sliderTabletHeight = 270;
    
    protected $sliderMobileWidth = 300;
    protected $sliderMobileHeight = 300;

    /**
     * Constructor
     */
    public function __construct() {
        $this->seoGenerate = true;
        $this->createdAt = new \DateTime();
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function __toString() {
        if (is_null($this->getName()))
            return '';
        
        return $this->getName();
    }
    public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'sliderDesktop':
                $size['width'] = $this->sliderDesktopWidth;
                $size['height'] = $this->sliderDesktopHeight;
                break;
            case 'sliderTablet':
                $size['width'] = $this->sliderTabletWidth;
                $size['height'] = $this->sliderTabletHeight;
                break;
            case 'sliderMobile':
                $size['width'] = $this->sliderMobileWidth;
                $size['height'] = $this->sliderMobileHeight;
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            switch ($type) {
                case 'sliderDesktop':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
                case 'sliderTablet':
                    $sThumbName = Tools::thumbName($this->photo, '_t');
                    break;
                case 'sliderMobile':
                    $sThumbName = Tools::thumbName($this->photo, '_m');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            switch ($type) {
                case 'sliderDesktop':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
                case 'sliderTablet':
                    $sThumbName = Tools::thumbName($this->photo, '_t');
                    break;
                case 'sliderMobile':
                    $sThumbName = Tools::thumbName($this->photo, '_m');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->deletePhoto();
        $this->file = $file;
        if (empty($this->photo)) {
            $this->setPhoto('empty');
        } else {
            $this->setPhoto('');
        }
    }

    public function getFile() {
        return $this->file;
    }
    
    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_d = Tools::thumbName($filename, '_d');
            $filename_t = Tools::thumbName($filename, '_t');
            $filename_m = Tools::thumbName($filename, '_m');
            
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_d)) {
                @unlink($filename_d);
            }
            if (file_exists($filename_t)) {
                @unlink($filename_t);
            }
            if (file_exists($filename_m)) {
                @unlink($filename_m);
            }
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductCategory
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ProductCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ProductCategory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ProductCategory
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set seoGenerate
     *
     * @param boolean $seoGenerate
     * @return ProductCategory
     */
    public function setSeoGenerate($seoGenerate)
    {
        $this->seoGenerate = $seoGenerate;
    
        return $this;
    }

    /**
     * Get seoGenerate
     *
     * @return boolean 
     */
    public function getSeoGenerate()
    {
        return $this->seoGenerate;
    }

    /**
     * Set seoTitle
     *
     * @param string $seoTitle
     * @return ProductCategory
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    
        return $this;
    }

    /**
     * Get seoTitle
     *
     * @return string 
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * Set seoKeywords
     *
     * @param string $seoKeywords
     * @return ProductCategory
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seoKeywords = $seoKeywords;
    
        return $this;
    }

    /**
     * Get seoKeywords
     *
     * @return string 
     */
    public function getSeoKeywords()
    {
        return $this->seoKeywords;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     * @return ProductCategory
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;
    
        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string 
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Add products
     *
     * @param \Ls\CmsBundle\Entity\Product $products
     * @return ProductCategory
     */
    public function addProduct(\Ls\CmsBundle\Entity\Product $products)
    {
        $this->products[] = $products;
    
        return $this;
    }

    /**
     * Remove products
     *
     * @param \Ls\CmsBundle\Entity\Product $products
     */
    public function removeProduct(\Ls\CmsBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ProductCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function getPhotoAbsolutePath() {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath() {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/product_category';
    }

    public function upload() {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
        $thumb = ThumbFactory::create($sSourceName);
        $dimensions = $thumb->getCurrentDimensions();

        //zmniejszenie zdjecia oryginalnego jesli jest za duze
        if ($dimensions['width'] > 1170 || $dimensions['height'] > 900) {
            $thumb->resize(1170, 900);
            $thumb->save($sSourceName);
        }

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameD = Tools::thumbName($sSourceName, '_d');
        $aThumbSizeD = $this->getThumbSize('sliderDesktop');
        $thumb->adaptiveResize($aThumbSizeD['width'], $aThumbSizeD['height']);
        $thumb->save($sThumbNameD);
        
        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameT = Tools::thumbName($sSourceName, '_t');
        $aThumbSizeT = $this->getThumbSize('sliderTablet');
        $thumb->adaptiveResize($aThumbSizeT['width'], $aThumbSizeT['height']);
        $thumb->save($sThumbNameT);
        
        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameM = Tools::thumbName($sSourceName, '_m');
        $aThumbSizeM = $this->getThumbSize('sliderMobile');
        $thumb->adaptiveResize($aThumbSizeM['width'], $aThumbSizeM['height']);
        $thumb->save($sThumbNameM);

        unset($this->file);
    }

    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = ThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }
    
    /**
     * Set photo
     *
     * @param string $photo
     * @return ProductCategory
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    
        return $this;
    }

    /**
     * Get photo
     *
     * @return string 
     */
    public function getPhoto()
    {
        return $this->photo;
    }
}