<?php

namespace Ls\CmsBundle\Extensions;

use Ls\CmsBundle\Utils\Config;

class SettingsExtension extends \Twig_Extension {

    protected $cms_config;

    function __construct(Config $config) {
        $this->cms_config = $config;
    }

    public function getGlobals() {
        return array(
            'settings' => $this->cms_config->all(),
            'wydarzenia' => $this->cms_config->wydarzenia(),
            'cms_config' => $this->cms_config
        );
    }

    public function getName() {
        return 'cms_config';
    }

}