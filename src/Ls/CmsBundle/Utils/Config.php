<?php

namespace Ls\CmsBundle\Utils;

use Ls\CmsBundle\Entity\Settings;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Config {

    protected $em;
    protected $repo;
    protected $container;
    protected $settings = array();
    protected $is_loaded = false;

    public function __construct(EntityManager $em, ContainerInterface $container) {
        $this->em = $em;
        $this->container = $container;
    }

    public function get($name, $default) {
        if (array_key_exists($name, $this->settings)) {
            return $this->settings[$name];
        }
        return $default;
    }

    public function all() {
        $settings = array();

        if ($this->is_loaded) {
            return $this->settings;
        }

        foreach ($this->getRepo()->findAll() as $setting) {
            $settings[$setting->getLabel()] = $setting->getValue();
        }

        $this->settings = $settings;
        $this->is_loaded;

        return $settings;
    }

    public function wydarzenia() {
        $qb = $this->em->createQueryBuilder();
        $news = $qb->select('a')
                ->from('LsCmsBundle:News', 'a')
                ->where($qb->expr()->isNotNull('a.published_at'))
                ->orderBy('a.published_at', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();
        
        foreach ($news as $entity) {
            $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 150, '...'));
        }

        return $news;
    }

    protected function getRepo() {
        if ($this->repo === null) {
            $this->repo = $this->em->getRepository(get_class(new Settings()));
        }

        return $this->repo;
    }

}
