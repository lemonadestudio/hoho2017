<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\CmsBundle\Entity\Attribute;
use Ls\CmsBundle\Utils\Tools;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ls\CmsBundle\Form\AttributesType;

/**
 * Attribute controller.
 *
 */
class AttributeController extends Controller {

    public function indexAction(Request $request, $slug) {
        $em = $this->getDoctrine()->getManager();
        $route = $request->get('_route');

        $post_value = $request->request->get('ls_cmsbundle_sort');
        
        if ($post_value != null) {
            $value = $post_value['condition'];
        } else {
            $value = '';
        }
        $category = $em->getRepository('LsCmsBundle:ProductCategory')->findOneBySlug($slug);
        
        $orderConditions = $this->setConditionsForQuery($value);
        if ($orderConditions == false) {
            $order = 'createdAt';
            $direction = 'DESC';
        } else {
            $order = $orderConditions['order']; 
            $direction = $orderConditions['direction'];
        }

        $qb = $em->createQueryBuilder();
        $attributes = $qb->select('a')
                ->from('LsCmsBundle:Attribute', 'a')
                ->join('a.products', 'p')
                ->where('p.id is NOT NULL')
                ->andWhere('p.category = :category')
                ->setParameter('category', $category->getId())
                ->getQuery()
                ->getResult();
                
        $attributeChoices = [];

        foreach ($attributes as $attribute) {
            $attributeChoices[$attribute->getId()] = $attribute->getName();
        }

        $attributesForm = $this->createForm(new AttributesType($attributeChoices));

        return $this->render('LsCmsBundle:Attribute:_attributes.html.twig', array(
            'attributes' => $attributes,
            'main' => false,
            'location' => $route,
            'form' => $attributesForm->createView(),
            'slug' => $slug
        ));
    }

    /**
     * @Template()
     */
    public function listInPaneAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()->select('a')
                ->from('LsCmsBundle:Attribute', 'a')
                ->orderBy('a.createdAt', 'ASC')
                ->getQuery()
                ->getResult();

        return array(
            'categories' => $entities
        );
    }
    
    /**
     * Returns set of conditions taken from post method (if exists).
     * @param string $postValue
     * @return array
     */
    public function setConditionsForQuery($postValue) 
    {
        if ($postValue == '') {
            return false;
        }
        $postValue = split('_', $postValue);
        
        $conditions = [
            'order' => $postValue[0],
            'direction' => strtoupper($postValue[1])
        ];
        
        return $conditions;
    }
}
