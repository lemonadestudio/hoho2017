<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Pages controller.
 *
 */
class PagesController extends Controller {

    /**
     * Finds and displays a Pages entity.
     *
     */
    public function showAction(Request $request, $slug) {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('LsCmsBundle:Pages')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pages entity.');
        }
        $location = $entity->getSlug();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem($entity->getTitle(), $this->get('router')->generate(
                        'lscms_pages_show', array('slug' => $entity->getSlug())
        ));
        return $this->render('LsCmsBundle:Pages:show.html.twig', array(
            'page' => $entity,
            'main' => false,
            'additionalContentClass' => 'subpage',
            'location' => $location
        ));
    }

}
