<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Ls\CmsBundle\Entity\GalleryPhoto;
use Ls\CmsBundle\Form\GalleryPhotoType;

/**
 * GalleryPhoto controller.
 *
 */
class GalleryPhotoController extends Controller
{

    /**
     * Lists all GalleryPhoto entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LsCmsBundle:GalleryPhoto')->findAll();

        return $this->render('LsCmsBundle:GalleryPhoto:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new GalleryPhoto entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new GalleryPhoto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('lscms_gallery_photo_show', array('id' => $entity->getId())));
        }

        return $this->render('LsCmsBundle:GalleryPhoto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a GalleryPhoto entity.
    *
    * @param GalleryPhoto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(GalleryPhoto $entity)
    {
        $form = $this->createForm(new GalleryPhotoType(), $entity, array(
            'action' => $this->generateUrl('lscms_gallery_photo_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new GalleryPhoto entity.
     *
     */
    public function newAction()
    {
        $entity = new GalleryPhoto();
        $form   = $this->createCreateForm($entity);

        return $this->render('LsCmsBundle:GalleryPhoto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a GalleryPhoto entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsCmsBundle:GalleryPhoto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GalleryPhoto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LsCmsBundle:GalleryPhoto:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing GalleryPhoto entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsCmsBundle:GalleryPhoto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GalleryPhoto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LsCmsBundle:GalleryPhoto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a GalleryPhoto entity.
    *
    * @param GalleryPhoto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(GalleryPhoto $entity)
    {
        $form = $this->createForm(new GalleryPhotoType(), $entity, array(
            'action' => $this->generateUrl('lscms_gallery_photo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing GalleryPhoto entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsCmsBundle:GalleryPhoto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GalleryPhoto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('lscms_gallery_photo_edit', array('id' => $id)));
        }

        return $this->render('LsCmsBundle:GalleryPhoto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a GalleryPhoto entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LsCmsBundle:GalleryPhoto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find GalleryPhoto entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('lscms_gallery_photo'));
    }

    /**
     * Creates a form to delete a GalleryPhoto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lscms_gallery_photo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
