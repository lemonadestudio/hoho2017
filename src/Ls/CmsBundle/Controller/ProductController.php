<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\CmsBundle\Entity\Product;
use Ls\CmsBundle\Utils\Tools;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ls\CmsBundle\Form\SortType;

/**
 * Product controller.      
 *
 */
class ProductController extends Controller {

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $route = $request->get('_route');
        $post_value = $request->request->get('ls_cmsbundle_sort');
        $today = new \DateTime('now');
        
        if ($post_value != null) {
            $value = $post_value['condition'];
        } else {
            $value = '';
        }
        
        $orderConditions = $this->setConditionsForQuery($value);
        if ($orderConditions == false) {
            $order = 'createdAt';
            $direction = 'DESC';
        } else {
            $order = $orderConditions['order'];
            $direction = $orderConditions['direction'];
        }
       
        $qb = $em->createQueryBuilder();
        if ($route == 'lscms_product_new') {
            $subpageTitle = 'Nowości';
            $subpageDesc = 'Lista najnowszych produktów w naszej ofercie.';
            $entities = $qb->select('a')
                ->from('LsCmsBundle:Product', 'a')
                ->where('a.isNew = 1')
                    ->andWhere('a.newUntilDate > :today')
                ->orderBy('a.'.$order, $direction)
                ->setParameter(':today', $today)
                ->getQuery()
                ->getResult();
        } elseif ($route == 'lscms_product_promotion') {
            $subpageTitle = 'Promocje';
            $subpageDesc = 'Lista promocyjnych produktów w naszej ofercie.';
            $entities = $qb->select('a')
                ->from('LsCmsBundle:Product', 'a')
                ->where('a.promotionPrice > 0')
                ->orderBy('a.'.$order, $direction)
                ->getQuery()
                ->getResult();
        } else {
            $subpageDesc = 'Lista wszystkich produktów w naszej ofercie.';
            $subpageTitle = 'Produkty';
            $entities = $qb->select('a')
                ->from('LsCmsBundle:Product', 'a')
                ->orderBy('a.'.$order, $direction)
                ->getQuery()
                ->getResult();
        }

        // formularz sortowania
        $sortForm = $this->createForm(new SortType($post_value['condition']));
        
        // paginacja
        $limit = $em->getRepository('LsCmsBundle:Settings')->findOneByLabel('limit_products')->getValue();
        $page = $this->get('request')->query->get('page', 1);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($entities, $page, $limit);
        $pagination->setPageRange(3);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem('Produkty', $this->get('router')->generate('lscms_product'));

        return $this->render('LsCmsBundle:Product:index.html.twig', array(
            'products' => $pagination,
            'additionalContentClass' => 'productList',
            'subpageTitle' => $subpageTitle,
            'subpageDesc' => $subpageDesc,
            'main' => false,
            'location' => $route,
            'form' => $sortForm->createView()
        ));
    }
    
    /**
     * Returns set of conditions taken from post method (if exists).
     * @param string $postValue
     * @return array
     */
    public function setConditionsForQuery($postValue) 
    {
        if ($postValue == '') {
            return false;
        }
        $postValue = split('_', $postValue);
        
        $conditions = [
            'order' => $postValue[0],
            'direction' => strtoupper($postValue[1])
        ];
        
        return $conditions;
    }
    
    public function promotedProductsAction() 
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $products = $qb->select('p')
                ->from('LsCmsBundle:Product', 'p')
                ->where('p.promotionPrice is not null')
                ->getQuery()
                ->getResult();
        
        return $this->render('LsCmsBundle:Product:_promoted.html.twig', array(
            'products' => $products
        ));
    }
    
    public function newProductsAction() 
    {
        $em = $this->getDoctrine()->getManager();
        $today = new \DateTime('now');
        
        $qb = $em->createQueryBuilder();
        
        $promoted = $qb->select('n')
                ->from('LsCmsBundle:Product', 'n')
                ->where('n.isNew = 1')
                    ->andWhere('n.newUntilDate > :today')
                ->setParameter(':today', $today)
                ->getQuery()
                ->getResult();
        
        if ($promoted) {
            $products = $promoted;
        } else {
            $products = $this->getAllProductsLimited(5);
        }
        
        return $this->render('LsCmsBundle:Product:_newest.html.twig', array(
            'products' => $products
        ));
        
    }
    
    /**
     * Return all products, sorted by adding date and limited to given parameter.
     * @param int $limit
     * @return array $products 
     */
    public function getAllProductsLimited($limit) 
    {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $products = $qb->select('a')
            ->from('LsCmsBundle:Product', 'a')
            ->orderBy('a.createdAt', 'DESC')
                ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
        
        return $products;
    }
    
    /**
     * Picks one item from some set.
     * @param array $items
     * @return object
     */
    public function pickRandomItem($items) 
    {
        $max = count($items);
        $random = rand(1, $max);
                
        return $items[$random];
    }
    
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('LsCmsBundle:Product')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem('Produkty', $this->get('router')->generate('lscms_product'));
        $breadcrumbs->addItem($entity->getName(), $this->get('router')->generate(
                        'lscms_product_show', array('slug' => $entity->getSlug())
        ));

        return $this->render('LsCmsBundle:Product:show.html.twig', array(
            'product' => $entity,
            'main' => false,
            'additionalContentClass' => 'product'
        ));
    }
    
    /**
     * @Template()
     */
    public function promotionsSliderAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()->select('s')
                ->from('LsCmsBundle:Slider', 's')
                ->orderBy('s.created_at', 'ASC')
                ->getQuery()
                ->getResult();

        return array(
            'sliders' => $entities
        );
    }
}
