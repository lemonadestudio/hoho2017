<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\CmsBundle\Entity\ProductCategory;
use Ls\CmsBundle\Utils\Tools;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ls\CmsBundle\Form\SortType;

/**
 * ProductCategory controller.
 *
 */
class ProductCategoryController extends Controller {

    public function showAction(Request $request, $slug) {
        $em = $this->getDoctrine()->getManager();

        $post_value = $request->request->get('ls_cmsbundle_sort');
        $attr_value = $request->request->get('lscms_bundle')['attributes'][0];
        if (!$post_value && !$attr_value) {
            $orderConditions = $this->get('session')->clear();
        }
        
        if ($post_value != null) {
            $value = $post_value['condition'];
        } else {
            $value = '';
        }
        
        if ($post_value != null) {
            $orderConditions = $this->setConditionsForQuery($value);
        } else {
            $orderConditions = $this->get('session')->get('order');
        }
        if ($orderConditions == false) {
            $order = 'createdAt';
            $direction = 'DESC';
        } else {
            $order = $orderConditions['order'];
            $direction = $orderConditions['direction'];
        }

        $category = $em->getRepository('LsCmsBundle:ProductCategory')->findOneBySlug($slug);
        // Atrybuty
        if ($attr_value) {
             $attribute = $em->getRepository('LsCmsBundle:Attribute')->findBy(array(
                 'id' => $attr_value
             ))[0]->getId();
        }
        if ($this->get('session')->get('attribute') && !$attr_value ) {
            $attribute = $this->get('session')->get('attribute');
        }

        if (!$category) {
            throw $this->createNotFoundException('Unable to find Product Category entity.');
        }
        
        $qb = $em->createQueryBuilder();
        if (isset($attribute)) {
            $products = $qb->select('t')
                ->from('LsCmsBundle:Product', 't')
                ->where('IDENTITY(t.category) = :categoryId')
                ->leftJoin('t.attributes', 'g')
                ->andWhere('g.id = :attribute')
                ->orderBy('t.'.$order, $direction)
                ->setParameter('categoryId', $category->getId())
                ->setParameter('attribute', $attribute)
                ->getQuery()
                ->getResult();
        } else {
            $products = $qb->select('a')
                ->from('LsCmsBundle:Product', 'a')
                ->where('IDENTITY(a.category) = :categoryId')
                ->orderBy('a.'.$order, $direction)
                ->setParameter('categoryId', $category->getId())
                ->getQuery()
                ->getResult();
        }

        $limit = $em->getRepository('LsCmsBundle:Settings')->findOneByLabel('limit_products')->getValue();
        $page = $this->get('request')->query->get('page', 1);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($products, $page, $limit);
        
        $sortForm = $this->createForm(new SortType($post_value['condition']));
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem(
            $category->getName(), 
            $this->get('router')->generate('lscms_productCategory_show', array('slug' => $category->getSlug()))
        );
        $this->get('session')->set('order', $orderConditions);
        if (isset($attribute)) {
            $this->get('session')->set('attribute', $attribute);
        }
        if (isset($attribute)) {
            $selected = $attribute;
        } else {
            $selected = '';
        }

        return $this->render('LsCmsBundle:ProductCategory:show.html.twig', array(
            'category' => $category,
            'products' => $pagination,
            'additionalContentClass' => 'productList',
            'main' => false,
            'form' => $sortForm->createView(),
            'slug' => $slug,
            'attribute' => $selected
        ));
    }

    /**
     * Make list of attributes, where array key is id and value is name.
     * @param array $attributesRepository
     * @return array
     */
    public function makeListOfAttributes($attributesRepository) 
    {
        $list = [];
        
        foreach ($attributesRepository as $ListItem) {
            $list[$ListItem->getId()] = $ListItem->getName();
        } 
        return $list;
    }
    
    /**
     * @Template()
     */
    public function listInPaneAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()->select('a')
                ->from('LsCmsBundle:ProductCategory', 'a')
                ->orderBy('a.createdAt', 'ASC')
                ->getQuery()
                ->getResult();

        return array(
            'categories' => $entities
        );
    }
    
    /**
     * Returns set of conditions taken from post method (if exists).
     * @param string $postValue
     * @return array
     */
    public function setConditionsForQuery($postValue) 
    {
        if ($postValue == '') {
            return false;
        }
        $postValue = split('_', $postValue);
        
        $conditions = [
            'order' => $postValue[0],
            'direction' => strtoupper($postValue[1])
        ];
        
        return $conditions;
    }
}
