<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\CmsBundle\Entity\Shop;
use Ls\CmsBundle\Entity\City;
use Ls\CmsBundle\Utils\Tools;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Shop controller.
 *
 */
class ShopController extends Controller {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $route = $this->container->get('request')->get('_route');
        
        $entities = $qb->select('a')
                ->from('LsCmsBundle:Shop', 'a')
                ->orderBy('a.createdAt', 'DESC')
                ->getQuery()
                ->getResult();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem('Sklepy', $this->get('router')->generate('lscms_shop'));

        return $this->render('LsCmsBundle:Shop:index.html.twig', array(
            'shops' => $entities,
            'main' => false,
            'location' => $route
        ));
    }

    public function showAction($citySlug) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('LsCmsBundle:Shop')->findOneBySlug($citySlug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Shop entity.');
        }
        
        $city = $entity->getCity();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem('Sklepy', $this->get('router')->generate('lscms_shop'));
        $breadcrumbs->addItem($entity->getName(), $this->get('router')->generate('lscms_shop_show', array('citySlug' => $entity->getSlug())));

		$response = array(
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsCmsBundle:Shop:_show.html.twig', array(
                'shop' => $entity,
                'city' => $city
            ))->getContent())
        );
		
		return new JsonResponse($response);
    }

    public function detailsAction($citySlug, $shopSlug) {
        $em = $this->getDoctrine()->getManager();
        $city = $em->getRepository('LsCmsBundle:City')->findOneBySlug($citySlug);
        $shop = $em->getRepository('LsCmsBundle:Shop')->findOneBySlug($shopSlug);
        
        if (!$city || !$shop) {
            throw $this->createNotFoundException('Unable to find City or Shop entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem('Sklepy', $this->get('router')->generate('lscms_shop'));
        $breadcrumbs->addItem($city->getName(), $this->get('router')->generate('lscms_shop_show', array('citySlug' => $city->getSlug())));
        $breadcrumbs->addItem($shop->getName(), $this->get('router')->generate('lscms_shop_details', array(
            'citySlug' => $city->getSlug(),
            'shopSlug' => $shop->getSlug())));

        return $this->render('LsCmsBundle:Shop:details.html.twig', array(
            'city' => $city,
            'shop' => $shop,
            'additionalContentClass' => 'shopDetails',
            'main' => false,
        ));
    }
    
    public function searchAction(Request $request) 
    {
        $em = $this->getDoctrine()->getManager();
        
        if ($request->get('code')) {
            $condition = $request->get('code');
            $result = $em->getRepository('LsCmsBundle:City')->findBy(array(
                'postal_code' => $condition    
            ));
            $error = 'Pod wybranym kodem pocztowym nie znaleziono sklepu.';
        } else {
            $condition = $request->get('city');
            $result = $em->getRepository('LsCmsBundle:City')->findBy(array(
                'name' => $condition    
            ));
            $error = 'Brak sklepu w mieście '.$condition.' w naszej bazie.';
        }
        
    if (! $result) {
            return new JsonResponse(array(
                'error' => $error
            ));
        } else {
            return new JsonResponse(array(
                'long' => $result[0]->getMapPositionFromLeft(),
                'lat' => $result[0]->getMapPositionFromTop()
            ));
        }
        
    }

    /**
     * @Template()
     */
    public function citiesListInPaneAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()->select('a')
                ->from('LsCmsBundle:Shop', 'a')
                ->orderBy('a.name', 'ASC')
                ->getQuery()
                ->getResult();

        return array(
            'shops' => $entities
        );
    }
    
    /**
     * @Template()
     */
    public function randomShopsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->createQueryBuilder()
                ->select('a')
                ->from('LsCmsBundle:Shop', 'a')
                ->where('a.photo IS NOT NULL')
                ->orderBy('a.createdAt', 'ASC')
                ->getQuery()
                ->getResult();

        shuffle($entities);
        
        return array(
            'shops' => $entities
        );
    }
}
