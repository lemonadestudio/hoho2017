<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

class PopupController extends Controller {

    /**
     * Find and display a Popup entity.
     *
     */
    public function popupAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $showPopup = true;
        
        $entity = $em->getRepository('LsCmsBundle:Popup')->findOneBy(['id' => 1]);

        if ($request->cookies->get("close-popup") == $entity->getCookieId()) {
            $showPopup = false;
        }

        return $this->render('LsCmsBundle:Popup:popup.html.twig', array(
            'entity' => $entity,
            'showPopup' => $showPopup
        ));
    }

    public function closeAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('LsCmsBundle:Popup')->findOneBy(['id' => 1]);

        $response = new Response("OK");
        $response->headers->setCookie(new Cookie('close-popup', $entity->getCookieId()));

        return $response;
    }

}
