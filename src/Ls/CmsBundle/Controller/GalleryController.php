<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\CmsBundle\Entity\Gallery;
use Ls\CmsBundle\Form\GalleryType;

/**
 * Gallery controller.
 *
 */
class GalleryController extends Controller {

    /**
     * Lists all Gallery entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()->select('a')
                ->from('LsCmsBundle:Gallery', 'a')
                ->where('a.on_list = 1')
                ->getQuery()
                ->getResult();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem('Galerie', $this->get('router')->generate('lscms_gallery'));

        return $this->render('LsCmsBundle:Gallery:index.html.twig', array(
                    'entities' => $entities,
                    'main' => false,
        ));
    }

    /**
     * Finds and displays a Gallery entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsCmsBundle:Gallery')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem('Galerie', $this->get('router')->generate('lscms_gallery'));
        $breadcrumbs->addItem($entity->getTitle(), $this->get('router')->generate(
                        'lscms_gallery_show', array('slug' => $entity->getSlug())
        ));

        return $this->render('LsCmsBundle:Gallery:show.html.twig', array(
                    'entity' => $entity,
                    'main' => false,
        ));
    }
    
    public function showFirstAction() {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
                ->from('LsCmsBundle:GalleryPhoto', 'a')
                ->where('IDENTITY(a.gallery) = 1')
                ->getQuery()
                ->getResult();

        $limit = $em->getRepository('LsCmsBundle:Settings')->findOneByLabel('limit_gallery')->getValue();
        $page = $this->get('request')->query->get('page', 1);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($entities, $page, $limit);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem('Galeria', $this->get('router')->generate('lscms_gallery'));

        return $this->render('LsCmsBundle:Gallery:showFirst.html.twig', array(
            'entities' => $pagination,
            'main' => false,
        ));
    }

}
