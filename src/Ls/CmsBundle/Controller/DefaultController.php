<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Ls\CmsBundle\Form\ContactType;
use Ls\CmsBundle\Entity\ContactFormMessage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller {

    public function indexAction() {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('LsCmsBundle:Pages')->findOneById(1);
        $shops = $em->getRepository('LsCmsBundle:Shop')->findAll();
        
        return $this->render('LsCmsBundle:Default:index.html.twig', array(
            'page' => $entity,
            'main' => true,
            'shops' => $shops
        ));
    }
    
    /**
     * Get all cities from shops.
     * @param array $shops
     * @return array
     */
    public function getAllCities($shops) 
    {
        $cities = [];
        $key = 0;
        
        foreach ($shops as $Shop) {
            $City = $Shop->getCity();
            $cities[$key] = $City;
            $key++;
        }
        return $cities;
    }

    public function contactAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ContactType());
        $route = $request->get('_route');
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem('Kontakt', $this->get('router')->generate('lscms_contact'));
        
        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                $companyName = $form->get('companyName')->getData();
                $fullName = $form->get('fullName')->getData();
                $phone = $form->get('phone')->getData();
                $email = $form->get('email')->getData();
                $content = $form->get('message')->getData();

                $message_txt = '<h3>Wiadomość z formularza kontaktowego:</h3>';
                $message_txt .= nl2br($content) . ' <hr />';
                $message_txt .= '<h3>Pozostałe dane:</h3>';
                $message_txt .= 'Imię i nazwisko: ' . $fullName . '<br />';
                $message_txt .= 'Nazwa firmy: ' . $companyName . '<br />';
                $message_txt .= 'Telefon: ' . $phone . '<br />';
                $message_txt .= 'E-mail: ' . $email . '<br />';

                $email_to = $em->getRepository('LsCmsBundle:Settings')->findOneByLabel('email_to')->getValue();
                
                $messageModel = new ContactFormMessage();
                $messageModel->setSenderEmail($email);
                $messageModel->setSenderMessage($message_txt);
                $messageModel->setSenderName($fullName);
                $messageModel->setSenderIP($request->getClientIp());
                $messageModel->setSentAt(new \DateTime());
                $em->persist($messageModel);
                $em->flush();

                $message = \Swift_Message::newInstance()
                        ->setSubject('Wiadomość z formularza kontaktowego')
                        ->setFrom(array($this->container->getParameter('mailer_user') => 'HOHO / Mailer'))
                        ->setTo($email_to)
                        ->setBody($message_txt, 'text/html')
                        ->addPart(strip_tags($message_txt), 'text/plain');

                $mailer = $this->get('mailer');
                $mailer->send($message);
                $spool = $mailer->getTransport()->getSpool();
                $transport = $this->container->get('swiftmailer.transport.real');
                $spool->flushQueue($transport);
                $request->getSession()->getFlashBag()->add('success', '<b>Dziękujemy serdecznie za wiadomość. Kontakt zwrotny uzyskają Państwo już niebawem.</b>');

                return $this->redirect($this->generateUrl('lscms_contact'));
            }
        }

        return $this->render('LsCmsBundle:Default:contact.html.twig', array(
            'form' => $form->createView(),
            'main' => false,
            'location' => $route
        ));
    }
    
    public function searchAction(Request $request) {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona główna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem('Wyniki wyszukiwania', $this->get('router')->generate('lscms_search'));
        $keyword = $request->query->get('keyword');
        
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $products = $qb->select('a')
                ->from('LsCmsBundle:Product', 'a')
                ->where($qb->expr()->like('a.name', ':keyword'))
                ->orWhere($qb->expr()->like('a.content', ':keyword'))
                ->setParameter('keyword', '%' . $keyword . '%')
                ->getQuery()
                ->getResult();
        
        // paginacja
        $limit = 12;
        $page = $this->get('request')->query->get('page', 1);
        $paginator = $this->get('knp_paginator');
        $paginedProducts = $paginator->paginate($products, $page, $limit);
        $paginedProducts->setPageRange(3);
        
        return $this->render('LsCmsBundle:Default:search.html.twig', array(
            'products' => $paginedProducts,
            'main' => true,
            'keyword' => $keyword
        ));
    }
}
