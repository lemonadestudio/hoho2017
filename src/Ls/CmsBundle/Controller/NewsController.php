<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\CmsBundle\Entity\News;
use Ls\CmsBundle\Form\NewsType;
use Ls\CmsBundle\Utils\Tools;

/**
 * News controller.
 *
 */
class NewsController extends Controller {

    /**
     * Lists all News entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
                ->from('LsCmsBundle:News', 'a')
                ->where($qb->expr()->isNotNull('a.published_at'))
                ->orderBy('a.published_at', 'DESC')
                ->getQuery()
                ->getResult();
        
        foreach ($entities as $entity) {
            $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 150, '...'));
        }

        $limit = $em->getRepository('LsCmsBundle:Settings')->findOneByLabel('limit_news')->getValue();
        $page = $this->get('request')->query->get('page', 1);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $entities, $page, $limit
        );

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem('Aktualności', $this->get('router')->generate('lscms_news'));

        return $this->render('LsCmsBundle:News:index.html.twig', array(
                    'entities' => $pagination,
                    'main' => false,
        ));
    }

    /**
     * Finds and displays a News entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsCmsBundle:News')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->get('router')->generate('lscms_homepage'));
        $breadcrumbs->addItem('Aktualności', $this->get('router')->generate('lscms_news'));
        $breadcrumbs->addItem($entity->getTitle(), $this->get('router')->generate(
                        'lscms_news_show', array('slug' => $entity->getSlug())
        ));

        return $this->render('LsCmsBundle:News:show.html.twig', array(
                    'entity' => $entity,
                    'main' => false,
        ));
    }

}
