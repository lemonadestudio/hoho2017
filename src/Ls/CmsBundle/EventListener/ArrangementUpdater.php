<?php

namespace Ls\CmsBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PostFlushEventArgs;

class ArrangementUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'postFlush',
        );
    }

    public function postFlush(PostFlushEventArgs $args) {
        $em = $args->getEntityManager();
        $_SESSION['stopupdate'] = true;
        if (isset($_SESSION['updateKolejnosc'])) {
            foreach ($_SESSION['updateKolejnosc'] as $update) {
                foreach ($update['ids'] as $id) {
                    $target = $em->getRepository('LsCmsBundle:' . $update['type'])->findOneById($id);
                    if ($target) {
                        $new_arrangement = $target->getArrangement() - 1;
                        $target->setArrangement($new_arrangement);
                        $em->persist($target);
                    }
                }
            }
            unset($_SESSION['updateKolejnosc']);
            $em->flush();
        }
        if (isset($_SESSION['updateIlosc'])) {
            unset($_SESSION['updateIlosc']);
        }
        unset($_SESSION['stopupdate']);
    }

}