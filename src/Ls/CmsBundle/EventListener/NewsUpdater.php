<?php

namespace Ls\CmsBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CmsBundle\Utils\Tools;
use Ls\CmsBundle\Entity\News;

class NewsUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'preUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof News) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            if ($entity->getSeoGenerate()) {
                // description
                $description = strip_tags($entity->getContent());
                $description = Tools::truncateWord(html_entity_decode($description), 255, '');

                // usunięcie nowych linii
                $description = preg_replace('@\v@', ' ', $description);
                // podwójnych białych znaków
                $description = preg_replace('@\h{2,}@', ' ', $description);

                // usunięcie ostatniego, niedokończonego zdania
                $description = preg_replace('@(.*)\..*@', '\1.', $description);

                // trim
                $description = trim($description);

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('@\.,;\'\"@', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoGenerate(false);
            }
            if ($entity->getContentShortGenerate()) {
                $content_short = strip_tags($entity->getContent());
                $content_short = Tools::truncateWord(html_entity_decode($content_short), 255, '');
                $content_short = trim($content_short);
                $entity->setContentShort($content_short);
                $entity->setContentShortGenerate(false);
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof News) {
            $entity->setUpdatedAt(new \DateTime());
            if ($entity->getSeoGenerate()) {
                // description
                $description = strip_tags($entity->getContent());
                $description = Tools::truncateWord(html_entity_decode($description), 255, '');

                // usunięcie nowych linii
                $description = preg_replace('@\v@', ' ', $description);
                // podwójnych białych znaków
                $description = preg_replace('@\h{2,}@', ' ', $description);

                // usunięcie ostatniego, niedokończonego zdania
                $description = preg_replace('@(.*)\..*@', '\1.', $description);

                // trim
                $description = trim($description);

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('@\.,;\'\"@', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $keywords = implode(', ', $keywords);
                $args->setNewValue('seo_title', strip_tags($entity->getTitle()));
                $args->setNewValue('seo_keywords', $keywords);
                $args->setNewValue('seo_description', $description);
                $args->setNewValue('seo_generate', false);
            }
            if ($entity->getContentShortGenerate()) {
                $content_short = strip_tags($entity->getContent());
                $content_short = Tools::truncateWord(html_entity_decode($content_short), 255, '');
                $content_short = trim($content_short);
                $args->setNewValue('content_short', $description);
                $args->setNewValue('content_short_generate', false);
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof News) {
            $entity->deletePhoto();
        }
    }

}