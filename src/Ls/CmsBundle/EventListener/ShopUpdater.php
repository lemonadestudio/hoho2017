<?php

namespace Ls\CmsBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CmsBundle\Utils\Tools;
use Ls\CmsBundle\Entity\Shop;

class ShopUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'preUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Shop) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Shop) {
            $entity->setUpdatedAt(new \DateTime());
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Shop) {
            $entity->deletePhoto();
        }
    }

}